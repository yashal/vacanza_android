package com.vacanza.utils;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import example.com.hotelbooking.R;

public class AppDialogs {

    private Activity ctx;
    private Dialog lDialog;
    private boolean closeActivity = true;

    public static ProgressDialog showLoading(Activity activity) {
        ProgressBar progressBar = new ProgressBar(activity, null, android.R.attr.progressBarStyleSmall);
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
//        mProgressDialog.setMessage("Connecting To Server... Please Wait");
        mProgressDialog.setMessage("Please Wait while we are connecting with hotel suppliers...");
        if (!activity.isFinishing() && !mProgressDialog.isShowing())
            mProgressDialog.show();
        return mProgressDialog;
    }

    public static ProgressDialog showPaymentLoading(Activity activity) {
        ProgressBar progressBar = new ProgressBar(activity, null, android.R.attr.progressBarStyleSmall);
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
//        mProgressDialog.setMessage("Connecting To Server... Please Wait");
        mProgressDialog.setMessage("Please Wait while we are connecting to payment gateway...");
        if (!activity.isFinishing() && !mProgressDialog.isShowing())
            mProgressDialog.show();
        return mProgressDialog;
    }
    public static ProgressDialog showServerDataLoading(Activity activity) {
        ProgressBar progressBar = new ProgressBar(activity, null, android.R.attr.progressBarStyleSmall);
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Connecting To Server... Please Wait");
        if (!activity.isFinishing() && !mProgressDialog.isShowing())
            mProgressDialog.show();
        return mProgressDialog;
    }

    public static ProgressDialog showNewLoading(Activity activity) {
        ProgressBar progressBar = new ProgressBar(activity, null, android.R.attr.progressBarStyleSmall);
        ProgressDialog mProgressDialog = new ProgressDialog(activity);
        mProgressDialog.setIndeterminate(true);
        if (!activity.isFinishing() && !mProgressDialog.isShowing())
            mProgressDialog.show();
        return mProgressDialog;
    }

    public AppDialogs(Activity ctx) {
        this.ctx = ctx;
    }

    public void displayCommonDialog(String header, String msg) {
        LinearLayout OkButtonLogout;
        ImageView close_dialog;
        final Dialog DialogLogOut = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_alert_dialog);
        TextView loogout_msg = (TextView) DialogLogOut.findViewById(R.id.text_exit);
        TextView dialog_header = (TextView) DialogLogOut.findViewById(R.id.dialog_header);
        close_dialog = (ImageView) DialogLogOut.findViewById(R.id.close_dialog);
        dialog_header.setText(header);
        loogout_msg.setText(msg);
        OkButtonLogout = (LinearLayout) DialogLogOut.findViewById(R.id.btn_yes_exit_LL);

        OkButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });
        close_dialog.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
            }
        });

        DialogLogOut.show();
    }
}
