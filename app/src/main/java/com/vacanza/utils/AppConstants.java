package com.vacanza.utils;

import android.app.Activity;

import java.util.ArrayList;

/**
 * Created by shubham on 7/09/2017.
 */
public class AppConstants {


    public static String BASE_URL = "http://api.myvacanza.com/hotelws.svc/";
    public final static String PREF_NAME = "example.com.hotelbooking.prefs";
    public final static String FMS_PRFERENCES_ROOM = "example.com.hotelbooking.prefs.rooms";
    public final static String DEFAULT_VALUE = "";
    public final static ArrayList<Activity> ACTIVITIES = new ArrayList<>();
    public final static String NAME = "name";
    public final static String CITY_NAME = "CITY_NAME";
    public final static String NO_INTERNET_CONNECTED = "No internet connection available";
    public final static String SERVER_NOT_RESPONDING = "Server Not Responding";
//    public final static String BASE_URL_PAYMENT = "http://www.myvacanza.com/paymobile.aspx?token=";//live
    public final static String BASE_URL_PAYMENT = "http://ja.myvacanza.com/paymobile.aspx?token=";//demo
    public final static String MEMBER_TYPE = "member_type";

    public static final int[] AGE_OF_CHILD = new int[]{0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11};
    public final static String ROOM = "ROOM";

    public final static String ADULT1 = "ADULT1";
    public final static String ADULT2 = "ADULT2";
    public final static String ADULT3 = "ADULT3";
    public final static String ADULT4 = "ADULT4";
    public final static String ADULT5 = "ADULT5";

    public final static String CHILD1 = "CHILD1";
    public final static String CHILD2 = "CHILD2";
    public final static String CHILD3 = "CHILD3";
    public final static String CHILD4 = "CHILD4";
    public final static String CHILD5 = "CHILD5";

    public final static String ROOM1CHILD1 = "ROOM1CHILD1";
    public final static String ROOM1CHILD2 = "ROOM1CHILD2";
    public final static String ROOM1CHILD3 = "ROOM1CHILD3";

    public final static String ROOM2CHILD1 = "ROOM2CHILD1";
    public final static String ROOM2CHILD2 = "ROOM2CHILD2";
    public final static String ROOM2CHILD3 = "ROOM2CHILD3";

    public final static String ROOM3CHILD1 = "ROOM3CHILD1";
    public final static String ROOM3CHILD2 = "ROOM3CHILD2";
    public final static String ROOM3CHILD3 = "ROOM3CHILD3";

    public final static String ROOM4CHILD1 = "ROOM4CHILD1";
    public final static String ROOM4CHILD2 = "ROOM4CHILD2";
    public final static String ROOM4CHILD3 = "ROOM4CHILD3";

    public final static String ROOM5CHILD1 = "ROOM5CHILD1";
    public final static String ROOM5CHILD2 = "ROOM5CHILD2";
    public final static String ROOM5CHILD3 = "ROOM5CHILD3";
    public final static String TOTAL_PERSON = "TOTAL_PERSON";

    public final static String FIRST_NAME = "FIRST_NAME";
    public final static String LAST_NAME = "LAST_NAME";
    public final static String EMAIL = "EMAIL";
    public final static String PHONE = "PHONE";
    public final static String RANDOM_NO = "RANDOM_NO";
    public final static String HOTEL_IMAGE = "HOTEL_IMAGE";
    public final static String NATIONALITY = "NATIONALITY";
    public final static String CURRENCY = "CURRENCY";

}

