package com.vacanza.controller;

import com.squareup.otto.Bus;

/**
 * Static class for Event bus
 */
public final class FmsEventBus {
    private static final Bus BUS = new Bus();

    public static Bus getInstance() {
        return BUS;
    }

    private FmsEventBus() {
    }
}
