package com.vacanza.fragment;

import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import example.com.hotelbooking.R;

/**
 * Created by pr0 on 11/8/17.
 */

public class LoaderFragment extends Fragment {
    ImageView currency, plane;
    Animation translate;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle SavedInstanceState) {

        View v = inflater.inflate(R.layout.loading_fragment, viewGroup, false);

        currency = (ImageView) v.findViewById(R.id.currency);
        plane = (ImageView) v.findViewById(R.id.plane);

        translate = AnimationUtils.loadAnimation(getActivity(), R.anim.translate_hor);
        translate.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {

            }

            @Override
            public void onAnimationEnd(Animation animation) {
                plane.startAnimation(translate);
            }

            @Override
            public void onAnimationRepeat(Animation animation) {

            }
        });
        plane.startAnimation(translate);

        currency.setColorFilter(new PorterDuffColorFilter(Color.parseColor("#0ebe02"),
                PorterDuff.Mode.SRC_IN));
        return v;
    }
}
