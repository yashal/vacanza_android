package com.vacanza.fragment;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ActivityManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import example.com.hotelbooking.R;

import com.facebook.FacebookSdk;
import com.facebook.login.LoginManager;
import com.vacanza.activity.BaseActivity;
import com.vacanza.activity.HomeActivity;
import com.vacanza.activity.LoginActivity;
import com.vacanza.activity.ManageBookingActivity;
import com.vacanza.utils.AppConstants;

public class DrawerFragment extends Fragment {

    public ActionBarDrawerToggle mDrawerToggle;
    private int selectedPos = -1;
    private DrawerLayout drawerlayout;
    private TextView notification_title, download_title;
    private TextView logout_title, userName, cart_count_drawer;
    private ImageView userImage;
    private LinearLayout pay, name_text_layout, dear_user_text_layout;
    private LinearLayout logout,manage_booking;

    public DrawerFragment() {

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.left_slide_list_layout, container, false);

        selectedPos = -1;


        dear_user_text_layout = (LinearLayout) v.findViewById(R.id.dear_user_text_layout);
        name_text_layout = (LinearLayout) v.findViewById(R.id.name_text_layout);
        final LinearLayout home = (LinearLayout) v.findViewById(R.id.home_menu_option);

        logout = (LinearLayout) v.findViewById(R.id.logout_menu);
        manage_booking = (LinearLayout) v.findViewById(R.id.manage_booking_menu);

        setClickListener(home, 0);

        setClickListener(manage_booking, 1);

        setClickListener(logout, 2);

        logout_title = (TextView) v.findViewById(R.id.logout_title);


        userName = (TextView) v.findViewById(R.id.home_menu_UserName);

        if (getFromPrefs(AppConstants.NAME) != null && !getFromPrefs(AppConstants.NAME).equals("")) {
            logout.setVisibility(View.VISIBLE);
            logout_title.setText("Logout");
            userName.setText(getFromPrefs(AppConstants.NAME));

        } else {
            logout.setVisibility(View.VISIBLE);
            logout_title.setText("Login");
            userName.setText("Welcome Guest");
        }

        dear_user_text_layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        return v;
    }

    private void setClickListener(final LinearLayout layout, final int pos) {
        layout.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                switch (event.getAction()) {

                    case MotionEvent.ACTION_DOWN:
                        break;
                    case MotionEvent.ACTION_UP:
                        selectedPos = pos;
                        drawerlayout.closeDrawers();
                        break;
                    case MotionEvent.ACTION_CANCEL:
                        selectedPos = -1;
                        break;
                }
                return true;
            }
        });
    }

    @SuppressLint("NewApi")
    public void setUp(final DrawerLayout drawerlayout) {

        this.drawerlayout = drawerlayout;
        mDrawerToggle = new ActionBarDrawerToggle(getActivity(), drawerlayout, R.string.open_drawer, R.string.close_drawer) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                getActivity().invalidateOptionsMenu();
                ((BaseActivity) getActivity()).hideSoftKeyboard();
                if (getFromPrefs(AppConstants.NAME) != null && !getFromPrefs(AppConstants.NAME).equals("")) {
                    logout.setVisibility(View.VISIBLE);
                    userName.setText(getFromPrefs(AppConstants.NAME));

                } else {
                    logout.setVisibility(View.VISIBLE);
                    logout_title.setText("Login");
                    userName.setText("Welcome Guest");
                }
            }

            @SuppressLint("NewApi")
            @Override
            public void onDrawerClosed(View drawerView) {
                ActivityManager am = (ActivityManager) getActivity().getSystemService(Context.ACTIVITY_SERVICE);
                ComponentName cn = am.getRunningTasks(1).get(0).topActivity;
                super.onDrawerClosed(drawerView);
                if (selectedPos != -1) {
                    switch (selectedPos) {
                        case 0:
                            if (!cn.getShortClassName().equals(".activity.HomeActivity")) {
                                navigateToHome();
                            } else {
                                drawerlayout.closeDrawers();
                            }
                            break;

                        case 1:
                            drawerlayout.closeDrawers();
                            manageBooking();
                            break;

                        case 2:
                            drawerlayout.closeDrawers();
                            logout();
                            break;


                        default:
                            break;
                    }
                    selectedPos = -1;
                }
            }
        };

        drawerlayout.setDrawerListener(mDrawerToggle);

    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = getActivity().getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE);
        return prefs.getString(key, AppConstants.DEFAULT_VALUE);
    }

    private void navigateToHome() {
        drawerlayout.closeDrawers();
        for (int i = 0; i < AppConstants.ACTIVITIES.size(); i++) {
            if (AppConstants.ACTIVITIES.get(i) != null)
                AppConstants.ACTIVITIES.get(i).finish();
        }
        Intent intent = new Intent(getActivity(), HomeActivity.class);
        startActivity(intent);
        getActivity().finish();
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private void logout() {
        if(getFromPrefs(AppConstants.MEMBER_TYPE)!=null && getFromPrefs(AppConstants.MEMBER_TYPE).equalsIgnoreCase("FACEBOOK")){
            FacebookSdk.sdkInitialize(getActivity().getApplicationContext());
            LoginManager.getInstance().logOut();
        }
        if (getFromPrefs(AppConstants.FIRST_NAME).length() > 0){
            ((BaseActivity) getActivity()).clearRoomData(getActivity());
            ((BaseActivity) getActivity()).clearAppData(getActivity());
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        }else{
            Intent intent = new Intent(getActivity(), LoginActivity.class);
            getActivity().startActivity(intent);
            getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        }

    }

    private void manageBooking() {
        Intent intent = new Intent(getActivity(), ManageBookingActivity.class);
        intent.putExtra("from", "DrawerFragment");
        getActivity().startActivity(intent);
        getActivity().overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    private void removeActivity(String activity) {
        for (int i = 0; i < AppConstants.ACTIVITIES.size(); i++) {
            if (AppConstants.ACTIVITIES.get(i) != null && AppConstants.ACTIVITIES.get(i).toString().contains(activity)) {
                AppConstants.ACTIVITIES.get(i).finish();
                AppConstants.ACTIVITIES.remove(i);
                break;
            }
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

}
