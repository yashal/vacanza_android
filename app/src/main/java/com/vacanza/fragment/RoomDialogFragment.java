package com.vacanza.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatDialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;


import com.squareup.otto.Subscribe;

import java.util.ArrayList;

import com.vacanza.Pojo.RoomModel;

import example.com.hotelbooking.R;

import com.vacanza.activity.HomeActivity;
import com.vacanza.adapter.RoomAdapter;
import com.vacanza.controller.FmsEventBus;
import com.vacanza.utils.AppConstants;

/**
 * Room Dialog
 */
public class RoomDialogFragment extends AppCompatDialogFragment {

    private Activity mActivity;

    View RemoveroomRL, addRoomRL;
    private ArrayList<RoomModel> roomModels;
    private RoomAdapter mRoomAdapter;
    ImageView dialog_done;
    private ArrayList<Integer> arr;

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

    @Override
    public void onStart() {
        super.onStart();
        FmsEventBus.getInstance().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        FmsEventBus.getInstance().unregister(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        DisplayMetrics mDisplayMetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(mDisplayMetrics);

        /** Initialize layouts **/
        roomModels = getArguments().getParcelableArrayList(HomeActivity.ROOM_DATA);
        View dialogView = inflater.inflate(R.layout.dialog_add_room, container, false);
        dialog_done = (ImageView) dialogView.findViewById(R.id.done);

        arr = new ArrayList<>();
        RemoveroomRL = dialogView.findViewById(R.id.RemoveRooms);
        addRoomRL = dialogView.findViewById(R.id.AddRooms);
        ListView mListRooms = (ListView) dialogView.findViewById(R.id.list_rooms);

        if (roomModels == null) {
            roomModels = new ArrayList<>();
            addNewRoom();
        }
        mRoomAdapter = new RoomAdapter(mActivity, roomModels);
        mListRooms.setAdapter(mRoomAdapter);
        RemoveroomRL.setVisibility(View.GONE);
        RemoveroomRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (roomModels.size() > 1) {
                    roomModels.remove(roomModels.size() - 1);
                    mRoomAdapter.notifyDataSetChanged();
                }
                if (roomModels.size() == 1) RemoveroomRL.setVisibility(View.GONE);
                if (roomModels.size() < 5) addRoomRL.setVisibility(View.VISIBLE);
                else addRoomRL.setVisibility(View.GONE);
            }
        });

        addRoomRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (roomModels.size() < 5) addNewRoom();
                if (roomModels.size() == 5) addRoomRL.setVisibility(View.GONE);
                if (roomModels.size() > 1) RemoveroomRL.setVisibility(View.VISIBLE);
                else RemoveroomRL.setVisibility(View.GONE);
            }
        });


        dialog_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                ((HomeActivity) mActivity).clearRoomData(mActivity);
                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM, String.valueOf(roomModels.size()));

                for (int i = 0; i < roomModels.size(); i++) {
                    if (i == 0) {
                        ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ADULT1,
                                String.valueOf(roomModels.get(0).getNumAdults()));
                        ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.CHILD1,
                                String.valueOf(roomModels.get(0).getNumChildren()));
                        for (int j = 0; j < roomModels.get(0).getNumChildren(); j++) {
                            if (j == 0) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM1CHILD1,
                                        String.valueOf(roomModels.get(0).getAgeChild1()));
                            } else if (j == 1) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM1CHILD2,
                                        String.valueOf(roomModels.get(0).getAgeChild2()));
                            } else if (j == 2) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM1CHILD3,
                                        String.valueOf(roomModels.get(0).getAgeChild3()));
                            }
                        }
                    } else if (i == 1) {
                        ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ADULT2,
                                String.valueOf(roomModels.get(1).getNumAdults()));
                        ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.CHILD2,
                                String.valueOf(roomModels.get(1).getNumChildren()));
                        for (int j = 0; j < roomModels.get(1).getNumChildren(); j++) {
                            if (j == 0) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM2CHILD1,
                                        String.valueOf(roomModels.get(1).getAgeChild1()));
                            } else if (j == 1) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM2CHILD2,
                                        String.valueOf(roomModels.get(1).getAgeChild2()));
                            } else if (j == 2) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM2CHILD3,
                                        String.valueOf(roomModels.get(1).getAgeChild3()));
                            }
                        }
                    } else if (i == 2) {
                        ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ADULT3,
                                String.valueOf(roomModels.get(2).getNumAdults()));
                        ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.CHILD3,
                                String.valueOf(roomModels.get(2).getNumChildren()));
                        for (int j = 0; j < roomModels.get(2).getNumChildren(); j++) {
                            if (j == 0) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM3CHILD1,
                                        String.valueOf(roomModels.get(2).getAgeChild1()));
                            } else if (j == 1) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM3CHILD2,
                                        String.valueOf(roomModels.get(2).getAgeChild2()));
                            } else if (j == 2) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM3CHILD3,
                                        String.valueOf(roomModels.get(2).getAgeChild3()));
                            }
                        }

                    } else if (i == 3) {
                        ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ADULT4,
                                String.valueOf(roomModels.get(3).getNumAdults()));
                        ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.CHILD4,
                                String.valueOf(roomModels.get(3).getNumChildren()));
                        for (int j = 0; j < roomModels.get(3).getNumChildren(); j++) {
                            if (j == 0) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM4CHILD1,
                                        String.valueOf(roomModels.get(3).getAgeChild1()));
                            } else if (j == 1) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM4CHILD2,
                                        String.valueOf(roomModels.get(3).getAgeChild2()));
                            } else if (j == 2) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM4CHILD3,
                                        String.valueOf(roomModels.get(3).getAgeChild3()));
                            }
                        }
                    } else if (i == 4) {
                        ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ADULT5,
                                String.valueOf(roomModels.get(4).getNumAdults()));
                        ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.CHILD5,
                                String.valueOf(roomModels.get(4).getNumChildren()));
                        for (int j = 0; j < roomModels.get(4).getNumChildren(); j++) {
                            if (j == 0) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM5CHILD1,
                                        String.valueOf(roomModels.get(4).getAgeChild1()));
                            } else if (j == 1) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM5CHILD2,
                                        String.valueOf(roomModels.get(4).getAgeChild2()));
                            } else if (j == 2) {
                                ((HomeActivity) mActivity).saveIntoRoomPrefs(AppConstants.ROOM5CHILD3,
                                        String.valueOf(roomModels.get(4).getAgeChild3()));
                            }
                        }
                    }
                }
                FmsEventBus.getInstance().post(roomModels);
                dismiss();

            }

        });
        return dialogView;
    }

    /**
     * Add new Room Default
     */
    private void addNewRoom() {
        RoomModel roomModel = new RoomModel();
        if (roomModels == null)
            roomModel.setRoomNo(1);
        else
            roomModel.setRoomNo(roomModels.size() + 1);
        roomModel.setNumAdults(1);
        roomModels.add(roomModel);
        if (mRoomAdapter != null) mRoomAdapter.notifyDataSetChanged(roomModels);
    }

    @Subscribe
    public void getRoomModel(ArrayList<RoomModel> roomModels) {
        this.roomModels = roomModels;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (roomModels != null && addRoomRL != null && RemoveroomRL != null)
        {
            if (roomModels.size() == 1)
            {
                RemoveroomRL.setVisibility(View.GONE);
                addRoomRL.setVisibility(View.VISIBLE);
            }
            else if (roomModels.size() == 5)
            {
                addRoomRL.setVisibility(View.GONE);
                RemoveroomRL.setVisibility(View.VISIBLE);
            }
            else
            {
                RemoveroomRL.setVisibility(View.VISIBLE);
                addRoomRL.setVisibility(View.VISIBLE);
            }
        }
    }
}
