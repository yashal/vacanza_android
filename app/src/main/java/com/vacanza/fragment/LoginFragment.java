package com.vacanza.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import example.com.hotelbooking.R;

import com.vacanza.activity.ForgetPasswordActivity;
import com.vacanza.activity.SignUpActivity;

/**
 * Created by pr0 on 11/8/17.
 */

public class LoginFragment extends Fragment implements View.OnClickListener {
    CardView login, signup, fb_login;

    TextView forgetPass;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle SavedInstanceState) {

        View v = inflater.inflate(R.layout.login_activity, viewGroup, false);

        login = (CardView) v.findViewById(R.id.login);
        signup = (CardView) v.findViewById(R.id.signup);
        fb_login = (CardView) v.findViewById(R.id.fb_login);
        forgetPass = (TextView) v.findViewById(R.id.forgetPass);

        login.setOnClickListener(this);
        signup.setOnClickListener(this);
        fb_login.setOnClickListener(this);
        forgetPass.setOnClickListener(this);

        return v;
    }

    @Override
    public void onClick(View view) {
        if (view == login) {

        } else if (view == signup) {
            Intent i = new Intent(getActivity(), SignUpActivity.class);
            startActivity(i);
        } else if (view == fb_login) {

        } else if (view == forgetPass) {
            Intent i = new Intent(getActivity(), ForgetPasswordActivity.class);
            startActivity(i);
        }
    }
}
