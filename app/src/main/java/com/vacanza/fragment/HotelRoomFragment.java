package com.vacanza.fragment;


import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vacanza.activity.HotelDetailsActivity;

import example.com.hotelbooking.R;

/**
 * A simple {@link android.support.v4.app.Fragment} subclass.
 */
public class HotelRoomFragment extends Fragment {

    private TextView roomCategory, boardType, currencyTxt, price;

    // data not deleted
    private String board_type, room_category, room_rate, room_process_id, currency;
    private int pos;
    private Activity mActivity;
    private LinearLayout book, cancellationPolicy;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.hotel_room_fragment, container, false);
        roomCategory = (TextView) view.findViewById(R.id.roomCategory);
        boardType = (TextView) view.findViewById(R.id.boardType);
        currencyTxt = (TextView) view.findViewById(R.id.currency);
        price = (TextView) view.findViewById(R.id.price);
        book = (LinearLayout) view.findViewById(R.id.book);
        cancellationPolicy = (LinearLayout) view.findViewById(R.id.cancellationPolicy);

        Bundle b = getArguments();
        board_type = b.getString("board_type");
        room_category = b.getString("room_category");
        room_rate = b.getString("room_rate");
        room_process_id = b.getString("room_process_id");
        currency = b.getString("currency");
        pos = b.getInt("pos");

        ((HotelDetailsActivity) getActivity()).setHtmlAsText(roomCategory, room_category);
        ((HotelDetailsActivity) getActivity()).setHtmlAsText(boardType, board_type);

        price.setText(room_rate);
        currencyTxt.setText(currency);

        cancellationPolicy.setTag(R.string.key, room_process_id);
        cancellationPolicy.setOnClickListener((HotelDetailsActivity) getActivity());

        book.setTag(R.string.key, board_type);
        book.setTag(R.string.data, room_category);
        book.setTag(R.string.value, room_rate);
        book.setTag(R.string.set, room_process_id);
        book.setOnClickListener((HotelDetailsActivity) getActivity());

        return view;
    }


    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        mActivity = activity;
    }

}
