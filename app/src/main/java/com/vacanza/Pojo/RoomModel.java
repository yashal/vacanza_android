package com.vacanza.Pojo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Class contain room data
 */
public class RoomModel implements Parcelable {

    private int roomNo;
    private int numAdults;
    private int numChildren;
    private int ageChild1;
    private int ageChild2;
    private int ageChild3;
    //    private int ageChild4;
//    private int ageChild5;
//    private int ageChild6;
    private int positionChild1;
    private int positionChild2;
    private int positionChild3;
//    private int positionChild4;
//    private int positionChild5;
//    private int positionChild6;

    public int getRoomNo() {
        return roomNo;
    }

    public void setRoomNo(int roomNo) {
        this.roomNo = roomNo;
    }

    public int getNumAdults() {
        return numAdults;
    }

    public void setNumAdults(int numAdults) {
        this.numAdults = numAdults;
    }

    public int getNumChildren() {
        return numChildren;
    }

    public void setNumChildren(int numChildren) {
        this.numChildren = numChildren;
    }

    public int getAgeChild1() {
        return ageChild1;
    }

    public void setAgeChild1(int ageChild1) {
        this.ageChild1 = ageChild1;
    }

    public int getAgeChild2() {
        return ageChild2;
    }

    public void setAgeChild2(int ageChild2) {
        this.ageChild2 = ageChild2;
    }

    public int getAgeChild3() {
        return ageChild3;
    }

    public void setAgeChild3(int ageChild3) {
        this.ageChild3 = ageChild3;
    }

   /* public int getAgeChild4() {
        return ageChild4;
    }

    public void setAgeChild4(int ageChild4) {
        this.ageChild4 = ageChild4;
    }

    public int getAgeChild5() {
        return ageChild5;
    }

    public void setAgeChild5(int ageChild5) {
        this.ageChild5 = ageChild5;
    }

    public int getAgeChild6() {
        return ageChild6;
    }

    public void setAgeChild6(int ageChild6) {
        this.ageChild6 = ageChild6;
    }*/

    public int getPositionChild1() {
        return positionChild1;
    }

    public void setPositionChild1(int positionChild1) {
        this.positionChild1 = positionChild1;
    }

    public int getPositionChild2() {
        return positionChild2;
    }

    public void setPositionChild2(int positionChild2) {
        this.positionChild2 = positionChild2;
    }

    public int getPositionChild3() {
        return positionChild3;
    }

    public void setPositionChild3(int positionChild3) {
        this.positionChild3 = positionChild3;
    }

    /*public int getPositionChild4() {
        return positionChild4;
    }

    public void setPositionChild4(int positionChild4) {
        this.positionChild4 = positionChild4;
    }

    public int getPositionChild5() {
        return positionChild5;
    }

    public void setPositionChild5(int positionChild5) {
        this.positionChild5 = positionChild5;
    }

    public int getPositionChild6() {
        return positionChild6;
    }

    public void setPositionChild6(int positionChild6) {
        this.positionChild6 = positionChild6;
    }*/

    public RoomModel() {

    }

    protected RoomModel(Parcel in) {
        roomNo = in.readInt();
        numAdults = in.readInt();
        numChildren = in.readInt();
        ageChild1 = in.readInt();
        ageChild2 = in.readInt();
        ageChild3 = in.readInt();
//        ageChild4 = in.readInt();
//        ageChild5 = in.readInt();
//        ageChild6 = in.readInt();
        positionChild1 = in.readInt();
        positionChild2 = in.readInt();
        positionChild3 = in.readInt();
//        positionChild4 = in.readInt();
//        positionChild5 = in.readInt();
//        positionChild6 = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(roomNo);
        dest.writeInt(numAdults);
        dest.writeInt(numChildren);
        dest.writeInt(ageChild1);
        dest.writeInt(ageChild2);
        dest.writeInt(ageChild3);
//        dest.writeInt(ageChild4);
//        dest.writeInt(ageChild5);
//        dest.writeInt(ageChild6);
        dest.writeInt(positionChild1);
        dest.writeInt(positionChild2);
        dest.writeInt(positionChild3);
//        dest.writeInt(positionChild4);
//        dest.writeInt(positionChild5);
//        dest.writeInt(positionChild6);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<RoomModel> CREATOR = new Parcelable.Creator<RoomModel>() {
        @Override
        public RoomModel createFromParcel(Parcel in) {
            return new RoomModel(in);
        }

        @Override
        public RoomModel[] newArray(int size) {
            return new RoomModel[size];
        }
    };
}