package com.vacanza.Pojo;

/**
 * Created by Yash on 11/22/2017.
 */

public class CancellationPolicyPojo extends BasePojo {

    private String SearchId;
    private String cancellationMessage;

    public String getSearchId() {
        return SearchId;
    }

    public void setSearchId(String searchId) {
        SearchId = searchId;
    }

    public String getCancellationMessage() {
        return cancellationMessage;
    }

    public void setCancellationMessage(String cancellationMessage) {
        this.cancellationMessage = cancellationMessage;
    }
}
