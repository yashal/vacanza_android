package com.vacanza.Pojo;

import java.util.ArrayList;

/**
 * Created by Yash on 9/19/2017.
 */

public class CityListData extends BasePojo {

    private ArrayList<CityListPojo> city;

    public ArrayList<CityListPojo> getCity() {
        return city;
    }

    public void setCity(ArrayList<CityListPojo> city) {
        this.city = city;
    }
}
