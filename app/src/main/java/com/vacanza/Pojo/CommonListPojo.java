package com.vacanza.Pojo;

/**
 * Created by Yash on 12/1/2017.
 */

public class CommonListPojo {

    private String strcode;
    private String strname;

    public String getStrcode() {
        return strcode;
    }

    public void setStrcode(String strcode) {
        this.strcode = strcode;
    }

    public String getStrname() {
        return strname;
    }

    public void setStrname(String strname) {
        this.strname = strname;
    }
}
