package com.vacanza.Pojo;

/**
 * Created by Yash on 9/19/2017.
 */

public class BasePojo {

    private String Message;
    private String Status;

    public String getIsshowcancellationbuton() {
        return Isshowcancellationbuton;
    }

    public void setIsshowcancellationbuton(String isshowcancellationbuton) {
        Isshowcancellationbuton = isshowcancellationbuton;
    }

    private String Isshowcancellationbuton;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }
}
