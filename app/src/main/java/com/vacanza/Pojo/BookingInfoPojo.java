package com.vacanza.Pojo;

import java.util.ArrayList;

/**
 * Created by zolo13 on 07/01/18.
 */

public class BookingInfoPojo extends BasePojo {

    private String bookingno;
    private String cancellationpolicy;
    private String checkin;
    private String checkout;
    private String currency;
    private String hotelcode;
    private String hoteladdress;
    private String hotelimage;
    private String hotelname;
    private String roomboardtype;
    private String roomcategory;
    private String supplier;
    private String totalprice;
    private String totalrooms;

    private ArrayList<GuestInfoPojo> Paxinfo;

    public String getBookingno() {
        return bookingno;
    }

    public void setBookingno(String bookingno) {
        this.bookingno = bookingno;
    }

    public String getCancellationpolicy() {
        return cancellationpolicy;
    }

    public void setCancellationpolicy(String cancellationpolicy) {
        this.cancellationpolicy = cancellationpolicy;
    }

    public String getCheckin() {
        return checkin;
    }

    public void setCheckin(String checkin) {
        this.checkin = checkin;
    }

    public String getCheckout() {
        return checkout;
    }

    public void setCheckout(String checkout) {
        this.checkout = checkout;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getHotelcode() {
        return hotelcode;
    }

    public void setHotelcode(String hotelcode) {
        this.hotelcode = hotelcode;
    }

    public String getHoteladdress() {
        return hoteladdress;
    }

    public void setHoteladdress(String hoteladdress) {
        this.hoteladdress = hoteladdress;
    }

    public String getHotelimage() {
        return hotelimage;
    }

    public void setHotelimage(String hotelimage) {
        this.hotelimage = hotelimage;
    }

    public String getHotelname() {
        return hotelname;
    }

    public void setHotelname(String hotelname) {
        this.hotelname = hotelname;
    }

    public String getRoomboardtype() {
        return roomboardtype;
    }

    public void setRoomboardtype(String roomboardtype) {
        this.roomboardtype = roomboardtype;
    }

    public String getRoomcategory() {
        return roomcategory;
    }

    public void setRoomcategory(String roomcategory) {
        this.roomcategory = roomcategory;
    }

    public String getTotalprice() {
        return totalprice;
    }

    public void setTotalprice(String totalprice) {
        this.totalprice = totalprice;
    }

    public String getTotalrooms() {
        return totalrooms;
    }

    public void setTotalrooms(String totalrooms) {
        this.totalrooms = totalrooms;
    }

    public ArrayList<GuestInfoPojo> getPaxinfo() {
        return Paxinfo;
    }

    public void setPaxinfo(ArrayList<GuestInfoPojo> paxinfo) {
        Paxinfo = paxinfo;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }

}
