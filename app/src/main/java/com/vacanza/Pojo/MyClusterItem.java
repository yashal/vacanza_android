package com.vacanza.Pojo;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

public class MyClusterItem implements ClusterItem {
    private final LatLng mPosition;
    private String mTitle;
    private String mSnippet;
    private String mHotelCode;
    private String mHotelSupplier;
    private String mCurrency;

    public MyClusterItem(double lat, double lng) {
        mPosition = new LatLng(lat, lng);
    }

    public MyClusterItem(double lat, double lng, String title, String snippet, String mHotelCode, String mHotelSupplier, String mCurrency) {
        mPosition = new LatLng(lat, lng);
        mTitle = title;
        mSnippet = snippet;
        this.mHotelSupplier = mHotelSupplier;
        this.mHotelCode = mHotelCode;
        this.mCurrency = mCurrency;
    }

    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    @Override
    public String getTitle() {
        return mTitle;
    }

    @Override
    public String getSnippet() {
        return mSnippet;
    }

    public String getmHotelCode() {
        return mHotelCode;
    }

    public String getmHotelSupplier() {
        return mHotelSupplier;
    }

    public String getmCurrency() {
        return mCurrency;
    }
}