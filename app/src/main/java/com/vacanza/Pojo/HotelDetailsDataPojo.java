package com.vacanza.Pojo;

import java.util.ArrayList;

/**
 * Created by Yash on 11/17/2017.
 */

public class HotelDetailsDataPojo extends BasePojo {

    private String SearchId;
    private String hoteladdress;
    private String hoteldescription;
    private String hoteldestination;
    private String hotelname;
    private String starrating;
    private ArrayList<HotelFacePojo> Hotelfac;
    private ArrayList<HotelRoomPojo> HotelRooms;
    private ArrayList<HotelImagesPojo> HotelImages;

    public String getSearchId() {
        return SearchId;
    }

    public void setSearchId(String searchId) {
        SearchId = searchId;
    }

    public String getHoteladdress() {
        return hoteladdress;
    }

    public void setHoteladdress(String hoteladdress) {
        this.hoteladdress = hoteladdress;
    }

    public String getHoteldescription() {
        return hoteldescription;
    }

    public void setHoteldescription(String hoteldescription) {
        this.hoteldescription = hoteldescription;
    }

    public String getHoteldestination() {
        return hoteldestination;
    }

    public void setHoteldestination(String hoteldestination) {
        this.hoteldestination = hoteldestination;
    }

    public String getHotelname() {
        return hotelname;
    }

    public void setHotelname(String hotelname) {
        this.hotelname = hotelname;
    }

    public String getStarrating() {
        return starrating;
    }

    public void setStarrating(String starrating) {
        this.starrating = starrating;
    }

    public ArrayList<HotelFacePojo> getHotelfac() {
        return Hotelfac;
    }

    public void setHotelfac(ArrayList<HotelFacePojo> hotelfac) {
        Hotelfac = hotelfac;
    }

    public ArrayList<HotelRoomPojo> getHotelRooms() {
        return HotelRooms;
    }

    public void setHotelRooms(ArrayList<HotelRoomPojo> hotelRooms) {
        HotelRooms = hotelRooms;
    }

    public ArrayList<HotelImagesPojo> getHotelImages() {
        return HotelImages;
    }

    public void setHotelImages(ArrayList<HotelImagesPojo> hotelImages) {
        HotelImages = hotelImages;
    }
}
