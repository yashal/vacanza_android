package com.vacanza.Pojo;

import java.io.Serializable;

/**
 * Created by Yash on 10/25/2017.
 */

public class HotelDataPojo implements Serializable {

    private String HotelAddress;
    private String HotelImg;
    private String HotelName;
    private String HotelRegion;
    private String boardType;
    private String currency;
    private String hotelCode;
    private String processId;
    private String roomCategory;
    private String star;
    private String totalPrice;
    private String Latitude;
    private String Longitude;
    private String supplier;

    public String getHotelAddress() {
        return HotelAddress;
    }

    public void setHotelAddress(String hotelAddress) {
        HotelAddress = hotelAddress;
    }

    public String getHotelImg() {
        return HotelImg;
    }

    public void setHotelImg(String hotelImg) {
        HotelImg = hotelImg;
    }

    public String getHotelName() {
        return HotelName;
    }

    public void setHotelName(String hotelName) {
        HotelName = hotelName;
    }

    public String getHotelRegion() {
        return HotelRegion;
    }

    public void setHotelRegion(String hotelRegion) {
        HotelRegion = hotelRegion;
    }

    public String getBoardType() {
        return boardType;
    }

    public void setBoardType(String boardType) {
        this.boardType = boardType;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getHotelCode() {
        return hotelCode;
    }

    public void setHotelCode(String hotelCode) {
        this.hotelCode = hotelCode;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getRoomCategory() {
        return roomCategory;
    }

    public void setRoomCategory(String roomCategory) {
        this.roomCategory = roomCategory;
    }

    public String getStar() {
        return star;
    }

    public void setStar(String star) {
        this.star = star;
    }

    public String getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(String totalPrice) {
        this.totalPrice = totalPrice;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getSupplier() {
        return supplier;
    }

    public void setSupplier(String supplier) {
        this.supplier = supplier;
    }
}
