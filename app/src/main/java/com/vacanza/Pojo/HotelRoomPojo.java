package com.vacanza.Pojo;

/**
 * Created by Yash on 11/17/2017.
 */

public class HotelRoomPojo {

    private String BoardType;
    private String RoomOption;
    private String hotelcode;
    private String paxes;
    private String processId;
    private String roomCategory;
    private String totalRoomRate;

    public String getBoardType() {
        return BoardType;
    }

    public void setBoardType(String boardType) {
        BoardType = boardType;
    }

    public String getRoomOption() {
        return RoomOption;
    }

    public void setRoomOption(String roomOption) {
        RoomOption = roomOption;
    }

    public String getHotelcode() {
        return hotelcode;
    }

    public void setHotelcode(String hotelcode) {
        this.hotelcode = hotelcode;
    }

    public String getPaxes() {
        return paxes;
    }

    public void setPaxes(String paxes) {
        this.paxes = paxes;
    }

    public String getProcessId() {
        return processId;
    }

    public void setProcessId(String processId) {
        this.processId = processId;
    }

    public String getRoomCategory() {
        return roomCategory;
    }

    public void setRoomCategory(String roomCategory) {
        this.roomCategory = roomCategory;
    }

    public String getTotalRoomRate() {
        return totalRoomRate;
    }

    public void setTotalRoomRate(String totalRoomRate) {
        this.totalRoomRate = totalRoomRate;
    }
}
