package com.vacanza.Pojo;

import java.sql.Array;
import java.util.ArrayList;

/**
 * Created by Yash on 12/1/2017.
 */

public class CountryPojo extends BasePojo {

    private ArrayList<CommonListPojo> commonList;

    public ArrayList<CommonListPojo> getCommonList() {
        return commonList;
    }

    public void setCommonList(ArrayList<CommonListPojo> commonList) {
        this.commonList = commonList;
    }
}
