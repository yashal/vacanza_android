package com.vacanza.Pojo;

import java.util.ArrayList;

/**
 * Created by Yash on 11/30/2017.
 */

public class SearchRoomPojo {

    private String Adult;
    private String Child;
    private ArrayList<ChildAgePojo> childAges;
    private ArrayList<AdultPojo> adultPojo;
    private ArrayList<AdultPojo> childPojo;

    public ArrayList<AdultPojo> getAdultPojo() {
        return adultPojo;
    }

    public void setAdultPojo(ArrayList<AdultPojo> adultPojo) {
        this.adultPojo = adultPojo;
    }

    public ArrayList<AdultPojo> getChildPojo() {
        return childPojo;
    }

    public void setChildPojo(ArrayList<AdultPojo> childPojo) {
        this.childPojo = childPojo;
    }

    public String getAdult() {
        return Adult;
    }

    public void setAdult(String adult) {
        Adult = adult;
    }

    public String getChild() {
        return Child;
    }

    public void setChild(String child) {
        Child = child;
    }

    public ArrayList<ChildAgePojo> getChildAges() {
        return childAges;
    }

    public void setChildAges(ArrayList<ChildAgePojo> childAges) {
        this.childAges = childAges;
    }
}
