package com.vacanza.Pojo;

/**
 * Created by Yash on 11/17/2017.
 */

public class HotelFacePojo {

    private String facilityname;

    public String getFacilityname() {
        return facilityname;
    }

    public void setFacilityname(String facilityname) {
        this.facilityname = facilityname;
    }
}
