package com.vacanza.Pojo;

/**
 * Created by zolo13 on 18/02/18.
 */

public class SendBookingInfoPojo extends BasePojo {

    private String failure_endpoint;
    private String failure_url;
    private String success_endpoint;
    private String success_url;

    public String getFailure_endpoint() {
        return failure_endpoint;
    }

    public void setFailure_endpoint(String failure_endpoint) {
        this.failure_endpoint = failure_endpoint;
    }

    public String getFailure_url() {
        return failure_url;
    }

    public void setFailure_url(String failure_url) {
        this.failure_url = failure_url;
    }

    public String getSuccess_endpoint() {
        return success_endpoint;
    }

    public void setSuccess_endpoint(String success_endpoint) {
        this.success_endpoint = success_endpoint;
    }

    public String getSuccess_url() {
        return success_url;
    }

    public void setSuccess_url(String success_url) {
        this.success_url = success_url;
    }
}
