package com.vacanza.Pojo;

import java.util.ArrayList;

/**
 * Created by shubhbam on 1/11/2018.
 */

public class ManageBookingPojoDetail extends BasePojo {

    private ArrayList<MangeBookingInfoPojo> commonList;

    public ArrayList<MangeBookingInfoPojo> getCommonList() {
        return commonList;
    }

    public void setCommonList(ArrayList<MangeBookingInfoPojo> commonList) {
        this.commonList = commonList;
    }


}
