package com.vacanza.Pojo;

/**
 * Created by shubhbam on 1/11/2018.
 */

public class MangeBookingInfoPojo {

    private String strcode;
    private String strname;

    public String getStrcode() {
        return strcode;
    }

    public void setStrcode(String strcode) {
        this.strcode = strcode;
    }

    public String getStrname() {
        return strname;
    }

    public void setStrname(String strname) {
        this.strname = strname;
    }


}
