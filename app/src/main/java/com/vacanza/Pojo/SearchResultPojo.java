package com.vacanza.Pojo;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by Yash on 10/25/2017.
 */

public class SearchResultPojo extends BasePojo implements Serializable {

    private String SearchId;
    private ArrayList<HotelDataPojo> Hotels;

    public String getSearchId() {
        return SearchId;
    }

    public void setSearchId(String searchId) {
        SearchId = searchId;
    }

    public ArrayList<HotelDataPojo> getHotels() {
        return Hotels;
    }

    public void setHotels(ArrayList<HotelDataPojo> hotels) {
        Hotels = hotels;
    }
}
