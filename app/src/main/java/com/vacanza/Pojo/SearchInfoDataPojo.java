package com.vacanza.Pojo;

import java.util.ArrayList;

/**
 * Created by Yash on 11/28/2017.
 */

public class SearchInfoDataPojo {

    private String CheckInDate;
    private String CheckOutDate;
    private String Cityname;
    private String Currency;
    private String Nationality;
    private String NoOfRooms;
    private String SearchId;
    private ArrayList<SearchRoomPojo> searchRoom;

    public String getCheckInDate() {
        return CheckInDate;
    }

    public void setCheckInDate(String checkInDate) {
        CheckInDate = checkInDate;
    }

    public String getCheckOutDate() {
        return CheckOutDate;
    }

    public void setCheckOutDate(String checkOutDate) {
        CheckOutDate = checkOutDate;
    }

    public String getCityname() {
        return Cityname;
    }

    public void setCityname(String cityname) {
        Cityname = cityname;
    }

    public String getCurrency() {
        return Currency;
    }

    public void setCurrency(String currency) {
        Currency = currency;
    }

    public String getNationality() {
        return Nationality;
    }

    public void setNationality(String nationality) {
        Nationality = nationality;
    }

    public String getNoOfRooms() {
        return NoOfRooms;
    }

    public void setNoOfRooms(String noOfRooms) {
        NoOfRooms = noOfRooms;
    }

    public String getSearchId() {
        return SearchId;
    }

    public void setSearchId(String searchId) {
        SearchId = searchId;
    }

    public ArrayList<SearchRoomPojo> getSearchRoom() {
        return searchRoom;
    }

    public void setSearchRoom(ArrayList<SearchRoomPojo> searchRoom) {
        this.searchRoom = searchRoom;
    }
}
