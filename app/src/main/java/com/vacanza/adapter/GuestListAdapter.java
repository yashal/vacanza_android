package com.vacanza.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.vacanza.Pojo.GuestInfoPojo;
import com.vacanza.activity.BookingInfoActivity;

import java.util.ArrayList;

import example.com.hotelbooking.R;

/**
 * Created by pr0 on 1/8/17.
 */

public class GuestListAdapter extends RecyclerView.Adapter<GuestListAdapter.ViewHolder> {

    private BookingInfoActivity context;
    private ArrayList<GuestInfoPojo> resultdata;
    private String firstName = "", lastNmae = "", title = "";

    public GuestListAdapter(BookingInfoActivity context, ArrayList<GuestInfoPojo> resultdata) {
        this.context = context;
        this.resultdata = resultdata;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_guest_name, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final GuestInfoPojo data = resultdata.get(position);
        if (data.getTitle() != null && !data.getTitle().isEmpty()) {
            title = data.getTitle();
        }
        if (data.getFirstname() != null && !data.getFirstname().isEmpty()) {
            firstName = data.getFirstname();
        }
        if (data.getLastname() != null && !data.getLastname().isEmpty()) {
            lastNmae = data.getLastname();
        }

        holder.name.setText(title + " " + firstName + " " + lastNmae);

    }

    @Override
    public int getItemCount() {
        return resultdata.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView name;

        public ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.name);
        }
    }
}
