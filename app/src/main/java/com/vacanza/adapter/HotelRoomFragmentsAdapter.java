package com.vacanza.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.vacanza.Pojo.HotelRoomPojo;
import com.vacanza.activity.HotelDetailsActivity;
import com.vacanza.fragment.HotelRoomFragment;

import java.util.ArrayList;


public class HotelRoomFragmentsAdapter extends FragmentPagerAdapter {

    private HotelDetailsActivity fragmentActivity;
    private ArrayList<HotelRoomPojo> details;
    private String currency;

    public HotelRoomFragmentsAdapter(FragmentManager fm, HotelDetailsActivity fragmentActivity, ArrayList<HotelRoomPojo> details, String currency) {
        super(fm);
        this.fragmentActivity = fragmentActivity;
        this.details = details;
        this.currency = currency;

    }

    @Override
    public Fragment getItem(int position) {
        HotelRoomFragment f = new HotelRoomFragment();
        Bundle b = new Bundle();
        b.putInt("pos", position);
        b.putString("board_type", details.get(position).getBoardType());
        b.putString("room_category", details.get(position).getRoomCategory());
        b.putString("room_rate", details.get(position).getTotalRoomRate());
        b.putString("room_process_id", details.get(position).getProcessId());
        b.putString("currency", currency);
        f.setArguments(b);
        return f;
    }

    @Override
    public int getItemPosition(Object item) {
        return POSITION_NONE;
    }

    @Override
    public int getCount() {
        return details.size();
    }
}
