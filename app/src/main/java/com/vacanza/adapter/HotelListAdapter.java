package com.vacanza.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import com.vacanza.Pojo.HotelDataPojo;
import com.vacanza.activity.BaseActivity;

import example.com.hotelbooking.R;

import com.vacanza.activity.SearchResultsActivity;
import com.vacanza.utils.AppConstants;

/**
 * Created by pr0 on 1/8/17.
 */

public class HotelListAdapter extends RecyclerView.Adapter<HotelListAdapter.ViewHolder> {

    private SearchResultsActivity context;
    private ArrayList<HotelDataPojo> resultdata;

    public HotelListAdapter(SearchResultsActivity context, ArrayList<HotelDataPojo> resultdata) {
        this.context = context;
        this.resultdata = resultdata;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_hotel_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final HotelDataPojo data = resultdata.get(position);
        if (data.getStar() != null && !data.getStar().isEmpty()) {
            holder.ratings.setRating(Float.parseFloat(data.getStar()));
        }
        else
        {
            holder.ratings.setRating(0);
        }

        if (data.getHotelName() != null && !data.getHotelName().isEmpty()) {
            holder.hotelName.setText(data.getHotelName());
        }
        else
        {
            holder.hotelName.setText("");
        }
        if (data.getHotelAddress() != null && !data.getHotelAddress().isEmpty()) {
            holder.address.setText(data.getHotelAddress() + " " + ((SearchResultsActivity) context).getFromPrefs(AppConstants.CITY_NAME));
        }
        else
        {
            holder.address.setText("");
        }
        if (data.getBoardType() != null && !data.getBoardType().isEmpty()) {
            holder.amenities.setText(data.getBoardType());
        }
        else
        {
            holder.amenities.setText("");
        }
        if (data.getCurrency() != null && !data.getCurrency().isEmpty()) {
            holder.currency.setText(data.getCurrency());
        }
        else
        {
            holder.currency.setText("");
        }
        if (data.getTotalPrice() != null && !data.getTotalPrice().isEmpty()) {
            holder.price.setText(data.getTotalPrice());
        }
        else
        {
            holder.price.setText("");
        }
        ((BaseActivity) context).setImageInLayout(context, (int) context.getResources().getDimension(R.dimen.pg_image_width), (int) context.getResources().getDimension(R.dimen.pg_image_height), data.getHotelImg(), holder.hotel_img);

        holder.rootLayout.setTag(R.string.key, data.getHotelCode());
        holder.rootLayout.setTag(R.string.data, data.getSupplier());
        holder.rootLayout.setTag(R.string.value, data.getHotelImg());
        holder.rootLayout.setTag(R.string.set, data.getCurrency());
        holder.rootLayout.setOnClickListener((SearchResultsActivity) context);
    }

    @Override
    public int getItemCount() {
        return resultdata.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView hotelName, address, amenities, price, currency;
        private RatingBar ratings;
        private ImageView hotel_img;
        private LinearLayout rootLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            hotelName = (TextView) itemView.findViewById(R.id.hotelName);
            address = (TextView) itemView.findViewById(R.id.address);
            amenities = (TextView) itemView.findViewById(R.id.amenities);
            price = (TextView) itemView.findViewById(R.id.price);
            currency = (TextView) itemView.findViewById(R.id.currency);
            hotel_img = (ImageView) itemView.findViewById(R.id.hotel_img);
            ratings = (RatingBar) itemView.findViewById(R.id.ratings);
            rootLayout = (LinearLayout) itemView.findViewById(R.id.rootLayout);
        }
    }
}
