package com.vacanza.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

import com.vacanza.Pojo.CityListPojo;

import example.com.hotelbooking.R;


public class CustomAutoCompleteAdapter extends BaseAdapter implements
        Filterable {
    // Main data structure
    private ArrayList<CityListPojo> data;
    private ArrayList<CityListPojo> dataBackup = null;
    private Context ctx;
    private CustomCardListAdapterFilter adapterFilter;

    public CustomAutoCompleteAdapter(ArrayList<CityListPojo> data, Context ctx) {
        this.data = data;
        this.dataBackup = data;
        this.ctx = ctx;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int pos) {
        return data.get(pos);
    }

    @Override
    public long getItemId(int pos) {
        return pos;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getView(int pos, View view, ViewGroup vg) {
        View v = view;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) ctx.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(R.layout.tv_dropdown, null);
        }

        TextView city_name = (TextView) v.findViewById(R.id.text);
        city_name.setTag("htag");
        city_name.setText(data.get(pos).getCityName());

        return v;
    }

    public Filter getFilter() {
        if (adapterFilter == null)
            adapterFilter = new CustomCardListAdapterFilter();
        return adapterFilter;
    }

    // Class enabling the filtering of this adapter
    private class CustomCardListAdapterFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            if (constraint == null || constraint.length() == 0) {
                results.values = dataBackup;
                results.count = dataBackup.size();
            } else {
                ArrayList<CityListPojo> filteredCardList = new ArrayList<CityListPojo>();
                for (CityListPojo card : dataBackup) {
                    if (card.getCityName().toLowerCase(Locale.getDefault()).startsWith(constraint.toString().toLowerCase(Locale.getDefault()))) {
                        filteredCardList.add(card);
                    }
                }
                results.values = filteredCardList;
                results.count = filteredCardList.size();
            }
            return results;
        }

        @Override
        protected void publishResults(CharSequence constraint,
                                      FilterResults results) {
            if (results.count == 0)
                notifyDataSetInvalidated();
            else {
                data = (ArrayList<CityListPojo>) results.values;
                notifyDataSetChanged();
            }
        }
    }
}