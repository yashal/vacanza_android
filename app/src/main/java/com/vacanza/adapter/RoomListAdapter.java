package com.vacanza.adapter;

import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.Html;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vacanza.Pojo.AdultPojo;
import com.vacanza.Pojo.SearchRoomPojo;
import com.vacanza.activity.BucketActivity;
import com.vacanza.utils.AppConstants;

import java.util.ArrayList;

import example.com.hotelbooking.R;

/**
 * Created by pr0 on 1/8/17.
 */

public class RoomListAdapter extends RecyclerView.Adapter<RoomListAdapter.ViewHolder> {

    private BucketActivity context;
    private ArrayList<SearchRoomPojo> resultdata;


    public RoomListAdapter(BucketActivity context, ArrayList<SearchRoomPojo> resultdata) {
        this.context = context;
        this.resultdata = resultdata;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.layout_room_list, parent, false);
        ViewHolder viewHolder = new ViewHolder(v);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final SearchRoomPojo data = resultdata.get(position);
        final ArrayList<AdultPojo> adultPojo = data.getAdultPojo();
        final ArrayList<AdultPojo> childPojo = data.getChildPojo();
        holder.room_no.setText("Room " + (position + 1));
        int adult_count = 0, child_count = 0;

        if (data.getAdult() != null && !data.getAdult().isEmpty()) {
            adult_count = Integer.parseInt(data.getAdult());
        }
        if (data.getChild() != null && !data.getChild().isEmpty()) {
            child_count = Integer.parseInt(data.getChild());
        }
        /*if (position == 0)
        {
            holder.email_adult1.setVisibility(View.VISIBLE);
            holder.name_adult1.setText(((BucketActivity)context).getFromPrefs(AppConstants.NAME));
            holder.email_adult1.setText(((BucketActivity)context).getFromPrefs(AppConstants.EMAIL));
        }
        else
        {
            holder.email_adult1.setVisibility(View.GONE);
        }*/

        for (int i = 0; i < adult_count; i++) {
            LinearLayout linearLayout_top = new LinearLayout(context);
            LinearLayout.LayoutParams lpRoot_top = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lpRoot_top.setMargins(0, 5, 0, 5);
            linearLayout_top.setLayoutParams(lpRoot_top);
            linearLayout_top.setOrientation(LinearLayout.VERTICAL);


            LinearLayout linearLayout_name = new LinearLayout(context);
            LinearLayout.LayoutParams lpRoot_name = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lpRoot_name.setMargins(0, 3, 0, 3);
            linearLayout_name.setLayoutParams(lpRoot_name);
            linearLayout_name.setOrientation(LinearLayout.HORIZONTAL);

            EditText et1_title = new EditText(context);
            LinearLayout.LayoutParams lp1_adult = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f);
            lp1_adult.setMargins(0, 0, 5, 0);
            et1_title.setPadding(15, 5, 15, 5);
            et1_title.setLayoutParams(lp1_adult);
            et1_title.setBackground(context.getResources().getDrawable(R.drawable.edittext_bg));
            et1_title.setHint(context.getResources().getString(R.string.adult_title_hint));
            et1_title.setHint(Html.fromHtml("<small>"+ context.getResources().getString(R.string.adult_title_hint) + "</small>"));
            et1_title.setSingleLine(true);

            EditText et2_first_name = new EditText(context);
            LinearLayout.LayoutParams lp2_adult = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.5f);
            lp2_adult.setMargins(5, 0, 5, 0);
            et2_first_name.setPadding(15, 5, 15, 5);
            et2_first_name.setLayoutParams(lp2_adult);
            et2_first_name.setHint(Html.fromHtml("<small>"+ context.getResources().getString(R.string.adult_first_name) + "</small>"));
            et2_first_name.setSingleLine(true);
            et2_first_name.setBackground(context.getResources().getDrawable(R.drawable.edittext_bg));


            EditText et3_last_name = new EditText(context);
            LinearLayout.LayoutParams lp3_adult = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.5f);
            lp3_adult.setMargins(5, 0, 0, 0);
            et3_last_name.setPadding(15, 5, 15, 5);
            et3_last_name.setLayoutParams(lp3_adult);
            et3_last_name.setBackground(context.getResources().getDrawable(R.drawable.edittext_bg));
            et3_last_name.setHint(Html.fromHtml("<small>"+ context.getResources().getString(R.string.adult_last_name) + "</small>"));
            et3_last_name.setSingleLine(true);

            linearLayout_name.addView(et1_title);
            linearLayout_name.addView(et2_first_name);
            linearLayout_name.addView(et3_last_name);


            LinearLayout linearLayout_email = new LinearLayout(context);
            LinearLayout.LayoutParams lpRoot_email = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lpRoot_email.setMargins(0, 5, 0, 3);
            linearLayout_email.setLayoutParams(lpRoot_email);
            linearLayout_email.setOrientation(LinearLayout.HORIZONTAL);

            EditText et4_email = new EditText(context);
            LinearLayout.LayoutParams lp4_email = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 1.0f);
            lp4_email.setMargins(0, 0, 0, 0);
            et4_email.setPadding(15, 5, 15, 5);
            et4_email.setLayoutParams(lp4_email);
            et4_email.setBackground(context.getResources().getDrawable(R.drawable.edittext_bg));
            et4_email.setHint(Html.fromHtml("<small>"+ context.getResources().getString(R.string.email) + "</small>"));
            et4_email.setSingleLine(true);

            linearLayout_email.addView(et4_email);


            if (position == 0 && i == 0) {
                et4_email.setVisibility(View.VISIBLE);
                if (!((BucketActivity) context).getFromPrefs(AppConstants.NAME).isEmpty()) {
                    et2_first_name.setText(((BucketActivity) context).getFromPrefs(AppConstants.FIRST_NAME));
                    et3_last_name.setText(((BucketActivity) context).getFromPrefs(AppConstants.LAST_NAME));
                    et4_email.setText(((BucketActivity) context).getFromPrefs(AppConstants.EMAIL));
                    if (et2_first_name.getText().toString().trim().length() > 0)
                    {
                        adultPojo.get(0).setFirstName(et2_first_name.getText().toString().trim());
                    }
                    if (et3_last_name.getText().toString().trim().length() > 0)
                    {
                        adultPojo.get(0).setLastName(et3_last_name.getText().toString().trim());
                    }
                    if (et4_email.getText().toString().trim().length() > 0)
                    {
                        adultPojo.get(0).setEmail(et4_email.getText().toString().trim());
                    }

                }

            } else {
                et4_email.setVisibility(View.GONE);
            }
            linearLayout_top.addView(linearLayout_name);
            linearLayout_top.addView(linearLayout_email);

            holder.layout_adult1.addView(linearLayout_top);


            final int finalI = i;


            et1_title.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    adultPojo.get(finalI).setTitle(s.toString());

                }
            });


            et2_first_name.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    adultPojo.get(finalI).setFirstName(s.toString());

                }
            });

            et3_last_name.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    adultPojo.get(finalI).setLastName(s.toString());

                }
            });

            et4_email.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    adultPojo.get(finalI).setEmail(s.toString());
                }
            });


        }

//        **************************
        for (int i = 0; i < child_count; i++) {

            LinearLayout linearLayout_top_child = new LinearLayout(context);
            LinearLayout.LayoutParams lpRoot_top_child = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lpRoot_top_child.setMargins(0, 5, 0, 5);
            linearLayout_top_child.setLayoutParams(lpRoot_top_child);
            linearLayout_top_child.setOrientation(LinearLayout.VERTICAL);

            LinearLayout linearLayout_child = new LinearLayout(context);
            LinearLayout.LayoutParams lpRoot_child = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            lpRoot_child.setMargins(0, 3, 0, 5);
            linearLayout_child.setLayoutParams(lpRoot_child);
            linearLayout_child.setOrientation(LinearLayout.HORIZONTAL);

            EditText et1_first_name = new EditText(context);
            LinearLayout.LayoutParams lp1_child = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.5f);
            lp1_child.setMargins(0, 0, 5, 0);
            et1_first_name.setPadding(15, 5, 15, 5);
            et1_first_name.setLayoutParams(lp1_child);
            et1_first_name.setBackground(context.getResources().getDrawable(R.drawable.edittext_bg));
            et1_first_name.setHint(Html.fromHtml("<small>"+ context.getResources().getString(R.string.child_first_name) + "</small>"));
            et1_first_name.setSingleLine(true);


            EditText et2_last_name = new EditText(context);
            LinearLayout.LayoutParams lp2_child = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.5f);
            lp2_child.setMargins(5, 0, 5, 0);
            et2_last_name.setPadding(15, 5, 15, 5);
            et2_last_name.setLayoutParams(lp2_child);
            et2_last_name.setBackground(context.getResources().getDrawable(R.drawable.edittext_bg));
            et2_last_name.setHint(Html.fromHtml("<small>"+ context.getResources().getString(R.string.child_last_name) + "</small>"));
            et2_last_name.setSingleLine(true);

            EditText et3_age = new EditText(context);
            LinearLayout.LayoutParams lp3_child = new LinearLayout.LayoutParams(0, ViewGroup.LayoutParams.WRAP_CONTENT, 0.3f);
            lp3_child.setMargins(5, 0, 0, 0);
            et3_age.setPadding(15, 5, 15, 5);
            et3_age.setLayoutParams(lp3_child);
            et3_age.setBackground(context.getResources().getDrawable(R.drawable.edittext_bg));
            et3_age.setHint(Html.fromHtml("<small>"+ context.getResources().getString(R.string.age) + "</small>"));
            et3_age.setSingleLine(true);
            et3_age.setInputType(InputType.TYPE_CLASS_NUMBER);

            linearLayout_child.addView(et1_first_name);
            linearLayout_child.addView(et2_last_name);
            linearLayout_child.addView(et3_age);
            linearLayout_top_child.addView(linearLayout_child);

            holder.layout_child1.addView(linearLayout_top_child);

            final int finalI = i;
            et1_first_name.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    childPojo.get(finalI).setFirstName(s.toString());
                }
            });

            et2_last_name.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    childPojo.get(finalI).setLastName(s.toString());
                }
            });

            et3_age.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                    childPojo.get(finalI).setAge(s.toString());
                }
            });

        }

    }

    @Override
    public int getItemCount() {
        return resultdata.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private TextView room_no;
        private LinearLayout layout_adult1, layout_child1;

        public ViewHolder(View itemView) {
            super(itemView);
            room_no = (TextView) itemView.findViewById(R.id.room_no);

            layout_adult1 = (LinearLayout) itemView.findViewById(R.id.layout_adult1);
            layout_child1 = (LinearLayout) itemView.findViewById(R.id.layout_child1);

        }
    }
}
