package com.vacanza.adapter;


import android.content.Context;
import android.support.v7.widget.AppCompatSpinner;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import com.vacanza.Pojo.RoomModel;

import example.com.hotelbooking.R;

import com.vacanza.controller.FmsEventBus;
import com.vacanza.utils.AppConstants;

/**
 * Adapter to show list of rooms with a maximum of 4 rooms
 */
public class RoomAdapter extends BaseAdapter {

    private final Context mContext;
    private final ArrayAdapter<CharSequence> mChildAgeAdapter;
    private ArrayList<RoomModel> mRoomModels;


    public RoomAdapter(Context context, ArrayList<RoomModel> roomModels) {
        mContext = context;
        mRoomModels = roomModels;

        /** Initiate Spinner **/
        mChildAgeAdapter = ArrayAdapter.createFromResource(mContext, R.array.Age_of_Child,
                R.layout.spinner_item);
        mChildAgeAdapter.setDropDownViewResource(R.layout.spinner_item);
    }

    @Override
    public int getCount() {
        return mRoomModels.size();
    }

    @Override
    public Object getItem(int position) {
        return mRoomModels.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public void notifyDataSetChanged(ArrayList<RoomModel> roomModels) {
        mRoomModels = roomModels;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Viewholder viewholder;
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_rooms, parent, false);

            viewholder = new Viewholder();
            viewholder.textview_room_num = (TextView) convertView.findViewById(R.id.text_room_num);
            viewholder.textview_num_adults = (TextView) convertView.findViewById(R.id.text_num_adult);
            viewholder.textview_num_child = (TextView) convertView.findViewById(R.id.text_num_child);
            viewholder.image_add_adult = (ImageView) convertView.findViewById(R.id.image_add_adult);
            viewholder.image_add_child = (ImageView) convertView.findViewById(R.id.image_add_child);
            viewholder.image_minus_adult = (ImageView) convertView.findViewById(R.id.image_minus_adult);
            viewholder.image_minus_child = (ImageView) convertView.findViewById(R.id.image_minus_child);
            viewholder.spinner_child_one = (AppCompatSpinner) convertView.findViewById(R.id.spinner_child_one);
            viewholder.spinner_child_two = (AppCompatSpinner) convertView.findViewById(R.id.spinner_child_two);
            viewholder.spinner_child_three = (AppCompatSpinner) convertView.findViewById(R.id.spinner_child_three);
            viewholder.layout_child_one = convertView.findViewById(R.id.layout_child_one);
            viewholder.layout_child_two = convertView.findViewById(R.id.layout_child_two);
            viewholder.layout_child_three = convertView.findViewById(R.id.layout_child_three);

            convertView.setTag(viewholder);

        } else {
            viewholder = (Viewholder) convertView.getTag();
        }

        setRoom(viewholder, mRoomModels.get(position));
        return convertView;
    }

    private void setRoom(final Viewholder viewholder, final RoomModel roomModel) {
        viewholder.textview_room_num.setText("Room " + roomModel.getRoomNo());
        viewholder.textview_num_adults.setText(String.valueOf(roomModel.getNumAdults()));
        viewholder.textview_num_child.setText(String.valueOf(roomModel.getNumChildren()));

        showHideViews(viewholder, roomModel);

        viewholder.spinner_child_one.setAdapter(mChildAgeAdapter);
        viewholder.spinner_child_two.setAdapter(mChildAgeAdapter);
        viewholder.spinner_child_three.setAdapter(mChildAgeAdapter);

        viewholder.spinner_child_one.setSelection(roomModel.getPositionChild1());
        viewholder.spinner_child_two.setSelection(roomModel.getPositionChild2());
        viewholder.spinner_child_three.setSelection(roomModel.getPositionChild3());

        viewholder.spinner_child_one.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                roomModel.setPositionChild1(position);
                roomModel.setAgeChild1(AppConstants.AGE_OF_CHILD[position]);
                FmsEventBus.getInstance().post(mRoomModels);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        viewholder.spinner_child_two.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                roomModel.setPositionChild2(position);
                roomModel.setAgeChild2(AppConstants.AGE_OF_CHILD[position]);
                FmsEventBus.getInstance().post(mRoomModels);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        viewholder.spinner_child_three.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                roomModel.setPositionChild3(position);
                roomModel.setAgeChild3(AppConstants.AGE_OF_CHILD[position]);
                FmsEventBus.getInstance().post(mRoomModels);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        viewholder.image_add_adult.setOnClickListener(new View.OnClickListener() { // Add adults if adults and child is less than 4
            @Override
            public void onClick(View v) {

                roomModel.setNumAdults(roomModel.getNumAdults() + 1);
                viewholder.textview_num_adults.setText(String.valueOf(roomModel.getNumAdults()));
                showHideViews(viewholder, roomModel);
                FmsEventBus.getInstance().post(mRoomModels);
            }
        });

        viewholder.image_minus_adult.setOnClickListener(new View.OnClickListener() { // Remove adults if adults > 1
            @Override
            public void onClick(View v) {

                roomModel.setNumAdults(roomModel.getNumAdults() - 1);
                viewholder.textview_num_adults.setText(String.valueOf(roomModel.getNumAdults()));
                showHideViews(viewholder, roomModel);
                FmsEventBus.getInstance().post(mRoomModels);
            }
        });

        viewholder.image_add_child.setOnClickListener(new View.OnClickListener() { // Remove adults if adults > 1
            @Override
            public void onClick(View v) {

                roomModel.setNumChildren(roomModel.getNumChildren() + 1);
                viewholder.textview_num_child.setText(String.valueOf(roomModel.getNumChildren()));
                showHideViews(viewholder, roomModel);
                FmsEventBus.getInstance().post(mRoomModels);
            }
        });

        viewholder.image_minus_child.setOnClickListener(new View.OnClickListener() { // Remove adults if adults > 1
            @Override
            public void onClick(View v) {

                roomModel.setNumChildren(roomModel.getNumChildren() - 1);
                viewholder.textview_num_child.setText(String.valueOf(roomModel.getNumChildren()));
                showHideViews(viewholder, roomModel);
                FmsEventBus.getInstance().post(mRoomModels);

            }
        });
    }

    /**
     * Show or hide views based on values
     */
    private void showHideViews(Viewholder viewholder, RoomModel roomModel) {
        //Cases when add and subtract buttons are enabled
        if (roomModel.getNumAdults() > 1) {
            viewholder.image_minus_adult.setEnabled(true);
        }
        if (roomModel.getNumChildren() > 0) {
            viewholder.image_minus_child.setEnabled(true);
        }

        if (roomModel.getNumAdults() + roomModel.getNumChildren() < 4) {
            viewholder.image_add_adult.setEnabled(true);
            viewholder.image_add_child.setEnabled(true);
        }
        if (roomModel.getNumAdults() < 3) {
            viewholder.image_add_adult.setEnabled(true);
        }
        if (roomModel.getNumChildren() < 3) {
            viewholder.image_add_child.setEnabled(true);
        }

        // Cases When add and subtract buttons are disabled
        if (roomModel.getNumAdults() == 1) {
            viewholder.image_minus_adult.setEnabled(false);
        }
        if (roomModel.getNumChildren() == 0) {
            viewholder.image_minus_child.setEnabled(false);
        }
        // comment by yash 13/10/2017
        /*if (roomModel.getNumAdults() + roomModel.getNumChildren() == 4) {
            viewholder.image_add_adult.setEnabled(false);
            viewholder.image_add_child.setEnabled(false);
        }*/
        if (roomModel.getNumAdults() == 6) {
            viewholder.image_add_adult.setEnabled(false);
        }
        if (roomModel.getNumChildren() == 3) {
            viewholder.image_add_child.setEnabled(false);
        }

        /** Show Spinner **/
        if (roomModel.getNumChildren() == 0) {
            roomModel.setAgeChild1(0);
            roomModel.setPositionChild1(0);
            roomModel.setAgeChild2(0);
            roomModel.setPositionChild2(0);
            roomModel.setAgeChild3(0);
            roomModel.setPositionChild3(0);
            viewholder.layout_child_one.setVisibility(View.GONE);
            viewholder.layout_child_two.setVisibility(View.GONE);
            viewholder.layout_child_three.setVisibility(View.GONE);
        } else if (roomModel.getNumChildren() == 1) {
            roomModel.setAgeChild2(0);
            roomModel.setPositionChild2(0);
            roomModel.setAgeChild3(0);
            roomModel.setPositionChild3(0);
            viewholder.layout_child_one.setVisibility(View.VISIBLE);
            viewholder.layout_child_two.setVisibility(View.GONE);
            viewholder.layout_child_three.setVisibility(View.GONE);
        } else if (roomModel.getNumChildren() == 2) {
            roomModel.setAgeChild3(0);
            roomModel.setPositionChild3(0);
            viewholder.layout_child_one.setVisibility(View.VISIBLE);
            viewholder.layout_child_two.setVisibility(View.VISIBLE);
            viewholder.layout_child_three.setVisibility(View.GONE);
        } else if (roomModel.getNumChildren() == 3) {
            viewholder.layout_child_one.setVisibility(View.VISIBLE);
            viewholder.layout_child_two.setVisibility(View.VISIBLE);
            viewholder.layout_child_three.setVisibility(View.VISIBLE);
        }
    }

    private static class Viewholder {
        private TextView textview_room_num;
        private TextView textview_num_adults;
        private TextView textview_num_child;
        private ImageView image_add_adult;
        private ImageView image_add_child;
        private ImageView image_minus_adult;
        private ImageView image_minus_child;
        private View layout_child_one;
        private View layout_child_two;
        private View layout_child_three;
        private AppCompatSpinner spinner_child_one;
        private AppCompatSpinner spinner_child_two;
        private AppCompatSpinner spinner_child_three;
    }

}
