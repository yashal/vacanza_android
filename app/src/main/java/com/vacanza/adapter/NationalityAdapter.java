package com.vacanza.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vacanza.Pojo.CommonListPojo;
import com.vacanza.Pojo.HotelDataPojo;
import com.vacanza.activity.HomeActivity;
import com.vacanza.activity.SearchResultsActivity;

import java.util.ArrayList;

import example.com.hotelbooking.R;


/**
 * Created by Yash on 5/13/2017.
 */

public class NationalityAdapter extends RecyclerView.Adapter<NationalityAdapter.ViewHolder> {
    private ArrayList<CommonListPojo> arr;
    private Context context;
    private int lastPosition = -1;
    private String from;
    private String comingFromActivity;

    public NationalityAdapter(Context context, ArrayList<CommonListPojo> arr, String from, String comingFromActivity) {
        this.arr = arr;
        this.context = context;
        this.from = from;
        this.comingFromActivity = comingFromActivity;
    }

    @Override
    public NationalityAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.location_row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NationalityAdapter.ViewHolder viewHolder, int i) {
        final CommonListPojo data = arr.get(i);
        viewHolder.country_name.setText(data.getStrname());
        viewHolder.nationality.setText(data.getStrcode());

        viewHolder.rootLL.setTag(R.string.key, data.getStrcode());
        viewHolder.rootLL.setTag(R.string.data, from);
        if (comingFromActivity.equalsIgnoreCase("HomeActivity")) {
            viewHolder.rootLL.setOnClickListener((HomeActivity) context);
        }else if (comingFromActivity.equalsIgnoreCase("SearchResultsActivity")) {

            viewHolder.rootLL.setOnClickListener((SearchResultsActivity) context);
        }

//        setAnimation(viewHolder.itemView, i);
    }

    @Override
    public int getItemCount() {
        return arr.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView nationality, country_name;
        private LinearLayout rootLL;

        public ViewHolder(View view) {
            super(view);

            nationality = (TextView) view.findViewById(R.id.nationality);
            country_name = (TextView) view.findViewById(R.id.country_name);
            rootLL = (LinearLayout) view.findViewById(R.id.rootLL);
        }
    }

    /**
     * Here is the key method to apply the animation
     */
    private void setAnimation(View viewToAnimate, int position) {
        // If the bound view wasn't previously displayed on screen, it's animated
        if (position > lastPosition) {
            Animation animation = AnimationUtils.loadAnimation(context,
                    (position > lastPosition) ? R.anim.up_from_bottom
                            : R.anim.down_from_top);
            viewToAnimate.startAnimation(animation);
            lastPosition = position;

        }
    }

}
