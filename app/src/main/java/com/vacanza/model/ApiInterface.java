package com.vacanza.model;


import com.vacanza.Pojo.BasePojo;
import com.vacanza.Pojo.BookingInfoPojo;
import com.vacanza.Pojo.CancellationPolicyPojo;
import com.vacanza.Pojo.CityListData;
import com.vacanza.Pojo.CountryPojo;
import com.vacanza.Pojo.HotelDetailsDataPojo;
import com.vacanza.Pojo.LoginPojo;
import com.vacanza.Pojo.ManageBookingPojoDetail;
import com.vacanza.Pojo.SearchInfoDataPojo;
import com.vacanza.Pojo.SearchResultPojo;
import com.vacanza.Pojo.SendBookingInfoPojo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ApiInterface {

    @GET("getDestination")
    Call<CityListData> getCityList(@Query("value") String value);

    /*@POST("getRXMLHotels?")
    Call<SearchResultPojo> createTask(@Field("value") String value);*/

    @GET("getRXMLHotels")
    Call<SearchResultPojo> getRXMLHotels(@Query("value") String value,@Query("membertype") String membertype);

    @GET("getHBHotels")
    Call<SearchResultPojo> getHBHotels(@Query("value") String value,@Query("membertype") String membertype);

    @GET("getJacHotels")
    Call<SearchResultPojo> getJacHotels(@Query("value") String value,@Query("membertype") String membertype);

    @GET("getDotwHotels")
    Call<SearchResultPojo> getDotwHotels(@Query("value") String value,@Query("membertype") String membertype);

    @GET("getGtaHotels")
    Call<SearchResultPojo> getGtaHotels(@Query("value") String value,@Query("membertype") String membertype);

    @GET("getFITHotels")
    Call<SearchResultPojo> getFITHotels(@Query("value") String value,@Query("membertype") String membertype);

    @GET("forgotPassword")
    Call<BasePojo> forgotPassword(@Query("emailid") String emailid);

    @GET("MemberLogin")
    Call<LoginPojo> doLogin(@Query("emailid") String emailid, @Query("password") String password);

    @GET("Membersignup")
    Call<LoginPojo> doSignUp(@Query("firstname") String firstname, @Query("lastname") String lastname,
                             @Query("membertype") String membertype,@Query("socialid") String socialid,
                             @Query("emailid") String emailid, @Query("phone") String phone,
                             @Query("password") String password);

    @GET("getHotelDetails")
    Call<HotelDetailsDataPojo> getHotelDetails(@Query("hotelcode") String hotelcode, @Query("supplier") String supplier,
                                               @Query("searchid") String searchid);

    @GET("getHotelCancellationPolicy")
    Call<CancellationPolicyPojo> getHotelCancellationPolicy(@Query("hotelcode") String hotelcode, @Query("supplier") String supplier,
                                                            @Query("Searchid") String Searchid, @Query("roomprocessid") String roomprocessid);

    @GET("getSearchInfo")
    Call<SearchInfoDataPojo> getSearchInfo(@Query("value") String value);

    @GET("getcurrency")
    Call<CountryPojo> getcurrency();

    @GET("getNationality")
    Call<CountryPojo> getNationality();

    @GET("sendBookingInfo")
    Call<SendBookingInfoPojo> sendBookingInfo(@Query("paxinfo") String paxinfo, @Query("Hotelcode") String Hotelcode,
                                              @Query("Supplier") String Supplier, @Query("Searchid") String Searchid,
                                              @Query("Processid") String Processid, @Query("cancellationPolicy") String cancellationPolicy,
                                              @Query("currency") String currency);

    @GET("getBookingInfo")
    Call<BookingInfoPojo> getBookingInfo(@Query("searchcode") String searchcode);

    @GET("managebooking")
    Call<ManageBookingPojoDetail> managebooking(@Query("bookingno") String bookingno,
                                                @Query("lastname") String lastname);
    @GET("CancelBooking")
    Call<ManageBookingPojoDetail> CancelBooking(@Query("bookingno") String bookingno,
                                                @Query("supplier") String supplier);

    @GET("filterHotels")
    Call<SearchResultPojo> filterHotels(@Query("searchcode") String searchcode, @Query("hotelname") String hotelname, @Query("star") String star);
}