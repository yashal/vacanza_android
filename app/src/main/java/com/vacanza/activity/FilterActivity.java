package com.vacanza.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import example.com.hotelbooking.R;

/**
 * Created by pr0 on 11/8/17.
 */

public class FilterActivity extends AppCompatActivity implements View.OnClickListener {

    LinearLayout hotel_head, hotel_lay, star_head, star_lay;

    private ImageView arr1, arr3;
    private TextView clear, done;
    private RadioGroup radioGroup;
    private EditText name;
    private String star = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.filter_activity);

        radioGroup = (RadioGroup) findViewById(R.id.radioGroup);
        radioGroup.clearCheck();

        hotel_head = (LinearLayout) findViewById(R.id.hotel_head);
        hotel_lay = (LinearLayout) findViewById(R.id.name_lay);
        name = (EditText) findViewById(R.id.name);

        hotel_head.setOnClickListener(this);

        star_head = (LinearLayout) findViewById(R.id.stars_head);
        star_lay = (LinearLayout) findViewById(R.id.stars_lay);

        star_head.setOnClickListener(this);

        arr1 = (ImageView) findViewById(R.id.arr1);
        arr3 = (ImageView) findViewById(R.id.arr3);

        clear = (TextView) findViewById(R.id.clear);
        done = (TextView) findViewById(R.id.done);

        clear.setOnClickListener(this);
        done.setOnClickListener(this);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton starCategory = (RadioButton) group.findViewById(checkedId);
                if (starCategory != null && checkedId > -1) {
                    String[] splitarr = starCategory.getText().toString().split(" ");
                    if (splitarr != null && splitarr.length > 0)
                    {
                        star = splitarr[0];
                    }
                }

            }
        });

    }

    @Override
    public void onClick(View view) {
        if (view == hotel_head) {
            showHide(hotel_lay, arr1);
        } else if (view == star_head) {
            showHide(star_lay, arr3);
        } else if (view == done) {
            Intent bundle = new Intent();

            bundle.putExtra("hotel_name", name.getText().toString().trim());
            bundle.putExtra("star", star);
            setResult(1, bundle);
            finish();
        }else if (view == clear)
        {
            radioGroup.clearCheck();
            star = "";
            name.setText("");
        }
    }

    private void showHide(LinearLayout layout, ImageView v) {
        if (layout.getVisibility() == View.GONE) {
            layout.setVisibility(View.VISIBLE);
            v.setRotation(90);
        } else {
            layout.setVisibility(View.GONE);
            v.setRotation(-90);
        }
    }
}
