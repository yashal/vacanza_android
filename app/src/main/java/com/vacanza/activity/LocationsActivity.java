package com.vacanza.activity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.TextView;

import java.util.ArrayList;

import com.vacanza.Pojo.CityListData;
import com.vacanza.Pojo.CityListPojo;

import example.com.hotelbooking.R;

import com.vacanza.adapter.CustomAutoCompleteAdapter;
import com.vacanza.model.ApiClient;
import com.vacanza.model.ApiInterface;
import com.vacanza.utils.AppConstants;
import com.vacanza.utils.AppDialogs;
import com.vacanza.utils.ConnectionDetector;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.view.View.VISIBLE;

/**
 * Created by pr0 on 5/8/17.
 */

public class LocationsActivity extends BaseActivity implements View.OnClickListener {
    private AutoCompleteTextView location;
    TextView selectAll, recents, error;
    private ConnectionDetector cd;
    private LocationsActivity ctx = this;
    private AppDialogs dialog;
    private ArrayList<CityListPojo> cityList;
    private int count = 0;
    private boolean city_found;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.locations_activity);

        AppConstants.ACTIVITIES.add(ctx);

        location = (AutoCompleteTextView) findViewById(R.id.location);
        location.setThreshold(3);
        selectAll = (TextView) findViewById(R.id.all);
        error = (TextView) findViewById(R.id.error);
        recents = (TextView) findViewById(R.id.recents);
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new AppDialogs(ctx);
        cityList = new ArrayList<>();

        selectAll.setOnClickListener(this);
        recents.setOnClickListener(this);

        location.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                hideSoftKeyboard();

                CityListPojo data = (CityListPojo) location.getAdapter().getItem(position);
                location.setText(data.getCityName());

                Intent bundle = new Intent();

                bundle.putExtra("city_name", location.getText().toString());
                setResult(1, bundle);
                finish();
            }
        });
        location.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
                String text = s.toString().trim();
                city_found = false;
//                if(count == 0)
                if (text.length() >= 3) {
//                        count = 1;
                    getCityList(text);
                }

            }
        });
    }

    @Override
    public void onClick(View view) {
        if (selectAll == view) {
            selectAll.setBackgroundColor(Color.parseColor("#8b8e8e"));
            recents.setBackgroundResource(R.drawable.stroke_rect);
        } else if (view == recents) {
            recents.setBackgroundColor(Color.parseColor("#8b8e8e"));
            selectAll.setBackgroundResource(R.drawable.stroke_rect);
        }
    }

    private void getCityList(String cityName) {
        if (cd.isConnectingToInternet()) {
//            d = AppDialogs.showLoading(ctx);
//            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<CityListData> call = apiService.getCityList(cityName);
            call.enqueue(new Callback<CityListData>() {
                @Override
                public void onResponse(Call<CityListData> call, Response<CityListData> response) {
                    if (response != null && response.body() != null) {
                        if (response.body().getStatus() != null && !response.body().getStatus().isEmpty()) {
                            if (response.body().getStatus().equalsIgnoreCase("true")) {
                                if (response.body().getCity() != null && response.body().getCity().size() > 0) {
                                    error.setVisibility(View.GONE);
                                    cityList.clear();
                                    city_found = true;
                                    cityList = response.body().getCity();
                                    CustomAutoCompleteAdapter adapter = new CustomAutoCompleteAdapter(cityList, ctx);
                                    location.setAdapter(adapter);
                                    adapter.notifyDataSetChanged();
                                    try {
                                        location.showDropDown();
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } else {
                                error.setVisibility(VISIBLE);
                            }
                        }
                    } else {
                        dialog.displayCommonDialog(getResources().getString(R.string.search_city_failed), AppConstants.SERVER_NOT_RESPONDING);

                    }
//                    d.dismiss();
                }

                @Override
                public void onFailure(Call<CityListData> call, Throwable t) {
                    // Log error here since request failed
//                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        count = 0;
        hideSoftKeyboard();
        location.setText("");
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (location != null) {
            location.dismissDropDown();
        }
    }
}
