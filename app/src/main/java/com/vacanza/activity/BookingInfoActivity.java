package com.vacanza.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.vacanza.Pojo.BookingInfoPojo;
import com.vacanza.Pojo.ManageBookingPojoDetail;
import com.vacanza.adapter.GuestListAdapter;
import com.vacanza.model.ApiClient;
import com.vacanza.model.ApiInterface;
import com.vacanza.utils.AppConstants;
import com.vacanza.utils.AppDialogs;
import com.vacanza.utils.ConnectionDetector;

import example.com.hotelbooking.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class BookingInfoActivity extends BaseActivity {

    private BookingInfoActivity ctx = this;
    private ImageView back;
    private RatingBar ratings;
    private TextView txt_cancel, txt_continue, hotel_name, hotel_address, booking_id, room_type, board_type, checkin, checkout, price, total_rooms;
    private LinearLayout btn_cancel, btn_continue, cancellation_policy;
    private RecyclerView guestList;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    //    private CardView home;
    private ConnectionDetector cd;
    private AppDialogs dialog;
    private String cancellationPolicy = "";
    private String bookingId = "";
    private String supplier = "";
    private String from;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking_info);

        if (getIntent().getStringExtra("FROM") != null)
            from = getIntent().getStringExtra("FROM");

        init();
        AppConstants.ACTIVITIES.add(ctx);
        getBookingInfoDetails();

        cancellation_policy.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (cancellationPolicy != null && !cancellationPolicy.isEmpty()) {
                    displayDialog(cancellationPolicy);
                }
            }
        });

    }

    private void init() {
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new AppDialogs(ctx);
        back = (ImageView) findViewById(R.id.back);
        ratings = (RatingBar) findViewById(R.id.ratings);
//        home = (CardView) findViewById(R.id.home);
        guestList = (RecyclerView) findViewById(R.id.guestList);
        cancellation_policy = (LinearLayout) findViewById(R.id.cancellation_policy);
        hotel_name = (TextView) findViewById(R.id.hotel_name);
        hotel_address = (TextView) findViewById(R.id.hotel_address);
        booking_id = (TextView) findViewById(R.id.booking_id);
        room_type = (TextView) findViewById(R.id.room_type);
        board_type = (TextView) findViewById(R.id.board_type);
        checkin = (TextView) findViewById(R.id.checkin);
        checkout = (TextView) findViewById(R.id.checkout);
        price = (TextView) findViewById(R.id.price);
        btn_continue = (LinearLayout) findViewById(R.id.continue_btn);
        btn_cancel = (LinearLayout) findViewById(R.id.cancel_btn);
        txt_continue = (TextView) findViewById(R.id.txt_continue);
        txt_cancel = (TextView) findViewById(R.id.txt_cancel);
        total_rooms = (TextView) findViewById(R.id.total_rooms);
        guestList.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        guestList.setLayoutManager(mLayoutManager);
        guestList.setNestedScrollingEnabled(false);


        if (from != null && from.equalsIgnoreCase("ManageBooking")) {
            txt_continue.setText("FINISH");
        }
        btn_continue.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (from != null && from.equalsIgnoreCase("ManageBooking")) {
                    onBackPressed();
                } else {
                    navigateToHome();
                }

            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                cancelBooking(bookingId, supplier);

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                navigateToHome();
            }
        });

    }

    private void getBookingInfoDetails() {
        if (cd.isConnectingToInternet()) {

            final ProgressDialog d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<BookingInfoPojo> call = apiService.getBookingInfo(getFromPrefs(AppConstants.RANDOM_NO)/*"497732"*/);
            call.enqueue(new Callback<BookingInfoPojo>() {
                @Override
                public void onResponse(Call<BookingInfoPojo> call, Response<BookingInfoPojo> response) {
                    d.dismiss();
                    if (response.body() != null) {
                        if (response.body().getIsshowcancellationbuton().equalsIgnoreCase("true")) {
                            if (from != null && from.equalsIgnoreCase("ManageBooking")) {
                                btn_cancel.setVisibility(View.VISIBLE);
                            }
                        }
                        if (response.body().getHotelname() != null && !response.body().getHotelname().isEmpty()) {
                            hotel_name.setText(response.body().getHotelname());
                        }
                        if (response.body().getHoteladdress() != null && !response.body().getHoteladdress().isEmpty()) {
                            hotel_address.setText(response.body().getHoteladdress());
                        } else {
                            hotel_address.setText(getFromPrefs(AppConstants.CITY_NAME));
                        }
                        if (response.body().getBookingno() != null && !response.body().getBookingno().isEmpty()) {
                            booking_id.setText(response.body().getBookingno());
                            bookingId = response.body().getBookingno();
                        }
                        if (response.body().getRoomcategory() != null && !response.body().getRoomcategory().isEmpty()) {
                            room_type.setText(response.body().getRoomcategory());
                        }
                        if (response.body().getRoomcategory() != null && !response.body().getSupplier().isEmpty()) {
                            supplier = response.body().getSupplier();
                        }
                        if (response.body().getRoomboardtype() != null && !response.body().getRoomboardtype().isEmpty()) {
                            board_type.setText(response.body().getRoomboardtype());
                        }
                        if (response.body().getCheckin() != null && !response.body().getCheckin().isEmpty()) {
                            checkin.setText(response.body().getCheckin());
                        }
                        if (response.body().getCheckout() != null && !response.body().getCheckout().isEmpty()) {
                            checkout.setText(response.body().getCheckout());
                        }
                        if (response.body().getTotalrooms() != null && !response.body().getTotalrooms().isEmpty()) {
                            total_rooms.setText(response.body().getTotalrooms());
                        }
                        if (response.body().getCancellationpolicy() != null && !response.body().getCancellationpolicy().isEmpty()) {
                            cancellationPolicy = response.body().getCancellationpolicy();
                        }
                        if (response.body().getCurrency() != null && !response.body().getCurrency().isEmpty() &&
                                response.body().getTotalprice() != null && !response.body().getTotalprice().isEmpty()) {
                            price.setText(response.body().getCurrency() + " " + response.body().getTotalprice());
                        }
                        if (response.body().getPaxinfo() != null && response.body().getPaxinfo().size() > 0) {
                            mAdapter = new GuestListAdapter(ctx, response.body().getPaxinfo());
                            guestList.setAdapter(mAdapter);
                        }

                    } else {
                        dialog.displayCommonDialog(getResources().getString(R.string.search_info_failed), AppConstants.SERVER_NOT_RESPONDING);
                    }

                }

                @Override
                public void onFailure(Call<BookingInfoPojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void cancelBooking(String bookingNo, String supplier) {
        if (cd.isConnectingToInternet()) {

            final ProgressDialog d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ManageBookingPojoDetail> call = apiService.CancelBooking(bookingNo, supplier);
            call.enqueue(new Callback<ManageBookingPojoDetail>() {
                @Override
                public void onResponse(Call<ManageBookingPojoDetail> call, Response<ManageBookingPojoDetail> response) {
                    d.dismiss();
                    if (response.body() != null) {
                        if (response.body().getCommonList() != null && response.body().getStatus().equalsIgnoreCase("true")) {
//                            String strCode = response.body().getCommonList().get(0).getStrcode();
//                            String strName = response.body().getCommonList().get(0).getStrname();
//                            saveIntoPrefs(AppConstants.RANDOM_NO, strCode);
                            Intent intent = new Intent(ctx, BookingInfoActivity.class);
                            intent.putExtra("FROM","ManageBooking");
                            startActivity(intent);
                            finish();
                        } else {
                            dialog.displayCommonDialog(getResources().getString(R.string.search_info_failed), AppConstants.SERVER_NOT_RESPONDING);
                        }

                    }
                }

                @Override
                public void onFailure(Call<ManageBookingPojoDetail> call, Throwable t) {
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    public void onBackPressed() {
//        super.onBackPressed();
        navigateToHome();
    }

    public void navigateToHome() {
        Intent i = new Intent(this, HomeActivity.class);
        startActivity(i);
        finish();
        clearRoomData(ctx);
        saveIntoPrefs(AppConstants.RANDOM_NO, "");
        saveIntoPrefs(AppConstants.HOTEL_IMAGE, "");
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }
}
