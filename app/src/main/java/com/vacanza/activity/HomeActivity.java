package com.vacanza.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.squareup.otto.Subscribe;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.Random;

import com.vacanza.Pojo.CommonListPojo;
import com.vacanza.Pojo.CountryPojo;
import com.vacanza.Pojo.RoomModel;

import example.com.hotelbooking.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import com.vacanza.adapter.NationalityAdapter;
import com.vacanza.controller.FmsEventBus;
import com.vacanza.fragment.RoomDialogFragment;
import com.vacanza.model.ApiClient;
import com.vacanza.model.ApiInterface;
import com.vacanza.utils.AppConstants;
import com.vacanza.utils.AppDialogs;
import com.vacanza.utils.ConnectionDetector;

public class HomeActivity extends BaseActivity implements View.OnClickListener, DatePickerDialog.OnDateSetListener {
    private ConnectionDetector cd;
    private AppDialogs dialog;
    private HomeActivity ctx = this;
    public static final String ROOM_DATA = "room_data";
    private static int IN_CAL = 0;
    private static int OUT_CAL = 1;
    private ArrayList<RoomModel> roomModels;
    private LinearLayout cehckinBtn, checkoutBtn;
    private TextView inDate, outDate, inMonth, outMonth, inDay, outDay;
    private CardView noOfRooms, searchBtn, topLay;
    private TextView search_text, no_of_guests, no_of_rooms;
    private int requestCode_room = 123;
    private int requestCode_city = 456;

    public JsonArray roomArray = new JsonArray();
    public JsonObject guestObj = new JsonObject();
    JsonArray jsonArrayAdult = new JsonArray();
    JsonArray jsonArrayChild = new JsonArray();
    JsonArray array;
//    JsonObject numsAdult = new JsonObject();

    private DatePickerDialog datePickerDialog;
    String cityName = "";
    String check_in_date = "";
    String check_out_date = "";
    int which = 2, guests = 0, rooms = 0;
    public String[] MONTHS = {"Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"};
    public String[] DAYS = {"Mon", "TUes", "Wed", "Thurs", "Fri", "Sat", "Sun"};

    long checkin_milli = System.currentTimeMillis() - 1000;
    private Calendar checkin_myCalendar, checkout_myCalendar;
    private int checkin_month, checkin_year, checkin_day;
    int room = 0;
    private String total_person;
    private TextView changeNationality, changeCurrency, currency_text, nationality_text;
    private Dialog nationality_dialog, currency_dialog;
    private RecyclerView mRecyclerView;
    private NationalityAdapter adapter;
    private ArrayList<CommonListPojo> nationalityList, currencyList;
    private boolean doubleBackToExitPressedOnce = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        for (int i = 0; i < AppConstants.ACTIVITIES.size(); i++) {
            if (AppConstants.ACTIVITIES.get(i) != null)
                AppConstants.ACTIVITIES.get(i).finish();
        }
        AppConstants.ACTIVITIES.add(ctx);
        setDrawerAndToolbar("Myvacanza Hotel Booking");
        init();

        if (cd.isConnectingToInternet()) {
            // TODO here we stop get currency api for further reference as per client requirements, and in xml file change icon visibility is gone
//            getcurrencyAPI();
            getNationalityAPI();
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
        changeNationality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (nationalityList != null && nationalityList.size() > 0) {
                    showNationalityDialog(ctx, "Nationality");
                } else {
                    Toast.makeText(ctx, "No Network Available. Please Check Internet Connectivity and Go back and again open the Application", Toast.LENGTH_SHORT).show();
                }
            }
        });

        changeCurrency.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (currencyList != null && currencyList.size() > 0) {
                    showCurrencyDialog(ctx, "Currency");
                } else {
                    Toast.makeText(ctx, "No Network Available. Please Check Internet Connectivity and Go back and again open the Application", Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getFromPrefs(AppConstants.NATIONALITY).isEmpty()) {
            nationality_text.setText("Malaysia");
        } else {
            nationality_text.setText(getFromPrefs(AppConstants.NATIONALITY));
        }
//        if (getFromPrefs(AppConstants.CURRENCY).isEmpty()) {
            currency_text.setText("MYR");
//        } else {
//            currency_text.setText(getFromPrefs(AppConstants.CURRENCY));
//        }
    }

    private void init() {
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new AppDialogs(ctx);
        topLay = (CardView) findViewById(R.id.topLay);

        cehckinBtn = (LinearLayout) findViewById(R.id.checkin_btn);
        checkoutBtn = (LinearLayout) findViewById(R.id.checkout_btn);

        search_text = (TextView) findViewById(R.id.search_text);

        no_of_guests = (TextView) findViewById(R.id.no_of_guests);
        no_of_rooms = (TextView) findViewById(R.id.no_of_rooms);

        changeCurrency = (TextView) findViewById(R.id.changeCurrency);
        changeNationality = (TextView) findViewById(R.id.changeNationality);
        nationality_text = (TextView) findViewById(R.id.nationality_text);
        currency_text = (TextView) findViewById(R.id.currency_text);


        inDate = (TextView) findViewById(R.id.in_date);
        outDate = (TextView) findViewById(R.id.out_date);
        inMonth = (TextView) findViewById(R.id.in_month);
        outMonth = (TextView) findViewById(R.id.out_month);
        inDay = (TextView) findViewById(R.id.in_day);
        outDay = (TextView) findViewById(R.id.out_day);

        noOfRooms = (CardView) findViewById(R.id.thirdLay);
        searchBtn = (CardView) findViewById(R.id.search_btn);

        Calendar c = Calendar.getInstance();

        checkoutBtn.setOnClickListener(this);
        cehckinBtn.setOnClickListener(this);

        noOfRooms.setOnClickListener(this);

        searchBtn.setOnClickListener(this);
        topLay.setOnClickListener(this);
        getDate();
        datePickerDialog = new DatePickerDialog(
                ctx, this, c.get(Calendar.YEAR), c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
        checkout_myCalendar = Calendar.getInstance();
        checkin_myCalendar = Calendar.getInstance();
        checkin_day = checkin_myCalendar.DAY_OF_MONTH;
        checkin_month = checkin_myCalendar.MONTH;
        checkin_year = checkin_myCalendar.YEAR;
        nationalityList = new ArrayList<>();
        currencyList = new ArrayList<>();
    }

    @Override
    public void onClick(View v) {
        if (v == cehckinBtn) {
            which = IN_CAL;
            showCheckInDatePicker();
        } else if (v == checkoutBtn) {
            which = OUT_CAL;
          /*  datePickerDialog.getDatePicker().setMinDate(checkin_milli+1*24*60*60*1000);
            datePickerDialog.show();*/

            showCheckOutDatePicker();
        } else if (v == noOfRooms) {
            /*Intent i = new Intent(ctx, RoomOptionsActivity.class);
            startActivityForResult(i, requestCode_room);*/
            showSelectRoomDialog();

        } else if (v == searchBtn) {
            total_person = no_of_guests.getText().toString();
            Date date1 = changeDateFormateFromString(check_in_date);
            Date date2 = changeDateFormateFromString(check_out_date);
            if (date1.before(date2)) {
                callAPI();
            } else {
                dialog.displayCommonDialog(getResources().getString(R.string.search_failed), "Check In Date should be before Check Out Date");
            }

        } else if (v == topLay) {
            Intent intent = new Intent(ctx, LocationsActivity.class);
            startActivityForResult(intent, requestCode_city);
            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        } else if (v.getId() == R.id.rootLL) {

            String str_code = (String) v.getTag(R.string.key);
            String str_from = (String) v.getTag(R.string.data);
            if (str_from.equals("Currency")) {
                currency_dialog.dismiss();
                saveIntoPrefs(AppConstants.CURRENCY, "");
                saveIntoPrefs(AppConstants.CURRENCY, str_code);
                currency_text.setText(str_code);
            } else if (str_from.equals("Nationality")) {
                nationality_dialog.dismiss();
                saveIntoPrefs(AppConstants.NATIONALITY, "");
                saveIntoPrefs(AppConstants.NATIONALITY, str_code);
                nationality_text.setText(str_code);
            }


        }
    }

    @Override
    public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
        Calendar calendar = Calendar.getInstance();
        calendar.set(i, i1, i2);

        if (which == IN_CAL) {
            inDay.setText(DAYS[calendar.get(Calendar.DAY_OF_WEEK) - 1]);
            inDate.setText(i2 + "");

            inMonth.setText(MONTHS[i1]);
            checkin_milli = calendar.getTimeInMillis() - 10000;
        } else {
            outDay.setText(DAYS[calendar.get(Calendar.DAY_OF_WEEK)]);
            outDate.setText(i2 + "");

            outMonth.setText(MONTHS[i1]);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == requestCode_room) {
            if (resultCode == 1) {
                rooms = data.getIntExtra("room", 2);
                guests = data.getIntExtra("guests", 3);
            }
        }
        if (requestCode == requestCode_city) {
            if (resultCode == 1) {
                cityName = data.getStringExtra("city_name");
                search_text.setText(cityName);
                saveIntoPrefs(AppConstants.CITY_NAME, cityName);
            }
        }
    }

    private void getDate() {
        Calendar calendar = Calendar.getInstance();
        Date today = calendar.getTime();

        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();

        DateFormat dateFormat = new SimpleDateFormat("EEE-dd-MMM-yyyy");

        String todayAsString = dateFormat.format(today);
        String tomorrowAsString = dateFormat.format(tomorrow);


        String[] arrCheckIn = todayAsString.split("-");
        inDay.setText(arrCheckIn[0]);
        inDate.setText(arrCheckIn[1]);
        inMonth.setText(arrCheckIn[2]);

        check_in_date = chaneDateFormateIntoString(today);

        String[] arrCheckout = tomorrowAsString.split("-");
        outDay.setText(arrCheckout[0]);
        outDate.setText(arrCheckout[1]);
        outMonth.setText(arrCheckout[2]);

        check_out_date = chaneDateFormateIntoString(tomorrow);

        initialRoomSize();
    }


    private Date changeDateFormateFromString(String str) {
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(str);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return date;
    }

    private void showCheckInDatePicker() {
        DatePickerDialog dialog = new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day) {
                checkin_myCalendar.set(year, month, day);
                checkin_year = year;
                checkin_month = month;
                checkin_day = day;
                updateLabel();

                checkin_myCalendar.add(Calendar.DAY_OF_YEAR, 1);
                Date tomorrow = checkin_myCalendar.getTime();
                DateFormat dateFormat = new SimpleDateFormat("EEE-dd-MMM-yyyy");
                String tomorrowAsString = dateFormat.format(tomorrow);

                String[] arrCheckout = tomorrowAsString.split("-");
                outDay.setText(arrCheckout[0]);
                outDate.setText(arrCheckout[1]);
                outMonth.setText(arrCheckout[2]);

                check_out_date = chaneDateFormateIntoString(tomorrow);
            }
        }, checkin_year, checkin_month, checkin_day);
        dialog.getDatePicker().setMinDate((System.currentTimeMillis()) - 1000);
        dialog.show();
    }

    private void updateLabel() {
        DateFormat dateFormat = new SimpleDateFormat("EEE-dd-MMM-yyyy");
        DateFormat dateFormatforAPI = new SimpleDateFormat("dd-MM-yyyy");

        String checkInDateAsString = dateFormat.format(checkin_myCalendar.getTime());
        String checkInDateAsStringforAPI = dateFormatforAPI.format(checkin_myCalendar.getTime());
        String[] arrCheckIn = checkInDateAsString.split("-");
        String[] arrCheckInforAPI = checkInDateAsStringforAPI.split("-");
        inDay.setText(arrCheckIn[0]);
        inDate.setText(arrCheckIn[1]);
        inMonth.setText(arrCheckIn[2]);
        check_in_date = arrCheckInforAPI[2] + "-" + arrCheckInforAPI[1] + "-" + arrCheckInforAPI[0];
    }

    private void showCheckOutDatePicker() {
        DatePickerDialog dialog = new DatePickerDialog(ctx, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker arg0, int year, int month, int day) {
                checkout_myCalendar.set(year, month, day);
                checkin_year = year;
                checkin_month = month;
                checkin_day = day;
                updateCheckOutLabel();
            }
        }, checkin_year, checkin_month, checkin_day);
        dialog.getDatePicker().setMinDate((checkin_myCalendar.getTime().getTime() /*+ 1 * 24 * 60 * 60 * 1000*/));
        dialog.show();
    }

    private void updateCheckOutLabel() {
        DateFormat dateFormat = new SimpleDateFormat("EEE-dd-MMM-yyyy");
        DateFormat dateFormatforAPI = new SimpleDateFormat("dd-MM-yyyy");

        String checkOutDateAsString = dateFormat.format(checkout_myCalendar.getTime());
        String checkOutDateAsStringforAPI = dateFormatforAPI.format(checkout_myCalendar.getTime());
        String[] arrCheckIn = checkOutDateAsString.split("-");
        String[] arrCheckOutforAPI = checkOutDateAsStringforAPI.split("-");
        outDay.setText(arrCheckIn[0]);
        outDate.setText(arrCheckIn[1]);
        outMonth.setText(arrCheckIn[2]);

        check_out_date = arrCheckOutforAPI[2] + "-" + arrCheckOutforAPI[1] + "-" + arrCheckOutforAPI[0];
    }

    public void showSelectRoomDialog() {
        RoomDialogFragment roomDialogFragment = new RoomDialogFragment();
        roomDialogFragment.setCancelable(false);
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(ROOM_DATA, roomModels);
        roomDialogFragment.setArguments(bundle);
        roomDialogFragment.show(getSupportFragmentManager(), "rdf");

    }

    @Subscribe
    public void getRoomModel(ArrayList<RoomModel> roomModels) {


        array = new JsonArray();

        this.roomModels = roomModels;
        int total_adult = 0, total_children = 0, totalGuest = 0;

        for (RoomModel roomModel : roomModels) {
            total_adult += roomModel.getNumAdults();
            total_children += roomModel.getNumChildren();
        }

        total_person = String.valueOf(total_adult + total_children);
        no_of_guests.setText(total_person);
        no_of_rooms.setText(String.valueOf(roomModels.size()));
        saveIntoRoomPrefs(AppConstants.TOTAL_PERSON, total_person);

        /** no of rooms*/
        int total = roomModels.size();
        if (total > 0) {
            room = total - 1;
        }

        for (int x = 0; x < roomModels.size(); x++) {

            JsonArray jsonArrayAdult = new JsonArray();
            JsonArray jsonArrayChild = new JsonArray();

            for (int i = 0; i < roomModels.get(x).getNumAdults(); i++) {
                JsonObject numsAdult = new JsonObject();
                try {
                    numsAdult.addProperty("Title", "Mr.");
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                jsonArrayAdult.add(numsAdult);
            }


            for (int i = 0; i < roomModels.get(x).getNumChildren(); i++) {
                JsonObject numsChild = new JsonObject();
                try {
                    numsChild.addProperty("Title", "Master");
                    numsChild.addProperty("age", x);

                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }

                jsonArrayChild.add(numsChild);
            }

            JsonObject object = new JsonObject();
            JsonObject numsAdult = new JsonObject();

            numsAdult.add("Adult", jsonArrayAdult);
            numsAdult.add("Child", jsonArrayChild);
            object.add("Guests", numsAdult);

            array.add(object);
        }

    }

    @Override
    protected void onStart() {
        super.onStart();
        FmsEventBus.getInstance().register(ctx);
    }

    @Override
    protected void onStop() {
        super.onStop();
        FmsEventBus.getInstance().unregister(ctx);
    }

    public void callAPI() {
        if (cityName.equalsIgnoreCase("")) {
            dialog.displayCommonDialog(getResources().getString(R.string.search_failed), "City Name Required");
        } else if (check_in_date.equalsIgnoreCase("")) {
            dialog.displayCommonDialog(getResources().getString(R.string.search_failed), "Check-In date Required");
        } else if (check_out_date.equalsIgnoreCase("")) {
            dialog.displayCommonDialog(getResources().getString(R.string.search_failed), "Check-Out date Required");
        } else {
            try {
                JSONObject mainObject = new JSONObject();
                JSONObject AvailabilitySearch = new JSONObject();
                JSONObject Authority = new JSONObject();
                JSONArray obj2 = new JSONArray(array.toString());
//                Authority.put("Currency", currency_text.getText().toString().trim());
                Authority.put("Currency", "MYR");
                AvailabilitySearch.put("Authority", Authority);
                AvailabilitySearch.put("CityName", cityName);
                AvailabilitySearch.put("CheckInDate", check_in_date);
                AvailabilitySearch.put("CheckOutDate", check_out_date);
                saveIntoPrefs(AppConstants.RANDOM_NO, "");
                int random_No = randomNo();
                saveIntoPrefs(AppConstants.RANDOM_NO, "" + random_No);
                AvailabilitySearch.put("Searchid", random_No);
                AvailabilitySearch.put("Nationality", nationality_text.getText().toString().trim());
                AvailabilitySearch.put("Room", obj2);
                JSONObject obj = new JSONObject(AvailabilitySearch.toString());
                mainObject.put("AvailabilitySearch", obj);

                String checkInDate = "";
                String checkoutDate = "";
                if (inDate.getText().toString() != null && !inDate.getText().toString().isEmpty() &&
                        inMonth.getText().toString() != null && !inMonth.getText().toString().isEmpty()) {
                    checkInDate = inDate.getText().toString() + " " + inMonth.getText().toString();
                }

                if (outDate.getText().toString() != null && !outDate.getText().toString().isEmpty() &&
                        outMonth.getText().toString() != null && !outMonth.getText().toString().isEmpty()) {
                    checkoutDate = outDate.getText().toString() + " " + outMonth.getText().toString();
                }

                Intent intent = new Intent(ctx, SearchResultsActivity.class);
                intent.putExtra("check_in_date", checkInDate);
                intent.putExtra("check_out_date", checkoutDate);
                intent.putExtra("total_person", total_person);
                intent.putExtra("mainObject", mainObject.toString());
                startActivity(intent);
                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

    public int randomNo() {
        Random Number = new Random();
        return Number.nextInt(1000000);
    }

    public void initialRoomSize() {
        no_of_guests.setText("1");
        no_of_rooms.setText("1");
        array = new JsonArray();
        for (int x = 0; x < 1; x++) {

            JsonArray jsonArrayAdult = new JsonArray();
            JsonArray jsonArrayChild = new JsonArray();

            for (int i = 0; i < 1; i++) {
                JsonObject numsAdult = new JsonObject();
                try {
                    numsAdult.addProperty("Title", "Mr.");
                } catch (Exception e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                jsonArrayAdult.add(numsAdult);
            }

            JsonObject object = new JsonObject();
            JsonObject numsAdult = new JsonObject();

            numsAdult.add("Adult", jsonArrayAdult);
            numsAdult.add("Child", jsonArrayChild);
            object.add("Guests", numsAdult);

            array.add(object);
        }

    }

    private void getNationalityAPI() {
        if (cd.isConnectingToInternet()) {

            final ProgressDialog d = AppDialogs.showServerDataLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<CountryPojo> call = apiService.getNationality();
            call.enqueue(new Callback<CountryPojo>() {
                @Override
                public void onResponse(Call<CountryPojo> call, Response<CountryPojo> response) {
                    d.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            if (response.body().getCommonList() != null) {
                                nationalityList = response.body().getCommonList();
                            }

                        } else {
                            dialog.displayCommonDialog(getResources().getString(R.string.location_error), response.body().getMessage());
                        }
                    } else {
                        dialog.displayCommonDialog(getResources().getString(R.string.location_error), AppConstants.SERVER_NOT_RESPONDING);
                    }

                }

                @Override
                public void onFailure(Call<CountryPojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        }
    }

    private void getcurrencyAPI() {
        if (cd.isConnectingToInternet()) {

            final ProgressDialog d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<CountryPojo> call = apiService.getcurrency();
            call.enqueue(new Callback<CountryPojo>() {
                @Override
                public void onResponse(Call<CountryPojo> call, Response<CountryPojo> response) {
                    d.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            if (response.body().getCommonList() != null) {
                                currencyList = response.body().getCommonList();
                            }
                        } else {
                            dialog.displayCommonDialog(getResources().getString(R.string.currency_error), response.body().getMessage());
                        }
                    } else {
                        dialog.displayCommonDialog(getResources().getString(R.string.currency_error), AppConstants.SERVER_NOT_RESPONDING);
                    }

                }

                @Override
                public void onFailure(Call<CountryPojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        }
    }

    public void showNationalityDialog(Context ctx, String title) {
        nationality_dialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        nationality_dialog.setContentView(R.layout.select_location_dialog);

        mRecyclerView = (RecyclerView) nationality_dialog.findViewById(R.id.change_location_recycler);
        ImageView close_dialog = (ImageView) nationality_dialog.findViewById(R.id.close_dialog);
        TextView dialog_header = (TextView) nationality_dialog.findViewById(R.id.dialog_header);
        dialog_header.setText(title);
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                nationality_dialog.dismiss();
            }
        });

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (nationalityList != null && nationalityList.size() > 0) {
            adapter = new NationalityAdapter(ctx, nationalityList, "Nationality", "HomeActivity");
            mRecyclerView.setAdapter(adapter);
        }

        nationality_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        nationality_dialog.show();
    }

    public void showCurrencyDialog(Context ctx, String title) {
        currency_dialog = new Dialog(ctx, android.R.style.Theme_Translucent_NoTitleBar);
        currency_dialog.setContentView(R.layout.select_location_dialog);

        mRecyclerView = (RecyclerView) currency_dialog.findViewById(R.id.change_location_recycler);
        ImageView close_dialog = (ImageView) currency_dialog.findViewById(R.id.close_dialog);
        TextView dialog_header = (TextView) currency_dialog.findViewById(R.id.dialog_header);
        dialog_header.setText(title);
        close_dialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                currency_dialog.dismiss();
            }
        });

        mRecyclerView.setHasFixedSize(true);
        LinearLayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        mRecyclerView.setLayoutManager(mLayoutManager);

        if (currencyList != null && currencyList.size() > 0) {
            adapter = new NationalityAdapter(ctx, currencyList, "Currency", "HomeActivity");
            mRecyclerView.setAdapter(adapter);
        }

        currency_dialog.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        currency_dialog.show();
    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;
        Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();

        new Handler().postDelayed(new Runnable() {

            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

}
