package com.vacanza.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.vacanza.Pojo.AdultPojo;
import com.vacanza.Pojo.BasePojo;
import com.vacanza.Pojo.CancellationPolicyPojo;
import com.vacanza.Pojo.SearchInfoDataPojo;
import com.vacanza.Pojo.SendBookingInfoPojo;
import com.vacanza.adapter.RoomListAdapter;
import com.vacanza.model.ApiClient;
import com.vacanza.model.ApiInterface;
import com.vacanza.utils.AppConstants;
import com.vacanza.utils.AppDialogs;
import com.vacanza.utils.ConnectionDetector;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import example.com.hotelbooking.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BucketActivity extends BaseActivity implements View.OnClickListener {

    private BucketActivity ctx = this;
    private ConnectionDetector cd;
    private AppDialogs dialog;
    private ImageView back, hotelImage;
    private TextView hotel_name, hotel_address, in_day, in_month, in_date, out_day, out_month, out_date,
            duration, room_type, board_type, total_price;
    private RatingBar ratings;
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager mLayoutManager;
    private RecyclerView.Adapter mAdapter;
    private String strHotelDestination = "", strHotelAddress = "", strRating = "", strHotelName = "", boardType = "", roomCategory = "",
            roomRate = "", roomProcessId = "", supplier = "", hotelCode = "";
    private LinearLayout payNow;
    private SearchInfoDataPojo basePojo;
    private SearchInfoDataPojo searchInfoDataPojo;
    private String checkInDateAsString = "", checkOutDateAsString = "", cityName = "", searchId = "", nationality = "", currency = "";
    JsonArray array;
    private String check_in_date, check_out_date, email, adultTitle, adultFirstName, childFirstName, childAge;
    private ProgressDialog d;
    private LinearLayout cancellationPolicy;
    private String cancellationMessage;
    private boolean mAdultFlag = false, mChildFlag = false, isChildAvail = false;
    private final static int PAYMENT_STATUS = 100;
    private String failure_endpoint = "";
    private String failure_url = "";
    private String success_endpoint = "";
    private String success_url = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_details);

        init();
        getSearchInfoAPI();
        getHotelCancellationAPI();
        back.setOnClickListener(this);
        cancellationPolicy.setOnClickListener(this);


        DisplayMetrics metrics = new DisplayMetrics();
        ctx.getWindowManager().getDefaultDisplay().getMetrics(metrics);
        int width = metrics.widthPixels;
        setImageInLayoutHome(ctx, (int) width, (int) ctx.getResources().getDimension(R.dimen.hotel_room_gallery_height),
                getFromPrefs(AppConstants.HOTEL_IMAGE), hotelImage);

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                array = new JsonArray();
                for (int i = 0; i < (Integer.parseInt(basePojo.getNoOfRooms())); i++) {
                    JsonArray jsonArrayAdult = new JsonArray();
                    JsonArray jsonArrayChild = new JsonArray();
                    for (int j = 0; j < (Integer.parseInt(basePojo.getSearchRoom().get(i).getAdult())); j++) {
                        JsonObject numsAdult = new JsonObject();
                        try {
                            adultTitle = searchInfoDataPojo.getSearchRoom().get(i).getAdultPojo().get(j).getTitle();
                            adultFirstName = searchInfoDataPojo.getSearchRoom().get(i).getAdultPojo().get(j).getFirstName();
                            if (adultTitle == null || adultTitle.equals("null") || adultTitle.length() == 0) {
                                mAdultFlag = false;
                                dialog.displayCommonDialog(getResources().getString(R.string.payment_failed), "Adult Title should not be empty");
                            } else if (adultFirstName == null || adultFirstName.equals("null") || adultFirstName.length() == 0) {
                                mAdultFlag = false;
                                dialog.displayCommonDialog(getResources().getString(R.string.payment_failed), "Adult First Name should not be empty");
                            } else if (searchInfoDataPojo.getSearchRoom().get(0).getAdultPojo().get(0).getEmail() == null ||
                                    searchInfoDataPojo.getSearchRoom().get(0).getAdultPojo().get(0).getEmail().equals("null") ||
                                    searchInfoDataPojo.getSearchRoom().get(0).getAdultPojo().get(0).getEmail().length() == 0) {
                                mAdultFlag = false;
                                dialog.displayCommonDialog(getResources().getString(R.string.payment_failed), "Adult Email should not be empty");
                            } else if (!isValidEmail(searchInfoDataPojo.getSearchRoom().get(0).getAdultPojo().get(0).getEmail())) {
                                mAdultFlag = false;
                                dialog.displayCommonDialog(getResources().getString(R.string.valid_email), getResources().getString(R.string.valid_email));
                            } else {
                                mAdultFlag = true;
                                numsAdult.addProperty("Title", adultTitle);
                                if (searchInfoDataPojo.getSearchRoom().get(i).getAdultPojo().get(j).getEmail() == null || searchInfoDataPojo.getSearchRoom().get(i).getAdultPojo().get(j).getEmail().equals("null")) {
                                    email = "";
                                } else {
                                    email = searchInfoDataPojo.getSearchRoom().get(i).getAdultPojo().get(j).getEmail();
                                }
                                numsAdult.addProperty("FirstName", adultFirstName);
                                numsAdult.addProperty("LastName", searchInfoDataPojo.getSearchRoom().get(i).getAdultPojo().get(j).getLastName());

                                numsAdult.addProperty("Email", email);
                                numsAdult.addProperty("ContactNo", "");
                                numsAdult.addProperty("Age", "30");
                            }

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }
                        jsonArrayAdult.add(numsAdult);
                    }

                    for (int k = 0; k < (Integer.parseInt(basePojo.getSearchRoom().get(i).getChild())); k++) {
                        isChildAvail = true;
                        JsonObject numsChild = new JsonObject();
                        try {
                            childFirstName = searchInfoDataPojo.getSearchRoom().get(i).getChildPojo().get(k).getFirstName();
                            childAge = searchInfoDataPojo.getSearchRoom().get(i).getChildPojo().get(k).getAge();
                            if (childFirstName == null || childFirstName.equals("null") || childFirstName.length() == 0) {
                                mChildFlag = false;
                                dialog.displayCommonDialog(getResources().getString(R.string.payment_failed), "Child First name should not empty");
                            } else if (childAge == null || childAge.equals("null") || childAge.length() == 0) {
                                mChildFlag = false;
                                dialog.displayCommonDialog(getResources().getString(R.string.payment_failed), "Child age name should not empty");
                            } else if (Integer.parseInt(childAge) > 12) {
                                mChildFlag = false;
                                dialog.displayCommonDialog(getResources().getString(R.string.payment_failed), "Child age should not be grater than 12");
                            } else {
                                mChildFlag = true;
                                numsChild.addProperty("Title", "Master");
                                numsChild.addProperty("FirstName", childFirstName);
                                numsChild.addProperty("LastName", searchInfoDataPojo.getSearchRoom().get(i).getChildPojo().get(k).getLastName());
                                numsChild.addProperty("age", childAge);
                            }

                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                            e.printStackTrace();
                        }

                        jsonArrayChild.add(numsChild);
                    }
                    JsonObject object = new JsonObject();
                    JsonObject numsAdult = new JsonObject();

                    numsAdult.add("Adult", jsonArrayAdult);
                    numsAdult.add("Child", jsonArrayChild);
                    object.add("Guests", numsAdult);

                    array.add(object);

                }

                try {
                    JSONObject mainObject = new JSONObject();
                    JSONObject AvailabilitySearch = new JSONObject();
                    JSONObject Authority = new JSONObject();
                    JSONArray obj2 = new JSONArray(array.toString());
                    Authority.put("Currency", currency);
                    AvailabilitySearch.put("Authority", Authority);
                    AvailabilitySearch.put("CityName", cityName);
                    AvailabilitySearch.put("CheckInDate", check_in_date);
                    AvailabilitySearch.put("CheckOutDate", check_out_date);
                    AvailabilitySearch.put("Searchid", searchId);
                    AvailabilitySearch.put("Nationality", nationality);
                    AvailabilitySearch.put("Room", obj2);
                    JSONObject obj = new JSONObject(AvailabilitySearch.toString());
                    mainObject.put("AvailabilitySearch", obj);

                    /*
                    else else {
                    }*/
                    if (!isChildAvail) {
                        if (mAdultFlag) {
                            sendBookingInfo(mainObject.toString());
                        }
                    } else {
                        if (mAdultFlag && mChildFlag) {
                            sendBookingInfo(mainObject.toString());
                        }
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private void init() {
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new AppDialogs(ctx);
        back = (ImageView) findViewById(R.id.back);
        hotelImage = (ImageView) findViewById(R.id.hotelImage);
        hotel_name = (TextView) findViewById(R.id.hotel_name);
        hotel_address = (TextView) findViewById(R.id.hotel_address);
        in_day = (TextView) findViewById(R.id.in_day);
        in_month = (TextView) findViewById(R.id.in_month);
        in_date = (TextView) findViewById(R.id.in_date);
        out_day = (TextView) findViewById(R.id.out_day);
        out_month = (TextView) findViewById(R.id.out_month);
        out_date = (TextView) findViewById(R.id.out_date);
        duration = (TextView) findViewById(R.id.duration);
        room_type = (TextView) findViewById(R.id.room_type);
        board_type = (TextView) findViewById(R.id.board_type);
        total_price = (TextView) findViewById(R.id.total_price);
        ratings = (RatingBar) findViewById(R.id.ratings);
        recyclerView = (RecyclerView) findViewById(R.id.room_list);
        payNow = (LinearLayout) findViewById(R.id.payNow);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        cancellationPolicy = (LinearLayout) findViewById(R.id.cancellationPolicy);

        if (getIntent().getStringExtra("strHotelName") != null) {
            strHotelName = getIntent().getStringExtra("strHotelName");
            hotel_name.setText(strHotelName);
        }
        if (getIntent().getStringExtra("strHotelAddress") != null && getIntent().getStringExtra("strHotelDestination") != null) {
            strHotelAddress = getIntent().getStringExtra("strHotelAddress");
            strHotelDestination = getIntent().getStringExtra("strHotelDestination");
            hotel_address.setText(strHotelAddress + ", " + strHotelDestination);
        }
        if (getIntent().getStringExtra("strRating") != null && !getIntent().getStringExtra("strRating").isEmpty()) {
            strRating = getIntent().getStringExtra("strRating");
            ratings.setRating(Float.parseFloat(strRating));
        }
        if (getIntent().getStringExtra("boardType") != null) {
            boardType = getIntent().getStringExtra("boardType");
            setHtmlAsText(board_type, boardType);
        }
        if (getIntent().getStringExtra("roomCategory") != null) {
            roomCategory = getIntent().getStringExtra("roomCategory");
            setHtmlAsText(room_type, roomCategory);
        }
        if (getIntent().getStringExtra("roomRate") != null) {
            roomRate = getIntent().getStringExtra("roomRate");
        }
        if (getIntent().getStringExtra("roomProcessId") != null) {
            roomProcessId = getIntent().getStringExtra("roomProcessId");
        }
        if (getIntent().getStringExtra("hotelCode") != null) {
            hotelCode = getIntent().getStringExtra("hotelCode");
        }
        if (getIntent().getStringExtra("supplier") != null) {
            supplier = getIntent().getStringExtra("supplier");
        }
    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            finish();
        } else if (view == cancellationPolicy) {
            if (cancellationMessage != null && !cancellationMessage.isEmpty()) {
                displayDialog(cancellationMessage);
            }
        }
    }

    private void getSearchInfoAPI() {
        if (cd.isConnectingToInternet()) {

            d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<SearchInfoDataPojo> call = apiService.getSearchInfo(getFromPrefs(AppConstants.RANDOM_NO)/*"497732"*/);
            call.enqueue(new Callback<SearchInfoDataPojo>() {
                @Override
                public void onResponse(Call<SearchInfoDataPojo> call, Response<SearchInfoDataPojo> response) {

                    if (response.body() != null) {
                        basePojo = response.body();
                        if (response.body().getCityname() != null && !response.body().getCityname().isEmpty()) {
                            cityName = response.body().getCityname();
                        }
                        if (response.body().getSearchId() != null && !response.body().getSearchId().isEmpty()) {
                            searchId = response.body().getSearchId();
                        } else {
                            searchId = getFromPrefs(AppConstants.RANDOM_NO);
                        }

                        if (response.body().getCurrency() != null && !response.body().getCurrency().isEmpty()) {
                            currency = response.body().getCurrency();
                            total_price.setText(currency + " " + roomRate);
                        } else {
                            currency = getFromPrefs(AppConstants.CURRENCY);
                            total_price.setText(currency + " " + roomRate);
                        }

                        if (response.body().getNationality() != null && !response.body().getNationality().isEmpty()) {
                            nationality = response.body().getNationality();
                        } else {
                            nationality = getFromPrefs(AppConstants.NATIONALITY);
                        }

                        if (response.body().getCheckInDate() != null && !response.body().getCheckInDate().isEmpty()) {
                            checkInDateAsString = changeDateFormate(response.body().getCheckInDate());
                            check_in_date = chaneDateFormateIntoString(changeStingIntoDate(checkInDateAsString));
                            String[] arrCheckIn = checkInDateAsString.split("-");
                            in_day.setText(arrCheckIn[0]);
                            in_date.setText(arrCheckIn[1]);
                            in_month.setText(arrCheckIn[2]);
                        }

                        if (response.body().getCheckOutDate() != null && !response.body().getCheckOutDate().isEmpty()) {
                            checkOutDateAsString = changeDateFormate(response.body().getCheckOutDate());
                            check_out_date = chaneDateFormateIntoString(changeStingIntoDate(checkOutDateAsString));
                            String[] arrCheckOut = checkOutDateAsString.split("-");
                            out_day.setText(arrCheckOut[0]);
                            out_date.setText(arrCheckOut[1]);
                            out_month.setText(arrCheckOut[2]);
                        }

                        if (response.body().getSearchRoom() != null && response.body().getSearchRoom().size() > 0) {
                            for (int i = 0; i < response.body().getSearchRoom().size(); i++) {
                                searchInfoDataPojo = response.body();
                                ArrayList<AdultPojo> adult = new ArrayList<>();
                                ArrayList<AdultPojo> child = new ArrayList<>();
                                for (int j = 0; j < (Integer.parseInt(response.body().getSearchRoom().get(i).getAdult())); j++) {
                                    AdultPojo adultpojo = new AdultPojo();
                                    adult.add(adultpojo);
                                }
                                for (int j = 0; j < (Integer.parseInt(response.body().getSearchRoom().get(i).getChild())); j++) {
                                    AdultPojo childPojo = new AdultPojo();
                                    child.add(childPojo);}

                                response.body().getSearchRoom().get(i).setAdultPojo(adult);
                                response.body().getSearchRoom().get(i).setChildPojo(child);
                            }
                            mAdapter = new RoomListAdapter(ctx, response.body().getSearchRoom());
                            recyclerView.setAdapter(mAdapter);
                        }
                    } else {
                        dialog.displayCommonDialog(getResources().getString(R.string.search_info_failed), AppConstants.SERVER_NOT_RESPONDING);
                    }
                }

                @Override
                public void onFailure(Call<SearchInfoDataPojo> call, Throwable t) {
                    // Log error here since request failed
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    private String changeDateFormate(String dtStart) {
        String returningType = "";

        try {
            SimpleDateFormat dateFormat1 = new SimpleDateFormat("EEE-dd-MMM-yyyy");
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date date = dateFormat.parse(dtStart);
            String dateTime = dateFormat1.format(date);
            returningType = dateTime;
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returningType;
    }

    private void getHotelCancellationAPI() {

        if (cd.isConnectingToInternet()) {

            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<CancellationPolicyPojo> call = apiService.getHotelCancellationPolicy(hotelCode, supplier, getFromPrefs(AppConstants.RANDOM_NO), roomProcessId);
            call.enqueue(new Callback<CancellationPolicyPojo>() {
                @Override
                public void onResponse(Call<CancellationPolicyPojo> call, Response<CancellationPolicyPojo> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getStatus().equalsIgnoreCase("true")) {
                                cancellationMessage = response.body().getCancellationMessage();
                            }
                            else
                            {
                                displayDialogError(response.body().getCancellationMessage());
                            }
                            d.dismiss();
                        } else {
                            d.dismiss();
                            dialog.displayCommonDialog(getResources().getString(R.string.cancellation_failed), AppConstants.SERVER_NOT_RESPONDING);
                        }
                    } else {
                        d.dismiss();
                        dialog.displayCommonDialog(getResources().getString(R.string.cancellation_failed), AppConstants.SERVER_NOT_RESPONDING);
                    }
                }

                @Override
                public void onFailure(Call<CancellationPolicyPojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void sendBookingInfo(String jsonString) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = AppDialogs.showPaymentLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<SendBookingInfoPojo> call = apiService.sendBookingInfo(jsonString, hotelCode, supplier, searchId, roomProcessId, cancellationMessage, currency);
            call.enqueue(new Callback<SendBookingInfoPojo>() {
                @Override
                public void onResponse(Call<SendBookingInfoPojo> call, Response<SendBookingInfoPojo> response) {
                    if (response.body() != null) {

                        if (response.body().getStatus() != null) {
                            d.dismiss();
                            if (response.body().getStatus().equalsIgnoreCase("true")) {
                                if (response.body().getFailure_endpoint() != null && !response.body().getFailure_endpoint().isEmpty())
                                {
                                    failure_endpoint = response.body().getFailure_endpoint();
                                }
                                if (response.body().getFailure_url() != null && !response.body().getFailure_url().isEmpty())
                                {
                                    failure_url = response.body().getFailure_url();
                                }
                                if (response.body().getSuccess_endpoint() != null && !response.body().getSuccess_endpoint().isEmpty())
                                {
                                    success_endpoint = response.body().getSuccess_endpoint();
                                }
                                if (response.body().getSuccess_url() != null && !response.body().getSuccess_url().isEmpty())
                                {
                                    success_url = response.body().getSuccess_url();
                                }

                                Intent intent = new Intent(ctx, PaymentActivity.class);
                                intent.putExtra("searchId", searchId);
                                intent.putExtra("hotelName", strHotelName);
                                intent.putExtra("failure_endpoint", failure_endpoint);
                                intent.putExtra("failure_url", failure_url);
                                intent.putExtra("success_endpoint", success_endpoint);
                                intent.putExtra("success_url", success_url);
                                startActivityForResult(intent, PAYMENT_STATUS);
                                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                            }
                        } else {
                            d.dismiss();
                            dialog.displayCommonDialog(getResources().getString(R.string.booking_info_failed), AppConstants.SERVER_NOT_RESPONDING);
                        }
                    } else {
                        d.dismiss();
                        dialog.displayCommonDialog(getResources().getString(R.string.booking_info_failed), AppConstants.SERVER_NOT_RESPONDING);
                    }

                }

                @Override
                public void onFailure(Call<SendBookingInfoPojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PAYMENT_STATUS) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                if (data.getStringExtra("result").equals("success")) {
                    for (int i = 0; i < AppConstants.ACTIVITIES.size(); i++) {
                        if (AppConstants.ACTIVITIES.get(i) != null && AppConstants.ACTIVITIES.get(i).toString().contains(getResources().getString(R.string.package_name) + ".HotelDetailsActivity")) {
                            AppConstants.ACTIVITIES.get(i).finish();
                            AppConstants.ACTIVITIES.remove(i);
                            break;
                        }
                    }
                    Toast.makeText(ctx, "This transaction is Successfull.", Toast.LENGTH_SHORT).show();
                    navigateToBookingInfoActivity();

                } else {
                    dialog.displayCommonDialog("Payment Failed", "This transaction could not be completed. Please try again.");
                }
            } else {
                dialog.displayCommonDialog("Payment Failed", "Error Code");
            }
        }
    }
    private void navigateToBookingInfoActivity()
    {
        Intent intent = new Intent(ctx, BookingInfoActivity.class);
        startActivity(intent);
        finish();
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    public void displayDialogError(String msg) {
        LinearLayout OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_dialog);
        TextView loogout_msg = (TextView) DialogLogOut.findViewById(R.id.text_exit);
        setHtmlAsText(loogout_msg, msg);
        loogout_msg.setMovementMethod(new ScrollingMovementMethod());
        OkButtonLogout = (LinearLayout) DialogLogOut.findViewById(R.id.btn_yes_exit_LL);
        OkButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();
                finish();
            }
        });
        DialogLogOut.show();
    }
}
