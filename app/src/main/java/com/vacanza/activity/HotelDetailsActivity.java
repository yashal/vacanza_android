package com.vacanza.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.View;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;

import java.util.ArrayList;

import com.vacanza.Pojo.CancellationPolicyPojo;
import com.vacanza.Pojo.HotelDetailsDataPojo;

import example.com.hotelbooking.R;

import com.vacanza.Pojo.HotelFacePojo;
import com.vacanza.Pojo.HotelRoomPojo;
import com.vacanza.adapter.HotelRoomFragmentsAdapter;
import com.vacanza.model.ApiClient;
import com.vacanza.model.ApiInterface;
import com.vacanza.utils.AppConstants;
import com.vacanza.utils.AppDialogs;
import com.vacanza.utils.ConnectionDetector;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pr0 on 4/8/17.
 */

public class HotelDetailsActivity extends BaseActivity implements View.OnClickListener {

    RecyclerView roomsView;
    private ImageView back, left_arr, right_arr;
    private SliderLayout slidingImages;
    CardView price;
    private HotelDetailsActivity ctx = this;
    private String hotel_code = "";
    private String supplier = "";
    private String currency = "";
    private ConnectionDetector cd;
    private AppDialogs dialog;
    private TextView booking_dates, no_of_guests, hotel_name, hotel_address, hotel_description, counter;
    private String strHotelName, strHotelAddress, strHotelDestination, strRating;
    private RatingBar ratings;
    private HorizontalScrollView facilities_list;
    private CardView hotel_facelity, description, roomcard;
    private ViewPager pager;
    private RelativeLayout arrowRL;
    private ArrayList<HotelRoomPojo> hotelRooms;
    private NestedScrollView nestedScrollView;
    private LinearLayout no_details_layout;

    @Override
    protected void onCreate(Bundle onSaveInstanceState) {
        super.onCreate(onSaveInstanceState);
        setContentView(R.layout.hotel_details_activity);

        init();

        AppConstants.ACTIVITIES.add(ctx);

        getHotelDetailsAPI();
        back.setOnClickListener(this);

        pager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageSelected(int pos) {
                if (pos == 0) {
                    left_arr.setVisibility(View.GONE);
                } else if (pos == (hotelRooms.size() - 1)) {
                    right_arr.setVisibility(View.GONE);
                } else {
                    right_arr.setVisibility(View.VISIBLE);
                    left_arr.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
                counter.setText(arg0 + 1 + "  of  " + hotelRooms.size());
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {

            }
        });
    }

    private void init() {
        facilities_list = (HorizontalScrollView) findViewById(R.id.facilities_list);
        hotel_description = (TextView) findViewById(R.id.hotel_description);
        hotel_name = (TextView) findViewById(R.id.hotel_name);
        hotel_address = (TextView) findViewById(R.id.hotel_address);
        no_of_guests = (TextView) findViewById(R.id.no_of_guests);
        booking_dates = (TextView) findViewById(R.id.booking_dates);
        ratings = (RatingBar) findViewById(R.id.ratings);
        hotel_facelity = (CardView) findViewById(R.id.hotel_facelity);
        description = (CardView) findViewById(R.id.description);
        roomcard = (CardView) findViewById(R.id.roomcard);
        pager = (ViewPager) findViewById(R.id.viewpager);
        back = (ImageView) findViewById(R.id.back);
        left_arr = (ImageView) findViewById(R.id.left_arr);
        right_arr = (ImageView) findViewById(R.id.right_arr);
        slidingImages = (SliderLayout) findViewById(R.id.gallery);
        arrowRL = (RelativeLayout) findViewById(R.id.arrowRL);
        counter = (TextView) findViewById(R.id.counter);
        nestedScrollView = (NestedScrollView) findViewById(R.id.nestedScrollView);
        no_details_layout = (LinearLayout) findViewById(R.id.no_details_layout);
        pager.setOffscreenPageLimit(3);
        if (getIntent().getStringExtra("hotel_code") != null) {
            hotel_code = getIntent().getStringExtra("hotel_code");
        }
        if (getIntent().getStringExtra("supplier") != null) {
            supplier = getIntent().getStringExtra("supplier");
        }
        if (getIntent().getStringExtra("currency") != null) {
            currency = getIntent().getStringExtra("currency");
        }
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new AppDialogs(ctx);

        if (getIntent().getStringExtra("total_person") != null) {
            no_of_guests.setText(getIntent().getStringExtra("total_person"));
        }
        if (getIntent().getStringExtra("booking_date") != null) {
            booking_dates.setText(getIntent().getStringExtra("booking_date"));
        }
        hotelRooms = new ArrayList<>();

        left_arr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = pager.getCurrentItem();
                if (tab > 0) {
                    tab--;
                    pager.setCurrentItem(tab);
                } else if (tab == 0) {
                    pager.setCurrentItem(tab);
                }
            }
        });

        // Images right navigatin
        right_arr.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int tab = pager.getCurrentItem();
                tab++;
                pager.setCurrentItem(tab);
            }
        });

    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            finish();
        } /*else if(view == price) {
            Intent i = new Intent(this, GuestDetailsActivity.class);
            startActivity(i);
        }*/ else if (view.getId() == R.id.cancellationPolicy) {
            String room_process_id = (String) view.getTag(R.string.key);
            getHotelCancellationAPI(room_process_id);
        } else if (view.getId() == R.id.book) {
            String board_type = (String) view.getTag(R.string.key);
            String room_category = (String) view.getTag(R.string.data);
            String room_rate = (String) view.getTag(R.string.value);
            String room_process_id = (String) view.getTag(R.string.set);
            Intent intent = new Intent(ctx, BucketActivity.class);
            intent.putExtra("strHotelName", strHotelName);
            intent.putExtra("strHotelAddress", strHotelAddress);
            intent.putExtra("strHotelDestination", strHotelDestination);
            intent.putExtra("strRating", strRating);
            intent.putExtra("boardType", board_type);
            intent.putExtra("roomCategory", room_category);
            intent.putExtra("roomRate", room_rate);
            intent.putExtra("roomProcessId", room_process_id);
            intent.putExtra("hotelCode", hotel_code);
            intent.putExtra("supplier", supplier);
            startActivity(intent);
            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);

        }
    }

    private void getHotelDetailsAPI() {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<HotelDetailsDataPojo> call = apiService.getHotelDetails(hotel_code, supplier, getFromPrefs(AppConstants.RANDOM_NO));
            call.enqueue(new Callback<HotelDetailsDataPojo>() {
                @Override
                public void onResponse(Call<HotelDetailsDataPojo> call, Response<HotelDetailsDataPojo> response) {
                    if (response.body() != null) {

                        if (response.body().getStatus() != null) {
                            if (response.body().getStatus().equalsIgnoreCase("true")) {
                                nestedScrollView.setVisibility(View.VISIBLE);
                                no_details_layout.setVisibility(View.GONE);
                                // get from images from url and set them into Gallery
                                if (response.body().getHotelImages() != null && response.body().getHotelImages().size() > 0) {
                                    for (int i = 0; i < response.body().getHotelImages().size(); ++i) {
                                        DefaultSliderView sliderView = new DefaultSliderView(ctx);
                                        sliderView.image(response.body().getHotelImages().get(i).getImageurl()).setScaleType(BaseSliderView.ScaleType.Fit);

                                        slidingImages.addSlider(sliderView);
                                    }
                                    slidingImages.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                                    slidingImages.stopAutoCycle();
                                } else {
                                    DefaultSliderView sliderView = new DefaultSliderView(ctx);
                                    sliderView.image(R.drawable.logo).setScaleType(BaseSliderView.ScaleType.Fit);
                                    slidingImages.addSlider(sliderView);
                                    slidingImages.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                                    slidingImages.stopAutoCycle();
                                }
                                // set rating
                                if (response.body().getStarrating() != null && !response.body().getStarrating().isEmpty()) {
                                    strRating = response.body().getStarrating();
                                    ratings.setRating(Float.parseFloat(strRating));
                                }

                                //set hotel name
                                if (response.body().getHotelname() != null && !response.body().getHotelname().isEmpty()) {
                                    strHotelName = response.body().getHotelname();
                                    hotel_name.setText(strHotelName);
                                }
                                //set hotel address
                                if (response.body().getHoteladdress() != null && !response.body().getHoteladdress().isEmpty()) {
                                    strHotelAddress = response.body().getHoteladdress();
                                    strHotelDestination = response.body().getHoteldestination();
                                    hotel_address.setText(strHotelAddress + ", " + strHotelDestination);
                                }
                                // set Hotel Amenities
                                if (response.body().getHotelfac() != null && response.body().getHotelfac().size() > 0) {
                                    hotel_facelity.setVisibility(View.VISIBLE);
                                    setFacilities(response.body().getHotelfac());
                                }
                                if (response.body().getHoteldescription() != null && !response.body().getHoteldescription().isEmpty()) {
                                    description.setVisibility(View.VISIBLE);
                                    setHtmlAsText(hotel_description, response.body().getHoteldescription());
                                }
                                if (response.body().getHotelRooms() != null && response.body().getHotelRooms().size() > 0) {
                                    roomcard.setVisibility(View.VISIBLE);
                                    arrowRL.setVisibility(View.VISIBLE);
                                    hotelRooms = response.body().getHotelRooms();
                                    counter.setText("1  of  " + hotelRooms.size());

                                    if (hotelRooms.size() == 1) {
                                        right_arr.setVisibility(View.GONE);
                                        left_arr.setVisibility(View.GONE);
                                    }
                                    HotelRoomFragmentsAdapter adapter = new HotelRoomFragmentsAdapter(ctx.getSupportFragmentManager(),
                                            ctx, response.body().getHotelRooms(), currency);
                                    pager.setAdapter(adapter);
                                }
                            } else {
                                nestedScrollView.setVisibility(View.GONE);
                                no_details_layout.setVisibility(View.VISIBLE);
                            }

                            d.dismiss();
                        } else {
                            d.dismiss();
                            dialog.displayCommonDialog(getResources().getString(R.string.hotel_detail_failed), AppConstants.SERVER_NOT_RESPONDING);
                        }
                    } else {
                        d.dismiss();
                        dialog.displayCommonDialog(getResources().getString(R.string.hotel_detail_failed), AppConstants.SERVER_NOT_RESPONDING);
                    }

                }

                @Override
                public void onFailure(Call<HotelDetailsDataPojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void setFacilities(final ArrayList<HotelFacePojo> hotelAmenities) {
        LinearLayout topLinearLayout = new LinearLayout(this);
        // topLinearLayout.setLayoutParams(android.widget.LinearLayout.LayoutParams.FILL_PARENT,android.widget.LinearLayout.LayoutParams.FILL_PARENT);
        topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);
        topLinearLayout.setPadding(15, 10, 10, 10);
        for (int i = 0; i < hotelAmenities.size(); i++) {

            LinearLayout innerLayout = new LinearLayout(this);
            innerLayout.setOrientation(LinearLayout.VERTICAL);
            LinearLayout.LayoutParams innerLayoutLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            innerLayoutLayoutParams.setMargins(20, 10, 20, 10);

            innerLayout.setLayoutParams(innerLayoutLayoutParams);


            TextView facility_name = new TextView(ctx);
            facility_name.setText(hotelAmenities.get(i).getFacilityname());
            facility_name.setTextSize(getResources().getDimension(R.dimen.text_desc));
            if ((i % 5) == 0) {
                facility_name.setTextColor(ContextCompat.getColor(ctx, R.color.btn_textColor));
            } else if ((i % 5) == 1) {
                facility_name.setTextColor(ContextCompat.getColor(ctx, R.color.color_accent_pressed));
            } else if ((i % 5) == 2) {
                facility_name.setTextColor(ContextCompat.getColor(ctx, R.color.blue));
            } else if ((i % 5) == 3) {
                facility_name.setTextColor(ContextCompat.getColor(ctx, R.color.currency));
            } else if ((i % 5) == 4) {
                facility_name.setTextColor(ContextCompat.getColor(ctx, R.color.orange));
            }


            LinearLayout.LayoutParams descLayoutParams = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
            descLayoutParams.setMargins(0, 5, 0, 0);
            descLayoutParams.gravity = Gravity.CENTER;
            facility_name.setLayoutParams(descLayoutParams);

            innerLayout.addView(facility_name);
            topLinearLayout.addView(innerLayout);

        }

        facilities_list.addView(topLinearLayout);
    }

    private void getHotelCancellationAPI(String processId) {
        if (cd.isConnectingToInternet()) {
            final ProgressDialog d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<CancellationPolicyPojo> call = apiService.getHotelCancellationPolicy(hotel_code, supplier, getFromPrefs(AppConstants.RANDOM_NO), processId);
            call.enqueue(new Callback<CancellationPolicyPojo>() {
                @Override
                public void onResponse(Call<CancellationPolicyPojo> call, Response<CancellationPolicyPojo> response) {
                    if (response.body() != null) {

                        if (response.body().getStatus() != null) {
                            if (response.body().getStatus().equalsIgnoreCase("true")) {
                                displayDialog(response.body().getCancellationMessage());
                            }
                            d.dismiss();
                        } else {
                            d.dismiss();
                            dialog.displayCommonDialog(getResources().getString(R.string.cancellation_failed), AppConstants.SERVER_NOT_RESPONDING);
                        }
                    } else {
                        d.dismiss();
                        dialog.displayCommonDialog(getResources().getString(R.string.cancellation_failed), AppConstants.SERVER_NOT_RESPONDING);
                    }

                }

                @Override
                public void onFailure(Call<CancellationPolicyPojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }


}
