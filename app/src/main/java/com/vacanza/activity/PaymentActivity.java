package com.vacanza.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.http.SslError;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.os.Bundle;
import android.view.View;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;

import com.vacanza.utils.AppConstants;
import com.vacanza.utils.AppDialogs;

import example.com.hotelbooking.R;

public class PaymentActivity extends BaseActivity {

    private WebView webview;
    private PaymentActivity ctx = this;
    private ProgressDialog progressDialog;
    private String coupon_type;
    private float grand_total;
    private String loaded_url = "";
    private String failure_endpoint = "";
    private String failure_url = "";
    private String success_endpoint = "";
    private String success_url = "";

    private String baseUrlForPayment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        if (getIntent().getStringExtra("failure_endpoint") != null)
        {
            failure_endpoint = getIntent().getStringExtra("failure_endpoint");
        }
        if (getIntent().getStringExtra("failure_url") != null)
        {
            failure_url = getIntent().getStringExtra("failure_url");
        }
        if (getIntent().getStringExtra("success_endpoint") != null)
        {
            success_endpoint = getIntent().getStringExtra("success_endpoint");
        }
        if (getIntent().getStringExtra("success_url") != null)
        {
            success_url = getIntent().getStringExtra("success_url");
        }

        baseUrlForPayment = AppConstants.BASE_URL_PAYMENT + getIntent().getStringExtra("searchId") + "&hotelname=" + getIntent().getStringExtra("hotelName");

        progressDialog = AppDialogs.showPaymentLoading(ctx);
        ImageView back = (ImageView) findViewById(R.id.back);
        back.setVisibility(View.VISIBLE);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


        webview = (WebView) findViewById(R.id.webview);

        webview.getSettings().setJavaScriptEnabled(true);

        webview.setWebChromeClient(new WebChromeClient() {
            public void onProgressChanged(WebView view, int progress) {
                if (progress >= 95) {
                    if (!ctx.isFinishing() && progressDialog != null && progressDialog.isShowing()) {
                        progressDialog.dismiss();
                    }
                }
            }
        });

        webview.setWebViewClient(new WebViewClient() {
            @Override
            public void onReceivedSslError(WebView view, final SslErrorHandler handler, SslError error) {
                AlertDialog.Builder builder = new AlertDialog.Builder(ctx);
                AlertDialog alertDialog = builder.create();
                String message = "SSL Certificate error.";
                switch (error.getPrimaryError()) {
                    case SslError.SSL_UNTRUSTED:
                        message = "The certificate authority is not trusted.";
                        break;
                    case SslError.SSL_EXPIRED:
                        message = "The certificate has expired.";
                        break;
                    case SslError.SSL_IDMISMATCH:
                        message = "The certificate Hostname mismatch.";
                        break;
                    case SslError.SSL_NOTYETVALID:
                        message = "The certificate is not yet valid.";
                        break;
                }

                message = "You are now being redirected to payment gateway page";
                alertDialog.setTitle("");
                alertDialog.setMessage(message);
                alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Ignore SSL certificate errors
                        handler.proceed();
                    }
                });

                alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        handler.cancel();
                    }
                });
                alertDialog.show();

            }

            public boolean shouldOverrideUrlLoading(WebView view, final String url) {
                if (!ctx.isFinishing() && progressDialog != null && progressDialog.isShowing())
                    progressDialog.dismiss();
                if (url.toLowerCase().startsWith("http") || url.toLowerCase().startsWith("https")) {
                    if (url.contains(success_endpoint) || url.contains(failure_endpoint)) {
                        progressDialog = AppDialogs.showLoading(ctx);
                        progressDialog.setTitle("Please Wait");
                        progressDialog.setMessage("Redirecting back to the app.");
                        try {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    progressDialog.dismiss();
                                    Intent returnIntent = new Intent();
                                    if (url.contains(failure_url)) {
                                        returnIntent.putExtra("result", "fail");
                                    } else if (url.contains(success_url)) {
                                        returnIntent.putExtra("result", "success");
                                    }
                                    setResult(Activity.RESULT_OK, returnIntent);
                                    finish();
                                }
                            }, 5000);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else if (url.equals("http://www.myvacanza.com/") || url.equals("https://www.myvacanza.com/")) {
                        Intent returnIntent = new Intent();
                        returnIntent.putExtra("result", "fail");
                        setResult(Activity.RESULT_OK, returnIntent);
                        finish();
                    }
                }
                return (true);
            }
        });

        openURL(baseUrlForPayment);


    }

    private void openURL(String url) {
        loaded_url = url;
        webview.loadUrl(url);
        webview.requestFocus();
    }

}
