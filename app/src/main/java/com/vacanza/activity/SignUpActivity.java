package com.vacanza.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.vacanza.Pojo.LoginPojo;

import example.com.hotelbooking.R;

import com.vacanza.model.ApiClient;
import com.vacanza.model.ApiInterface;
import com.vacanza.utils.AppConstants;
import com.vacanza.utils.AppDialogs;
import com.vacanza.utils.ConnectionDetector;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pr0 on 11/8/17.
 */

public class SignUpActivity extends BaseActivity implements View.OnClickListener {

    private SignUpActivity ctx = this;
    private ImageView back;
    private CardView login, signup;
    private EditText email, password, confirmPassword, phone, lastName, firstName;
    private ConnectionDetector cd;
    private AppDialogs dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signup_activity);
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new AppDialogs(ctx);
        back = (ImageView) findViewById(R.id.back);
        login = (CardView) findViewById(R.id.login);
        signup = (CardView) findViewById(R.id.signup);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        confirmPassword = (EditText) findViewById(R.id.confirmPassword);
        phone = (EditText) findViewById(R.id.phone);
        lastName = (EditText) findViewById(R.id.lastName);
        firstName = (EditText) findViewById(R.id.firstName);
        back.setOnClickListener(this);
        login.setOnClickListener(this);
        signup.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {

        if (view == back) {
            finish();
        } else if (view == login) {
            Intent intent = new Intent(ctx, LoginActivity.class);
            startActivity(intent);
            finish();
        } else if (view == signup) {
            String str_firstName = firstName.getText().toString();
            String str_lastName = lastName.getText().toString();
            String str_email = email.getText().toString();
            String str_phone = phone.getText().toString();
            String str_password = password.getText().toString();
            String str_confirmPassword = confirmPassword.getText().toString();

            if (str_firstName.equals("")) {
                dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), getResources().getString(R.string.empty_firstname));
            } else if (str_lastName.equals("")) {
                dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), getResources().getString(R.string.empty_lastname));
            } else if (str_email.equals("")) {
                dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), getResources().getString(R.string.empty_email));
            } else if (!isValidEmail(str_email)) {
                dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), getResources().getString(R.string.valid_email));
            } else if (str_phone.equals("") && !str_phone.matches("[0-9]+")) {
                dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), getResources().getString(R.string.valid_mobile_reg));
            } else if (str_password.equals("")) {
                dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), getResources().getString(R.string.blank_password));
            } else if (str_confirmPassword.equals("")) {
                dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), getResources().getString(R.string.blank_confirm_password));
            } else if (!str_confirmPassword.equals(str_password)) {
                dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), getResources().getString(R.string.mismatch_password));
            } else {
                doSignUp(str_firstName, str_lastName, str_email, str_phone, str_password);
            }

        }

    }

    private void doSignUp(final String firstName_str, final String lastName_str, final String email_str, final String phone_str, String password_str) {
        if (cd.isConnectingToInternet()) {

            final ProgressDialog d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<LoginPojo> call = apiService.doSignUp(firstName_str, lastName_str,"","", email_str, phone_str, password_str);
            call.enqueue(new Callback<LoginPojo>() {
                @Override
                public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                    d.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            saveIntoPrefs(AppConstants.FIRST_NAME, response.body().getFirstname());
                            saveIntoPrefs(AppConstants.LAST_NAME, response.body().getLastname());
                            saveIntoPrefs(AppConstants.NAME, response.body().getFirstname() + " " + response.body().getLastname());
                            saveIntoPrefs(AppConstants.EMAIL, response.body().getEmailid());
                            saveIntoPrefs(AppConstants.MEMBER_TYPE, "APP");
                            saveIntoPrefs(AppConstants.PHONE, response.body().getPhone());
                            Intent intent = new Intent(ctx, HomeActivity.class);
                            startActivity(intent);
                            finish();
                        } else {
                            dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), response.body().getMessage());
                        }
                    } else {
                        dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), AppConstants.SERVER_NOT_RESPONDING);
                    }

                }

                @Override
                public void onFailure(Call<LoginPojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        }
        else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }
}
