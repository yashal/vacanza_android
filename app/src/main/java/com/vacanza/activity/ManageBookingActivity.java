package com.vacanza.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.vacanza.Pojo.ManageBookingPojoDetail;
import com.vacanza.model.ApiClient;
import com.vacanza.model.ApiInterface;
import com.vacanza.utils.AppConstants;
import com.vacanza.utils.AppDialogs;
import com.vacanza.utils.ConnectionDetector;

import example.com.hotelbooking.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pr0 on 8/30/17.
 */

public class ManageBookingActivity extends BaseActivity implements View.OnClickListener {

    ImageView back;
    CardView bookings;
    EditText lname, book_id;
    private ConnectionDetector cd;
    private ManageBookingActivity ctx = this;
    private AppDialogs dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.manage_booking);
        AppConstants.ACTIVITIES.add(ctx);
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new AppDialogs(ctx);
        back = (ImageView) findViewById(R.id.back);
        bookings = (CardView) findViewById(R.id.bookings);
        lname = (EditText) findViewById(R.id.lname);
        book_id = (EditText) findViewById(R.id.booking_no);

        bookings.setOnClickListener(this);
        back.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (back == view) {
            finish();
        } else if (view == bookings) {
            if (lname.getText().toString().isEmpty() || book_id.getText().toString().isEmpty()) {
                Toast.makeText(this, "Please enter all the details", Toast.LENGTH_SHORT).show();
            }else{
                manageBooking(lname.getText().toString(),book_id.getText().toString());
            }
        }
    }
    private void manageBooking(String lastName,String bookingNo) {
        if (cd.isConnectingToInternet()) {

            final ProgressDialog d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<ManageBookingPojoDetail> call = apiService.managebooking(bookingNo,lastName);
            call.enqueue(new Callback<ManageBookingPojoDetail>() {
                @Override
                public void onResponse(Call<ManageBookingPojoDetail> call, Response<ManageBookingPojoDetail> response) {
                    d.dismiss();
                    if (response.body() != null) {
                        if (response.body().getCommonList() != null && response.body().getStatus().equalsIgnoreCase("true")) {
                            String strCode = response.body().getCommonList().get(0).getStrcode();
                            String strName = response.body().getCommonList().get(0).getStrname();
                            saveIntoPrefs(AppConstants.RANDOM_NO, strCode);
                            Intent intent = new Intent(ctx, BookingInfoActivity.class);
                            intent.putExtra("FROM","ManageBooking");
                            startActivity(intent);
                        } else {
                            dialog.displayCommonDialog(getResources().getString(R.string.search_info_failed), AppConstants.SERVER_NOT_RESPONDING);
                        }

                    }
                }
                @Override
                public void onFailure (Call < ManageBookingPojoDetail > call, Throwable t){
                    d.dismiss();
                }

            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

}
