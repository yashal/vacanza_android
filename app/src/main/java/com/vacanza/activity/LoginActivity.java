package com.vacanza.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.vacanza.Pojo.LoginPojo;

import example.com.hotelbooking.R;

import com.vacanza.model.ApiClient;
import com.vacanza.model.ApiInterface;
import com.vacanza.utils.AppConstants;
import com.vacanza.utils.AppDialogs;
import com.vacanza.utils.ConnectionDetector;

import org.json.JSONObject;

import java.util.Arrays;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Yash on 11/10/2017.
 */

public class LoginActivity extends BaseActivity implements View.OnClickListener {

    private LoginActivity ctx = this;
    private CardView login, signup, fb_login;
    private TextView forgetPass;
    private EditText email, password;
    private ConnectionDetector cd;
    private CallbackManager callbackManager;
    private AppDialogs dialog;
    private  ProgressDialog d ;
    private String strHotelName = "", strHotelAddress = "", strHotelDestination = "", strRating = "",
            board_type = "", room_category = "", room_rate = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login_activity);
        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();

        if (getIntent().getStringExtra("strHotelName") != null) {
            strHotelName = getIntent().getStringExtra("strHotelName");
        }
        if (getIntent().getStringExtra("strHotelAddress") != null && getIntent().getStringExtra("strHotelDestination") != null) {
            strHotelAddress = getIntent().getStringExtra("strHotelAddress");
            strHotelDestination = getIntent().getStringExtra("strHotelDestination");
        }
        if (getIntent().getStringExtra("strRating") != null && !getIntent().getStringExtra("strRating").isEmpty()) {
            strRating = getIntent().getStringExtra("strRating");
        }
        if (getIntent().getStringExtra("boardType") != null) {
            board_type = getIntent().getStringExtra("boardType");
        }
        if (getIntent().getStringExtra("roomCategory") != null) {
            room_category = getIntent().getStringExtra("roomCategory");
        }
        if (getIntent().getStringExtra("roomRate") != null) {
            room_rate = getIntent().getStringExtra("roomRate");
        }
        login = (CardView) findViewById(R.id.login);
        signup = (CardView) findViewById(R.id.signup);
        fb_login = (CardView) findViewById(R.id.fb_login);
        forgetPass = (TextView) findViewById(R.id.forgetPass);
        email = (EditText) findViewById(R.id.email);
        password = (EditText) findViewById(R.id.password);
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new AppDialogs(ctx);
        login.setOnClickListener(this);
        signup.setOnClickListener(this);
        fb_login.setOnClickListener(this);
        forgetPass.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        if (view == login) {

            String str_email = email.getText().toString().trim();
            String str_password = password.getText().toString().trim();

            if (str_email.equals("")) {
                dialog.displayCommonDialog(getResources().getString(R.string.login_failed), getResources().getString(R.string.empty_email));
            } else if (!isValidEmail(str_email)) {
                dialog.displayCommonDialog(getResources().getString(R.string.login_failed), getResources().getString(R.string.valid_email));
            } else if (str_password.equals("")) {
                dialog.displayCommonDialog(getResources().getString(R.string.login_failed), getResources().getString(R.string.empty_password));
            } else {
                doLogInWithEmail(str_email, str_password);
            }

        } else if (view == signup) {
            Intent i = new Intent(ctx, SignUpActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        } else if (view == fb_login) {
            try {
                if (cd.isConnectingToInternet()) {

                    LoginManager.getInstance().logInWithReadPermissions(ctx, Arrays.asList("public_profile", "user_friends","user_birthday","email"));

                } else {
//                            dialog.NoInternetConnection(ctx);
                }


                try {
                    LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
                        @Override
                        public void onSuccess(LoginResult loginResult) {
                            String token = loginResult.getAccessToken().getToken();
                            String uid = loginResult.getAccessToken().getUserId();
//                            d = AppDialogs.showServerDataLoading(ctx);
                            System.out.println("token" + token);
                            System.out.println("uid" + uid);
                            setFacebookData(loginResult, uid);
                        }

                        @Override
                        public void onCancel() {
                            Toast.makeText(ctx, "Login cancelled by user!", Toast.LENGTH_LONG).show();
                            System.out.println("Facebook Login failed!!");
                        }

                        @Override
                        public void onError(FacebookException error) {
                            Toast.makeText(ctx, "Login unsuccessful!", Toast.LENGTH_LONG).show();
                            System.out.println("Facebook Login failed!! because of " + error.getCause().toString());
                        }
                    });
                } catch (NullPointerException e) {
                    System.out.println("Exception on fb callback" + e);
                }

            } catch (NoClassDefFoundError e) {
                System.out.println("Exception " + e);
            }

        } else if (view == forgetPass) {
            Intent i = new Intent(ctx, ForgetPasswordActivity.class);
            startActivity(i);
            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
        }
    }

    private void doLogInWithEmail(String email_str, String password_str) {
        if (cd.isConnectingToInternet()) {

            final ProgressDialog d = AppDialogs.showServerDataLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<LoginPojo> call = apiService.doLogin(email_str, password_str);
            call.enqueue(new Callback<LoginPojo>() {
                @Override
                public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                    d.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            saveIntoPrefs(AppConstants.FIRST_NAME, response.body().getFirstname());
                            saveIntoPrefs(AppConstants.LAST_NAME, response.body().getLastname());
                            saveIntoPrefs(AppConstants.NAME, response.body().getFirstname() + " " + response.body().getLastname());
                            saveIntoPrefs(AppConstants.EMAIL, response.body().getEmailid());
                            saveIntoPrefs(AppConstants.PHONE, response.body().getPhone());
                            Intent intent = new Intent(ctx, HomeActivity.class);
                            startActivity(intent);
                            finish();
                                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                        } else {
                            dialog.displayCommonDialog(getResources().getString(R.string.login_failed), response.body().getMessage());
                        }
                    } else {
                        dialog.displayCommonDialog(getResources().getString(R.string.login_failed), AppConstants.SERVER_NOT_RESPONDING);
                    }

                }

                @Override
                public void onFailure(Call<LoginPojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        }
        else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    /***************************************  facebook Login ***********************/
    private void setFacebookData(final LoginResult loginResult,final String uid) {

        GraphRequest request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(),

                new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(JSONObject object, GraphResponse response) {
// Application code
                        try {
                            String birthday="";
                            Log.i("Response", response.toString());

                            String firstName = response.getJSONObject().getString("first_name");
                            String lastName = response.getJSONObject().getString("last_name");
                            String gender = response.getJSONObject().getString("gender");
                            String email = response.getJSONObject().optString("email");
                            if(!response.getJSONObject().optString("birthday").equalsIgnoreCase("")) {
                                birthday = response.getJSONObject().optString("birthday");
                            }else{
                                birthday="01/01/1993";//Default age
                            }
                            System.out.println("BIRTHDAY"+birthday);
                            System.out.println("EMAIL"+email);
                            doSignUp(firstName,lastName,email,"","",uid);
//                            register(firstName,lastName,age,gender,"facebook",uid);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,email,first_name,last_name,gender, birthday");
        request.setParameters(parameters);
        request.executeAsync();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            super.onActivityResult(requestCode, resultCode, data);
            callbackManager.onActivityResult(requestCode, resultCode, data);

        } catch (Exception ex) {

        }

    }

    private void doSignUp(final String firstName_str, final String lastName_str, final String email_str, final String phone_str, String password_str,String uid) {
        if (cd.isConnectingToInternet()) {

            final ProgressDialog d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<LoginPojo> call = apiService.doSignUp(firstName_str, lastName_str,"facebook",uid, email_str, phone_str, password_str);
            call.enqueue(new Callback<LoginPojo>() {
                @Override
                public void onResponse(Call<LoginPojo> call, Response<LoginPojo> response) {
                    d.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            saveIntoPrefs(AppConstants.FIRST_NAME, response.body().getFirstname());
                            saveIntoPrefs(AppConstants.LAST_NAME, response.body().getLastname());
                            saveIntoPrefs(AppConstants.NAME, response.body().getFirstname() + " " + response.body().getLastname());
                            saveIntoPrefs(AppConstants.EMAIL, response.body().getEmailid());
                            saveIntoPrefs(AppConstants.MEMBER_TYPE, "FACEBOOK");
                            saveIntoPrefs(AppConstants.PHONE, response.body().getPhone());
                            Intent intent = new Intent(ctx, HomeActivity.class);
                            startActivity(intent);
                            finish();
                            overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                        } else {
                            dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), response.body().getMessage());
                        }
                    } else {
                        dialog.displayCommonDialog(getResources().getString(R.string.registration_failed), AppConstants.SERVER_NOT_RESPONDING);
                    }

                }

                @Override
                public void onFailure(Call<LoginPojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        }
        else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }
}
