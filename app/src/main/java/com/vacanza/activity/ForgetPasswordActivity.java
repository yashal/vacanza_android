package com.vacanza.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.vacanza.Pojo.BasePojo;
import com.vacanza.Pojo.LoginPojo;
import com.vacanza.model.ApiClient;
import com.vacanza.model.ApiInterface;
import com.vacanza.utils.AppConstants;
import com.vacanza.utils.AppDialogs;
import com.vacanza.utils.ConnectionDetector;

import example.com.hotelbooking.R;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pr0 on 8/27/17.
 */

public class ForgetPasswordActivity extends BaseActivity implements View.OnClickListener {

    private ForgetPasswordActivity ctx = this;

    private ImageView back;
    private LinearLayout reset;
    private EditText emailBox;

    private String email = "";
    private ConnectionDetector cd;
    private AppDialogs dialog;

    @Override
    protected void onCreate(Bundle savedInstaceState) {
        super.onCreate(savedInstaceState);
        setContentView(R.layout.forgot_password_lay);

        cd = new ConnectionDetector(getApplicationContext());
        dialog = new AppDialogs(ctx);

        back = (ImageView) findViewById(R.id.back);
        reset = (LinearLayout) findViewById(R.id.reset);
        emailBox = (EditText) findViewById(R.id.email);

        back.setOnClickListener(this);
        reset.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            finish();
        } else if (view == reset) {
            email = emailBox.getText().toString();
            if (TextUtils.isEmpty(email)) {
                dialog.displayCommonDialog(getResources().getString(R.string.forgot_password_failed), getResources().getString(R.string.empty_email));
            } else if (!isValidEmail(email)) {
                dialog.displayCommonDialog(getResources().getString(R.string.forgot_password_failed), getResources().getString(R.string.valid_email));
            }
            else {
                forgotPasswordNetworkCall(email);
            }
        }
    }
    private void forgotPasswordNetworkCall(String emailId)
    {
        if (cd.isConnectingToInternet()) {

            final ProgressDialog d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<BasePojo> call = apiService.forgotPassword(emailId);
            call.enqueue(new Callback<BasePojo>() {
                @Override
                public void onResponse(Call<BasePojo> call, Response<BasePojo> response) {
                    d.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            if (TextUtils.isEmpty(response.body().getMessage())) {
                                Toast.makeText(ctx, response.body().getMessage(), Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(ctx, LoginActivity.class);
                                startActivity(intent);
                                finish();
                                overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
                            }
                        } else {
                            dialog.displayCommonDialog(getResources().getString(R.string.forgot_password_failed), response.body().getMessage());
                        }
                    } else {
                        dialog.displayCommonDialog(getResources().getString(R.string.forgot_password_failed), AppConstants.SERVER_NOT_RESPONDING);
                    }

                }

                @Override
                public void onFailure(Call<BasePojo> call, Throwable t) {
                    // Log error here since request failed
                    d.dismiss();
                }
            });
        }
        else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }
}
