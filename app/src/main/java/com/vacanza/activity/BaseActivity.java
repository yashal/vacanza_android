package com.vacanza.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.text.method.ScrollingMovementMethod;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

import example.com.hotelbooking.R;

import com.vacanza.fragment.DrawerFragment;
import com.vacanza.utils.AppConstants;


/**
 * Created by Administrator on 22-Aug-16.
 */
public class BaseActivity extends AppCompatActivity {

    public DrawerLayout mDrawerLayout;
    private SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);
    }

    public Toolbar setDrawerAndToolbar(String name) {
        Toolbar appbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(appbar);
        getSupportActionBar().setTitle("");

        TextView header = (TextView) findViewById(R.id.header);
        header.setText(name);

        final LinearLayout drawer_button = (LinearLayout) findViewById(R.id.drawerButton);

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideSoftKeyboard();
                if (mDrawerLayout != null && !mDrawerLayout.isDrawerOpen(Gravity.LEFT))
                    mDrawerLayout.openDrawer(Gravity.LEFT);
                else if (mDrawerLayout != null)
                    mDrawerLayout.closeDrawers();
            }
        });
        DrawerFragment fragment = (DrawerFragment) getSupportFragmentManager().findFragmentById(R.id.fragment_drawer);
        fragment.setUp(mDrawerLayout);
        return appbar;
    }

    public Toolbar setToolbar(String name) {
        Toolbar appbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(appbar);
        getSupportActionBar().setTitle("");

        TextView header = (TextView) findViewById(R.id.header);
        header.setText(name);

        return appbar;
    }

    public void hideSoftKeyboard() {
        if (getCurrentFocus() != null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    public void saveIntoPrefs(String key, String value) {
        SharedPreferences prefs = getSharedPreferences(AppConstants.PREF_NAME, MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public String getFromPrefs(String key) {
        SharedPreferences prefs = getSharedPreferences(AppConstants.PREF_NAME, MODE_PRIVATE);
        return prefs.getString(key, AppConstants.DEFAULT_VALUE);
    }

    public void saveIntoRoomPrefs(String key, String value) {
        SharedPreferences prefs = getSharedPreferences(AppConstants.FMS_PRFERENCES_ROOM, MODE_PRIVATE);
        SharedPreferences.Editor edit = prefs.edit();
        edit.putString(key, value);
        edit.commit();
    }

    public String getFromRoomPrefs(String key) {
        SharedPreferences prefs = getSharedPreferences(AppConstants.FMS_PRFERENCES_ROOM, MODE_PRIVATE);
        return prefs.getString(key, AppConstants.DEFAULT_VALUE);
    }

    public static void clearRoomData(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(AppConstants.FMS_PRFERENCES_ROOM, Context.MODE_PRIVATE);
        preferences.edit().clear().commit();
    }

    public static void clearAppData(Context context) {
        SharedPreferences preferences = context.getSharedPreferences(AppConstants.PREF_NAME, Context.MODE_PRIVATE);
        preferences.edit().clear().commit();
    }

    public void setImageInLayout(Context ctx, int width, int height, String url, ImageView image) {
        if (url.isEmpty()) {
            image.setImageResource(R.drawable.logo);
        } else {
            Picasso.with(ctx).load(url).resize(width, height).placeholder(R.drawable.logo).error(R.drawable.logo).into(image);
        }

    }

    public void setImageInLayoutHome(Context ctx, int width, int height, String url, ImageView image) {
        if (url.isEmpty()) {
            image.setImageResource(R.drawable.logo);
        } else {
            Picasso.with(ctx).load(url).fit().centerCrop().placeholder(R.drawable.logo).error(R.drawable.logo).into(image);
        }
    }

    public int getScreenHeight() {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int height = displaymetrics.heightPixels;
        int width = displaymetrics.widthPixels;

        return height;
    }

    public SimpleDateFormat getDateFormat() {
        String myFormat = "EEE-dd-MMM-yyyy"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        return sdf;
    }


    // dynamically permission added

    /**
     * method that will return whether the permission is accepted. By default it is true if the user is using a device below
     * version 23
     *
     * @param permission
     * @return
     */

    public boolean hasPermission(String permission) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (canMakeSmores()) {
                return (checkSelfPermission(permission) == PackageManager.PERMISSION_GRANTED);
            }
        }
        return true;
    }

    /**
     * method to determine whether we have asked
     * for this permission before.. if we have, we do not want to ask again.
     * They either rejected us or later removed the permission.
     *
     * @param permission
     * @return
     */
    public boolean shouldWeAsk(String permission) {
        return (sharedPreferences.getBoolean(permission, true));
    }

    /**
     * we will save that we have already asked the user
     *
     * @param permission
     */
    public void markAsAsked(String permission, SharedPreferences sharedPreferences) {
        sharedPreferences.edit().putBoolean(permission, false).apply();
    }

    /**
     * We may want to ask the user again at their request.. Let's clear the
     * marked as seen preference for that permission.
     *
     * @param permission
     */
    public void clearMarkAsAsked(String permission) {
        sharedPreferences.edit().putBoolean(permission, true).apply();
    }


    /**
     * This method is used to determine the permissions we do not have accepted yet and ones that we have not already
     * bugged the user about.  This comes in handle when you are asking for multiple permissions at once.
     *
     * @param wanted
     * @return
     */
    public ArrayList<String> findUnAskedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * this will return us all the permissions we have previously asked for but
     * currently do not have permission to use. This may be because they declined us
     * or later revoked our permission. This becomes useful when you want to tell the user
     * what permissions they declined and why they cannot use a feature.
     *
     * @param wanted
     * @return
     */
    public ArrayList<String> findRejectedPermissions(ArrayList<String> wanted) {
        ArrayList<String> result = new ArrayList<String>();

        for (String perm : wanted) {
            if (!hasPermission(perm) && !shouldWeAsk(perm)) {
                result.add(perm);
            }
        }

        return result;
    }

    /**
     * Just a check to see if we have marshmallows (version 23)
     *
     * @return
     */
    public boolean canMakeSmores() {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1);
    }

    /**
     * a method that will centralize the showing of a snackbar
     */
    public void makePostRequestSnack(String message, int size) {

        Toast.makeText(getApplicationContext(), String.valueOf(size) + " " + message, Toast.LENGTH_SHORT).show();

        finish();
    }

    public boolean isValidEmail(CharSequence target) {
        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

    public void openSoftKeyBoard(EditText et) {
        et.requestFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(et, InputMethodManager.SHOW_IMPLICIT);
    }

    public void displayDialog(String msg) {
        LinearLayout OkButtonLogout;
        final Dialog DialogLogOut = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        DialogLogOut.setContentView(R.layout.custom_dialog);
        TextView loogout_msg = (TextView) DialogLogOut.findViewById(R.id.text_exit);
        setHtmlAsText(loogout_msg, msg);
        loogout_msg.setMovementMethod(new ScrollingMovementMethod());
        OkButtonLogout = (LinearLayout) DialogLogOut.findViewById(R.id.btn_yes_exit_LL);
        OkButtonLogout.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                DialogLogOut.dismiss();


            }
        });
        DialogLogOut.show();
    }

    public void setHtmlAsText(TextView tv, String message) {
        if (tv != null) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                tv.setText(Html.fromHtml(message, Html.FROM_HTML_MODE_COMPACT));
            } else {
                tv.setText(Html.fromHtml(message));
            }
        }
    }

    public String chaneDateFormateIntoString(Date date) {
        DateFormat dateFormatforInAPI = new SimpleDateFormat("yyyy-MM-dd");
        String dateAsStringforAPI = dateFormatforInAPI.format(date);
        return dateAsStringforAPI;
    }

    public Date changeStingIntoDate(String string) {
        DateFormat format = new SimpleDateFormat("EEE-dd-MMM-yyyy", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(string);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return date;
    }

}