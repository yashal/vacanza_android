package com.vacanza.activity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;

import com.google.maps.android.clustering.ClusterManager;
import com.vacanza.Pojo.CommonListPojo;
import com.vacanza.Pojo.CountryPojo;
import com.vacanza.Pojo.HotelDataPojo;
import com.vacanza.Pojo.MyClusterItem;
import com.vacanza.Pojo.SearchResultPojo;

import example.com.hotelbooking.R;

import com.vacanza.adapter.HotelListAdapter;
import com.vacanza.adapter.NationalityAdapter;
import com.vacanza.model.ApiClient;
import com.vacanza.model.ApiInterface;
import com.vacanza.utils.AppConstants;
import com.vacanza.utils.AppDialogs;
import com.vacanza.utils.ConnectionDetector;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by pr0 on 1/8/17.
 */

public class SearchResultsActivity extends BaseActivity implements View.OnClickListener, OnMapReadyCallback/*, OnClusterItemClickListener*/ {
    private ImageView back;
    private SearchResultsActivity ctx = this;
    private LinearLayout sortBtn, filterBtn, mapBtn, priceBtn, tabLay;
    private ArrayList<HotelDataPojo> hotel_detail_arr;
    private String total_person, mainObject;
    private TextView no_of_guests, booking_dates;
    private RecyclerView recyclerView, currency_recycler;
    private RecyclerView.LayoutManager mLayoutManager, mCurrencyLayoutManager;
    private RecyclerView.Adapter mAdapter, mCurrencyAdapter;
    private ConnectionDetector cd;
    private AppDialogs dialog;
    private LinearLayout no_item_layout, currencyLay;
    private TextView noOfHotelsText;
    private ProgressDialog d;
    private int selectedItem = 1;
    private RelativeLayout mapLayout;
    private GoogleMap googleMap;
    private ClusterManager<MyClusterItem> mClusterManager;
    private double lattitude, longitude;
    private boolean mFlag = true;

    private ArrayList<CommonListPojo> currencyList;
    private int requestCodeFilter = 556;

    @Override
    protected void onCreate(Bundle onSavedInstanceState) {
        super.onCreate(onSavedInstanceState);
        setContentView(R.layout.result_activity);

        init();

        AppConstants.ACTIVITIES.add(ctx);

        if (getIntent().getStringExtra("total_person") != null) {
            total_person = getIntent().getStringExtra("total_person");
            no_of_guests.setText(total_person + "Guest(s)");
        }

        if (getIntent().getStringExtra("check_in_date") != null && !getIntent().getStringExtra("check_in_date").isEmpty() &&
                getIntent().getStringExtra("check_out_date") != null && !getIntent().getStringExtra("check_out_date").isEmpty()) {
            booking_dates.setText(getIntent().getStringExtra("check_in_date") + " - " + getIntent().getStringExtra("check_out_date"));
        }

        if (getIntent().getStringExtra("mainObject") != null) {
            mainObject = getIntent().getStringExtra("mainObject");
        }

        hotel_detail_arr = new ArrayList<>();
        back.setOnClickListener(this);
        sortBtn.setOnClickListener(this);
        priceBtn.setOnClickListener(this);
        mapBtn.setOnClickListener(this);
        filterBtn.setOnClickListener(this);
        getRXMLHotelsAPI();
//        getcurrencyAPI();

    }

    @Override
    public void onClick(View view) {
        if (view == back) {
            if (mFlag) {
                super.onBackPressed();
            } else {
                mFlag = true;
                mapLayout.setVisibility(View.GONE);
                recyclerView.setVisibility(View.VISIBLE);
                currencyLay.setVisibility(View.GONE);
            }
        } else if (view == sortBtn) {
            mapLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            currencyLay.setVisibility(View.GONE);
            customFilterDialog();
            mFlag = true;
        } else if (view == mapBtn) {
            mFlag = false;
            mapLayout.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            currencyLay.setVisibility(View.GONE);

            for (int z = 0; z < hotel_detail_arr.size(); z++) {
                if (hotel_detail_arr.get(z).getLatitude() != null && !hotel_detail_arr.get(z).getLatitude().isEmpty() &&
                        hotel_detail_arr.get(z).getLongitude() != null && !hotel_detail_arr.get(z).getLongitude().isEmpty()) {
                    udpateMapView(Double.parseDouble(hotel_detail_arr.get(z).getLatitude()),
                            Double.parseDouble(hotel_detail_arr.get(z).getLongitude()), 10);
                }
            }
        } else if (view == priceBtn) {
            mFlag = false;
            mapLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.GONE);
            currencyLay.setVisibility(View.VISIBLE);
        } else if (view == filterBtn) {
            mFlag = true;
//            mapLayout.setVisibility(View.GONE);
//            recyclerView.setVisibility(View.GONE);
//            currencyLay.setVisibility(View.GONE);
            Intent intent = new Intent(ctx, FilterActivity.class);
            startActivityForResult(intent, requestCodeFilter);
        } else if (view.getId() == R.id.rootLayout) {
            String hotel_code = (String) view.getTag(R.string.key);
            String supplier = (String) view.getTag(R.string.data);
            String hotelImage = (String) view.getTag(R.string.value);
            String currency = (String) view.getTag(R.string.set);
            saveIntoPrefs(AppConstants.HOTEL_IMAGE, "");
            saveIntoPrefs(AppConstants.HOTEL_IMAGE, hotelImage);
            navigateToHotelDetails(hotel_code, supplier, currency);
        } else if (view.getId() == R.id.rootLL) {  // TODO this currency click is for further refrence.
            String str_code = (String) view.getTag(R.string.key);
            String str_from = (String) view.getTag(R.string.data);
            if (str_from.equals("Currency")) {
                mainObject = mainObject.replace(getFromPrefs(AppConstants.CURRENCY), str_code);
                saveIntoPrefs(AppConstants.CURRENCY, "");
                saveIntoPrefs(AppConstants.CURRENCY, str_code);
                mFlag = true;
                getRXMLHotelsAPI();
                recyclerView.setVisibility(View.VISIBLE);
                currencyLay.setVisibility(View.GONE);

            }
        }
    }

    private void navigateToHotelDetails(String hotel_code, String supplier, String currency) {
        Intent i = new Intent(ctx, HotelDetailsActivity.class);
        i.putExtra("hotel_code", hotel_code);
        i.putExtra("supplier", supplier);
        i.putExtra("currency", currency);
        i.putExtra("total_person", no_of_guests.getText().toString().trim());
        i.putExtra("booking_date", booking_dates.getText().toString().trim());
        startActivity(i);
        overridePendingTransition(R.anim.pull_in_left, R.anim.push_out_right);
    }

    public void customFilterDialog() {
        LinearLayout price_low_to_high, price_high_to_low, name_a_to_z, name_z_to_a, star_0_to_5, star_5_to_0;
        ImageView one, two, three, four, five, six;
        final Dialog filter_dialog = new Dialog(this, android.R.style.Theme_Translucent_NoTitleBar);
        filter_dialog.setContentView(R.layout.custom_filter_dialog);
        filter_dialog.setCanceledOnTouchOutside(false);
        if (!filter_dialog.isShowing()) {

            one = (ImageView) filter_dialog.findViewById(R.id.one);
            two = (ImageView) filter_dialog.findViewById(R.id.two);
            three = (ImageView) filter_dialog.findViewById(R.id.three);
            four = (ImageView) filter_dialog.findViewById(R.id.four);
            five = (ImageView) filter_dialog.findViewById(R.id.five);
            six = (ImageView) filter_dialog.findViewById(R.id.six);

            price_low_to_high = (LinearLayout) filter_dialog.findViewById(R.id.price_low_to_high);
            price_high_to_low = (LinearLayout) filter_dialog.findViewById(R.id.price_high_to_low);
            name_a_to_z = (LinearLayout) filter_dialog.findViewById(R.id.name_a_to_z);
            name_z_to_a = (LinearLayout) filter_dialog.findViewById(R.id.name_z_to_a);
            star_0_to_5 = (LinearLayout) filter_dialog.findViewById(R.id.star_0_to_5);
            star_5_to_0 = (LinearLayout) filter_dialog.findViewById(R.id.star_5_to_0);

            setStyle(one, two, three, four, five, six);

            price_low_to_high.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shortByPriceLowToHigh();
                    filter_dialog.dismiss();
                    selectedItem = 1;
                }
            });

            price_high_to_low.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shortByPriceHighToLow();
                    filter_dialog.dismiss();
                    selectedItem = 2;
                }
            });

            name_a_to_z.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shortByNameAToZ();
                    filter_dialog.dismiss();
                    selectedItem = 3;
                }
            });

            name_z_to_a.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shortByNameZToA();
                    filter_dialog.dismiss();
                    selectedItem = 4;
                }
            });

            star_0_to_5.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shortByStarZeroToFive();
                    filter_dialog.dismiss();
                    selectedItem = 5;
                }
            });

            star_5_to_0.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    shortByStarFiveToZero();
                    filter_dialog.dismiss();
                    selectedItem = 6;
                }
            });

            filter_dialog.show();
        }
    }

    private void setStyle(ImageView img1, ImageView img2, ImageView img3, ImageView img4, ImageView img5, ImageView img6) {
        if (selectedItem == 1) {
            img1.setImageResource(R.mipmap.radio_selected);
            img2.setImageResource(R.mipmap.radio_box);
            img3.setImageResource(R.mipmap.radio_box);
            img4.setImageResource(R.mipmap.radio_box);
            img5.setImageResource(R.mipmap.radio_box);
            img6.setImageResource(R.mipmap.radio_box);
        } else if (selectedItem == 2) {
            img1.setImageResource(R.mipmap.radio_box);
            img2.setImageResource(R.mipmap.radio_selected);
            img3.setImageResource(R.mipmap.radio_box);
            img4.setImageResource(R.mipmap.radio_box);
            img5.setImageResource(R.mipmap.radio_box);
            img6.setImageResource(R.mipmap.radio_box);
        } else if (selectedItem == 3) {
            img1.setImageResource(R.mipmap.radio_box);
            img2.setImageResource(R.mipmap.radio_box);
            img3.setImageResource(R.mipmap.radio_selected);
            img4.setImageResource(R.mipmap.radio_box);
            img5.setImageResource(R.mipmap.radio_box);
            img6.setImageResource(R.mipmap.radio_box);
        } else if (selectedItem == 4) {
            img1.setImageResource(R.mipmap.radio_box);
            img2.setImageResource(R.mipmap.radio_box);
            img3.setImageResource(R.mipmap.radio_box);
            img4.setImageResource(R.mipmap.radio_selected);
            img5.setImageResource(R.mipmap.radio_box);
            img6.setImageResource(R.mipmap.radio_box);
        } else if (selectedItem == 5) {
            img1.setImageResource(R.mipmap.radio_box);
            img2.setImageResource(R.mipmap.radio_box);
            img3.setImageResource(R.mipmap.radio_box);
            img4.setImageResource(R.mipmap.radio_box);
            img5.setImageResource(R.mipmap.radio_selected);
            img6.setImageResource(R.mipmap.radio_box);
        } else if (selectedItem == 6) {
            img1.setImageResource(R.mipmap.radio_box);
            img2.setImageResource(R.mipmap.radio_box);
            img3.setImageResource(R.mipmap.radio_box);
            img4.setImageResource(R.mipmap.radio_box);
            img5.setImageResource(R.mipmap.radio_box);
            img6.setImageResource(R.mipmap.radio_selected);
        }
    }

    private void init() {
        no_of_guests = (TextView) findViewById(R.id.no_of_guests);
        booking_dates = (TextView) findViewById(R.id.booking_dates);
        back = (ImageView) findViewById(R.id.back);
        mapLayout = (RelativeLayout) findViewById(R.id.mapLayout);
        tabLay = (LinearLayout) findViewById(R.id.tab);
        sortBtn = (LinearLayout) findViewById(R.id.sort_btn);
        mapBtn = (LinearLayout) findViewById(R.id.map_btn);
        priceBtn = (LinearLayout) findViewById(R.id.price_btn);
        filterBtn = (LinearLayout) findViewById(R.id.filter_btn);
        no_item_layout = (LinearLayout) findViewById(R.id.no_item_layout);
        noOfHotelsText = (TextView) findViewById(R.id.noOfHotelsText);
        recyclerView = (RecyclerView) findViewById(R.id.sortedView);
        recyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setNestedScrollingEnabled(false);
        cd = new ConnectionDetector(getApplicationContext());
        dialog = new AppDialogs(ctx);

        currencyLay = (LinearLayout) findViewById(R.id.currencyLay);
        currency_recycler = (RecyclerView) findViewById(R.id.currency_recycler);
        currency_recycler.setHasFixedSize(true);
        mCurrencyLayoutManager = new LinearLayoutManager(ctx, LinearLayoutManager.VERTICAL, false);
        currency_recycler.setLayoutManager(mCurrencyLayoutManager);
        currency_recycler.setNestedScrollingEnabled(false);

        currencyList = new ArrayList<>();

        MapFragment mapFragment = (MapFragment) getFragmentManager()
                .findFragmentById(R.id.mapFragment);
        mapFragment.getMapAsync(this);
    }

    // short by price low to high method definition
    private void shortByPriceLowToHigh() {

        try {
            Collections.sort(hotel_detail_arr, new Comparator<HotelDataPojo>() {
                public int compare(HotelDataPojo s1, HotelDataPojo s2) {
                    // notice the cast to (Double) to invoke compareTo
                    NumberFormat f = NumberFormat.getInstance();
                    double previousPrice = 0;
                    double nextPrice = 0;
                    try {
                        previousPrice = f.parse(s1.getTotalPrice()).doubleValue();
                        nextPrice = f.parse(s2.getTotalPrice()).doubleValue();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return ((Double) (previousPrice)).compareTo((Double) (nextPrice));
                }
            });
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
        mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    // short by price high to low method definition
    private void shortByPriceHighToLow() {
        try {
            Collections.sort(hotel_detail_arr, new Comparator<HotelDataPojo>() {
                public int compare(HotelDataPojo s1, HotelDataPojo s2) {
                    // notice the cast to (Double) to invoke compareTo
                    NumberFormat f = NumberFormat.getInstance();
                    double previousPrice = 0;
                    double nextPrice = 0;
                    try {
                        previousPrice = f.parse(s1.getTotalPrice()).doubleValue();
                        nextPrice = f.parse(s2.getTotalPrice()).doubleValue();
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    return -((Double) (previousPrice)).compareTo((Double) (nextPrice));
                }
            });
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }

        mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    // short by Name A To Z method definition
    private void shortByNameAToZ() {
        Collections.sort(hotel_detail_arr, new Comparator<HotelDataPojo>() {
            public int compare(HotelDataPojo s1, HotelDataPojo s2) {
                // notice the cast to (Integer) to invoke compareTo
                return s1.getHotelName().compareTo(s2.getHotelName());
            }
        });

        mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    // short by Name Z To A method definition
    private void shortByNameZToA() {
        Collections.sort(hotel_detail_arr, new Comparator<HotelDataPojo>() {
            public int compare(HotelDataPojo s1, HotelDataPojo s2) {
                // notice the cast to (Integer) to invoke compareTo
                return s2.getHotelName().compareTo(s1.getHotelName());
            }
        });

        mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    // short by star 0 to 5 method definition
    private void shortByStarZeroToFive() {
        Collections.sort(hotel_detail_arr, new Comparator<HotelDataPojo>() {
            public int compare(HotelDataPojo s1, HotelDataPojo s2) {
                // notice the cast to (Integer) to invoke compareTo
                return ((Float) (Float.parseFloat(s1.getStar()))).compareTo((Float) (Float.parseFloat(s2.getStar())));
            }
        });

        mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    // short by star 0 to 5 method definition
    private void shortByStarFiveToZero() {
        Collections.sort(hotel_detail_arr, new Comparator<HotelDataPojo>() {
            public int compare(HotelDataPojo s1, HotelDataPojo s2) {
                // notice the cast to (Integer) to invoke compareTo
                return ((Float) (Float.parseFloat(s2.getStar()))).compareTo((Float) (Float.parseFloat(s1.getStar())));
            }
        });

        mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
        recyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
    }

    private void getRXMLHotelsAPI() {
        if (cd.isConnectingToInternet()) {
            d = AppDialogs.showLoading(ctx);
            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            String membertype = getFromPrefs(AppConstants.MEMBER_TYPE);
            System.out.println("RXML_MemberType:" + membertype);
            Call<SearchResultPojo> call = apiService.getRXMLHotels(mainObject.toString(), membertype);

            call.enqueue(new Callback<SearchResultPojo>() {
                @Override
                public void onResponse(Call<SearchResultPojo> call, Response<SearchResultPojo> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getHotels() != null && response.body().getHotels().size() > 0) {
                                dialogDismis();
                                hotel_detail_arr = response.body().getHotels();
                                shortByPriceLowToHigh();
//                                mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
//                                recyclerView.setAdapter(mAdapter);
                                setPropertiesCount(hotel_detail_arr);
                            }
                        }
                    }

                    getJacHotels();
                }

                @Override
                public void onFailure(Call<SearchResultPojo> call, Throwable t) {
                    // Log error here since request failed
                    getJacHotels();
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void getJacHotels() {
        if (cd.isConnectingToInternet()) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            String membertype = getFromPrefs(AppConstants.MEMBER_TYPE);
            System.out.println("JacHotels_MemberType:" + membertype);

            Call<SearchResultPojo> call = apiService.getJacHotels(mainObject.toString(), membertype);
            call.enqueue(new Callback<SearchResultPojo>() {
                @Override
                public void onResponse(Call<SearchResultPojo> call, Response<SearchResultPojo> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getHotels() != null && response.body().getHotels().size() > 0) {
                                dialogDismis();
//                              hotel_detail_arr = response.body().getHotels();
                                hotel_detail_arr.addAll(response.body().getHotels());
                                shortByPriceLowToHigh();
//                                mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
//                                recyclerView.setAdapter(mAdapter);
                                setPropertiesCount(hotel_detail_arr);
                            }
                        }
                    }
                    getDotwHotels();
                }

                @Override
                public void onFailure(Call<SearchResultPojo> call, Throwable t) {
                    // Log error here since request failed
                    getDotwHotels();

                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void getDotwHotels() {
        if (cd.isConnectingToInternet()) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            String membertype = getFromPrefs(AppConstants.MEMBER_TYPE);
            System.out.println("DotwHotels_MemberType:" + membertype);

            Call<SearchResultPojo> call = apiService.getDotwHotels(mainObject.toString(), membertype);
            call.enqueue(new Callback<SearchResultPojo>() {
                @Override
                public void onResponse(Call<SearchResultPojo> call, Response<SearchResultPojo> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getHotels() != null && response.body().getHotels().size() > 0) {
                                dialogDismis();
//                                hotel_detail_arr = response.body().getHotels();
                                hotel_detail_arr.addAll(response.body().getHotels());
                                shortByPriceLowToHigh();
//                                mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
//                                recyclerView.setAdapter(mAdapter);
                                setPropertiesCount(hotel_detail_arr);
                            }
                        }
                    }
                    getGtaHotels();
                }

                @Override
                public void onFailure(Call<SearchResultPojo> call, Throwable t) {
                    // Log error here since request failed
                    getGtaHotels();

                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void getGtaHotels() {
        if (cd.isConnectingToInternet()) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            String membertype = getFromPrefs(AppConstants.MEMBER_TYPE);
            System.out.println("GtaHotels_MemberType:" + membertype);

            Call<SearchResultPojo> call = apiService.getGtaHotels(mainObject.toString(), membertype);
            call.enqueue(new Callback<SearchResultPojo>() {
                @Override
                public void onResponse(Call<SearchResultPojo> call, Response<SearchResultPojo> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getHotels() != null && response.body().getHotels().size() > 0) {
                                dialogDismis();
//                                hotel_detail_arr = response.body().getHotels();
                                hotel_detail_arr.addAll(response.body().getHotels());
                                shortByPriceLowToHigh();
//                                mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
//                                recyclerView.setAdapter(mAdapter);
                                setPropertiesCount(hotel_detail_arr);
                            }
                        }
                    }
                    getFITHotels();
                }

                @Override
                public void onFailure(Call<SearchResultPojo> call, Throwable t) {
                    // Log error here since request failed
                    getFITHotels();

                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void getFITHotels() {
        if (cd.isConnectingToInternet()) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            String membertype = getFromPrefs(AppConstants.MEMBER_TYPE);
            System.out.println("FITHotels_MemberType:" + membertype);

            Call<SearchResultPojo> call = apiService.getFITHotels(mainObject.toString(), membertype);
            call.enqueue(new Callback<SearchResultPojo>() {
                @Override
                public void onResponse(Call<SearchResultPojo> call, Response<SearchResultPojo> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getHotels() != null && response.body().getHotels().size() > 0) {
                                dialogDismis();
//                                hotel_detail_arr = response.body().getHotels();
                                hotel_detail_arr.addAll(response.body().getHotels());
                                shortByPriceLowToHigh();
//                                mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
//                                recyclerView.setAdapter(mAdapter);
                                setPropertiesCount(hotel_detail_arr);
                            }
                        }
                    }
                    getHBHotelsAPI();
                }

                @Override
                public void onFailure(Call<SearchResultPojo> call, Throwable t) {
                    // Log error here since request failed
                    getHBHotelsAPI();

                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void getHBHotelsAPI() {
        if (cd.isConnectingToInternet()) {

            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);

            String membertype = getFromPrefs(AppConstants.MEMBER_TYPE);
            System.out.println("HBHotels_MemberType:" + membertype);

            Call<SearchResultPojo> call = apiService.getHBHotels(mainObject.toString(), membertype);
            call.enqueue(new Callback<SearchResultPojo>() {
                @Override
                public void onResponse(Call<SearchResultPojo> call, Response<SearchResultPojo> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getHotels() != null && response.body().getHotels().size() > 0) {
                                dialogDismis();
                                hotel_detail_arr.addAll(response.body().getHotels());
                                shortByPriceLowToHigh();
//                                mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
//                                recyclerView.setAdapter(mAdapter);
//                                mAdapter.notifyDataSetChanged();
                                setPropertiesCount(hotel_detail_arr);
                                no_item_layout.setVisibility(View.GONE);
                            } else {
                                if (hotel_detail_arr.size() == 0) {
                                    no_item_layout.setVisibility(View.VISIBLE);
                                }
                                dialogDismis();
                            }
                        } else {
                            dialogDismis();
                            if (hotel_detail_arr.size() == 0) {
                                dialog.displayCommonDialog(getResources().getString(R.string.search_hotel_failed), AppConstants.SERVER_NOT_RESPONDING);
                            }
                        }
                    } else {
                        dialogDismis();
                        if (hotel_detail_arr.size() == 0) {
                            dialog.displayCommonDialog(getResources().getString(R.string.search_hotel_failed), AppConstants.SERVER_NOT_RESPONDING);
                        }
                    }

                }

                @Override
                public void onFailure(Call<SearchResultPojo> call, Throwable t) {
                    // Log error here since request failed
                    dialogDismis();
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }

    private void setPropertiesCount(ArrayList<HotelDataPojo> arr) {
        noOfHotelsText.setText("Showing Result in " + getFromPrefs(AppConstants.CITY_NAME) + "(" +
                arr.size() + " properties)");
    }

    private void dialogDismis() {
        if (d != null && d.isShowing()) {
            d.dismiss();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.googleMap = googleMap;
        this.googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        this.googleMap.getUiSettings().setZoomControlsEnabled(false);
        this.googleMap.getUiSettings().setCompassEnabled(false);
        this.googleMap.getUiSettings().setZoomGesturesEnabled(true);

        boolean success = false;
        try {
            // Customise the styling of the base map using a JSON object defined
            // in a raw resource file.
            if (checkDayOrNight()) {
                success = googleMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                this, R.raw.style_json));
            } else {
                success = googleMap.setMapStyle(
                        MapStyleOptions.loadRawResourceStyle(
                                this, R.raw.day_sample_json));
            }
            if (!success) {

            }
        } catch (Resources.NotFoundException e) {
            e.printStackTrace();
        }
        // Position the map's camera near Sydney, Australia.
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(-34, 151)));
    }

    private void udpateMapView(double lat, double lan, int zoomLevel) {


        if (googleMap != null) {
            // Position the map.
            googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(lat, lan), zoomLevel));

            // Initialize the manager with the context and the map.
            // (Activity extends context, so we can pass 'this' in the constructor.)
            mClusterManager = new ClusterManager<MyClusterItem>(this, googleMap);
            // Point the map's listeners at the listeners implemented by the cluster
            // manager.
            googleMap.setOnCameraIdleListener(mClusterManager);
            googleMap.setOnMarkerClickListener(mClusterManager);
//            mClusterManager.setOnClusterItemClickListener(this);

            // Add cluster items (markers) to the cluster manager.
            for (int i = 0; i < hotel_detail_arr.size(); i++) {
                if (hotel_detail_arr.get(i).getLatitude() != null && !hotel_detail_arr.get(i).getLatitude().isEmpty()
                        && hotel_detail_arr.get(i).getLongitude() != null && !hotel_detail_arr.get(i).getLongitude().isEmpty()) {
                    double offset = i / 60d;
                    lattitude = Double.parseDouble(hotel_detail_arr.get(i).getLatitude());
                    longitude = Double.parseDouble(hotel_detail_arr.get(i).getLongitude());
                    MyClusterItem offsetItem = new MyClusterItem(lattitude, longitude, hotel_detail_arr.get(i).getHotelName(),
                            hotel_detail_arr.get(i).getHotelAddress(),
                            hotel_detail_arr.get(i).getHotelCode(), hotel_detail_arr.get(i).getSupplier(), hotel_detail_arr.get(i).getCurrency());
                    mClusterManager.addItem(offsetItem);

                }
            }

            mClusterManager.setOnClusterItemClickListener(
                    new ClusterManager.OnClusterItemClickListener<MyClusterItem>() {
                        @Override
                        public boolean onClusterItemClick(MyClusterItem clusterItem) {

                            navigateToHotelDetails(clusterItem.getmHotelCode(), clusterItem.getmHotelSupplier(), clusterItem.getmCurrency());

                            return false;
                        }
                    });

        }

    }

    @Override
    public void onBackPressed() {
        if (mFlag) {
            super.onBackPressed();
        } else {
            mFlag = true;
            mapLayout.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            currencyLay.setVisibility(View.GONE);
        }


    }

    public boolean checkDayOrNight() {
        Boolean isNight;
        Calendar cal = Calendar.getInstance();
        int hour = cal.get(Calendar.HOUR_OF_DAY);
        isNight = hour < 6 || hour > 18;
        return isNight;
    }

    private void getcurrencyAPI() {
        if (cd.isConnectingToInternet()) {

//            final ProgressDialog d = AppDialogs.showLoading(ctx);
//            d.setCanceledOnTouchOutside(false);
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<CountryPojo> call = apiService.getcurrency();
            call.enqueue(new Callback<CountryPojo>() {
                @Override
                public void onResponse(Call<CountryPojo> call, Response<CountryPojo> response) {
//                    d.dismiss();

                    if (response.body() != null) {
                        if (response.body().getStatus().equalsIgnoreCase("true")) {
                            if (response.body().getCommonList() != null) {
                                currencyList = response.body().getCommonList();
                                if (currencyList != null && currencyList.size() > 0) {
                                    mCurrencyAdapter = new NationalityAdapter(ctx, currencyList, "Currency", "SearchResultsActivity");
                                    currency_recycler.setAdapter(mCurrencyAdapter);
                                }
                            }
                        } else {
                            dialog.displayCommonDialog(getResources().getString(R.string.currency_error), response.body().getMessage());
                        }
                    } else {
                        dialog.displayCommonDialog(getResources().getString(R.string.currency_error), AppConstants.SERVER_NOT_RESPONDING);
                    }

                }

                @Override
                public void onFailure(Call<CountryPojo> call, Throwable t) {
                    // Log error here since request failed
//                    d.dismiss();
                }
            });
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == requestCodeFilter) {
            if (resultCode == 1) {
                hotel_detail_arr.clear();
                String hotelName = data.getStringExtra("hotel_name");
                String star = data.getStringExtra("star");
                getFilterHotelList(hotelName, star);
            }
        }
    }

    //      Fetch Hotle List after Apply Filter
    private void getFilterHotelList(String hotelName, String star) {
        if (cd.isConnectingToInternet()) {
            ApiInterface apiService =
                    ApiClient.getClient().create(ApiInterface.class);

            Call<SearchResultPojo> call = apiService.filterHotels(getFromPrefs(AppConstants.RANDOM_NO), hotelName, star);
            call.enqueue(new Callback<SearchResultPojo>() {
                @Override
                public void onResponse(Call<SearchResultPojo> call, Response<SearchResultPojo> response) {
                    if (response.body() != null) {
                        if (response.body().getStatus() != null) {
                            if (response.body().getHotels() != null && response.body().getHotels().size() > 0) {
                                dialogDismis();
                                hotel_detail_arr = response.body().getHotels();
                                mAdapter = new HotelListAdapter(ctx, hotel_detail_arr);
                                recyclerView.setAdapter(mAdapter);
                                mAdapter.notifyDataSetChanged();
                                setPropertiesCount(hotel_detail_arr);
                                no_item_layout.setVisibility(View.GONE);
                            } else {
                                if (hotel_detail_arr.size() == 0) {
                                    no_item_layout.setVisibility(View.VISIBLE);
                                }
                                dialogDismis();

                            }
                        } else {
                            dialogDismis();
                            if (hotel_detail_arr.size() == 0) {
                                dialog.displayCommonDialog(getResources().getString(R.string.search_hotel_failed), AppConstants.SERVER_NOT_RESPONDING);
                            }
                        }
                    } else {
                        dialogDismis();
                        if (hotel_detail_arr.size() == 0) {
                            dialog.displayCommonDialog(getResources().getString(R.string.search_hotel_failed), AppConstants.SERVER_NOT_RESPONDING);
                        }
                    }

                }

                @Override
                public void onFailure(Call<SearchResultPojo> call, Throwable t) {
                    // Log error here since request failed
                    dialogDismis();
                }
            });
        } else {
            dialog.displayCommonDialog(getResources().getString(R.string.no_internet), AppConstants.NO_INTERNET_CONNECTED);
        }
    }
}
